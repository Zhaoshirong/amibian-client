unit desktop.window.shell;

interface

uses
  W3C.DOM,
  System.Widget,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Colors,
  System.Time,
  System.IoUtils,
  System.Device.Storage,

  desktop.editor,

  Desktop.Control,
  Desktop.IconView,
  Desktop.Scrollbar,
  Desktop.Panel,
  Desktop.Button,
  Desktop.Switch,
  Desktop.Window,
  Desktop.Network.Connection,
  Desktop.Types,

  ragnarok.messages.factory,
  ragnarok.messages.base,
  ragnarok.messages.callstack,
  ragnarok.messages.network,

  SmartCL.System,
  SmartCL.Time,
  SmartCL.Controls.Elements,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Borders,
  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet,
  SmartCL.Net.Websocket,
  SmartCL.Controls.Toolbar;

type

  TWbWindowContentEditor = class(TWbWindowContent)
  private
    FEditor:  TTerminalEditor;
    procedure HandleBlocked(Sender: TObject);
    procedure HandleUnBlocked(Sender: TObject);
    procedure HandleScrollbarMoved(Sender: TObject);
  protected
    procedure SetEnabled(const EnabledValue: boolean); override;

    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  Editor: TTerminalEditor read FEditor;
  end;

  TWbShellCommandEvent = procedure (Sender: TObject; CommandText: string);

  TWbShellWindow = class(TWbWindow)
  private
    FClient:  TWbNetworkClient;
    procedure HandleConnected(Sender: TObject);
    procedure HandleDiconnected(Sender: TObject);
    procedure HandleEditPositionChanged(Sender: TObject);
  protected
    procedure UpdateScrollbars;

    function  GetFullScreenElement: TControlHandle; override;

    function  CreateContentInstance: TWbWindowContent; override;
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;

  public
    procedure ExecShellCommand(CommandText: string);
    procedure Print(Text: string);
    procedure PrintF(Text: string; const Values: array of const);

    function  GetEditor: TTerminalEditor;

    property  Client: TWbNetworkClient read FClient;
    property  OnShellCommand: TWbShellCommandEvent;
  end;


implementation

//#############################################################################
// TWbShellWindow
//#############################################################################

procedure TWbShellWindow.InitializeObject;
begin
  inherited;
  Header.Title.Caption := 'Shell';
  Options := [ woSizeable, woVScroll];

  var LAccess := GetDesktop();
  if LAccess <> nil then
  begin
    FClient := LAccess.GetConnection(true);
    FClient.OnConnected := @HandleConnected;
    FClient.OnDisconnected := @HandleDiconnected;
  end else
  raise EW3Exception.Create("Failed to access desktop API error");

  GetEditor().OnCursorPosChanged := @HandleEditPositionChanged;

  self.VerticalScroll.OnPositionChanged :=
    procedure (Sender: TObject)
    begin
      //writeln("Position changed to:" + VerticalScroll.Position.ToString());
      var LEditor := GetEditor();
      LEditor.SetTopLine(VerticalScroll.position);
    end;

  GetEditor().OnNewLine :=
    procedure (Sender: TObject)
    begin
      if not (csDestroying in ComponentState) then
        UpdateScrollbars();

      var LEditor := GetEditor();
      LEditor.SetTopLine( LEditor.LineCount - LEditor.LinesShowing);
      VerticalScroll.Position := LEditor.topRow;
    end;

  Content.Enabled := false;
end;

procedure TWbShellWindow.FinalizeObject;
begin

  try
    // decouple events
    FClient.OnConnected := nil;
    FClient.OnDisconnected := nil;

    // perform logout if required
    try
      if (FClient.Connection.SocketState in [stConnecting, stOpen]) then
        FClient.Connection.Disconnect();
    finally

      FClient.free;
      FClient := nil;
    end;
  except
    on e: exception do;
  end;

  inherited;
end;

procedure TWbShellWindow.ObjectReady;
begin
  inherited;

  TW3Dispatch.WaitFor([self, Content], 6,
    procedure(Success: boolean)
    begin

      var LEditor := TWbWindowContentEditor(Content).Editor;
      TW3Dispatch.WaitFor([LEditor],
        procedure ()
        begin
          LEditor.NewDocument(
            procedure (Success: boolean)
            begin
              LEditor.Print('QUARTEX WEB Operating System and Libraries',  false);
              LEditor.Print('Copyright © 2010-2019 Quartex Components, Inc.',  false);
              LEditor.Print('All Rights Reserved.',  false);
              LEditor.Print(' ',  false);
            end);

          TW3Dispatch.Execute(
          procedure ()
          begin
            var LAccess := GetDesktop();
            var LHostInfo := LAccess.GetHostInfo();
            PrintF("Connecting to host: %s ", [LHostInfo.hiHostName]);
            FClient.Connection.Connect(LHostInfo.htWebSocketEndpoint);
          end, 500);

        end);
    end);
end;

procedure TWbShellWindow.UpdateScrollbars;
begin
  var LEditor := GetEditor();
  if LEditor.LinesShowing < LEditor.LineCount then
  begin
    VerticalScroll.enabled := true;
    VerticalScroll.Total := LEditor.LineCount;
    VerticalScroll.Position := LEditor.LineCount - LEditor.LinesShowing;
    VerticalScroll.PageSize := LEditor.LinesShowing;
  end else
  begin
    if VerticalScroll.Enabled then
    begin
      VerticalScroll.total := 10;
      VerticalScroll.pagesize := 10;
      VerticalScroll.position := 0;
      VerticalScroll.enabled := false;
    end;
  end;
end;

function TWbShellWindow.GetFullScreenElement: TControlHandle;
begin
  // Ok we want the editor canvas to be the fullscreen  element
  result := TWbWindowContentEditor(self.Content).Editor.Handle;
end;

procedure TWbShellWindow.Resize;
begin
  inherited;
  UpdateScrollbars();
end;

function TWbShellWindow.CreateContentInstance: TWbWindowContent;
begin
  result := TWbWindowContentEditor.Create(self);
end;

procedure TWbShellWindow.HandleEditPositionChanged(Sender: TObject);
begin
  UpdateScrollbars();
end;

procedure TWbShellWindow.HandleConnected(Sender: TObject);
begin
  Content.Enabled := true;
  print("Connected. Sending credentials");
  Header.Title.Caption := 'Shell - Connected';

  TW3Dispatch.Execute( procedure ()
  begin
    FClient.Login(
    procedure (const Response: TQTXBaseMessage)
    begin
      var LEditor := GetEditor();
      if TQTXServerMessage(response).Code = 200 then
      begin
        Header.Title.Caption := 'Shell - Logged in';
        LEditor.Print("You are now logged in", false);
        LEditor.ShowTerminalPrefix();
        LEditor.SetFocus();
      end else
      begin
        Header.Title.Caption := 'Shell - Login failed';
        LEditor.PrintF("Login failed, error code %d with message '%s'",
        [TQTXServerMessage(response).Code,
        TQTXServerMessage(response).Response], false);
      end;
    end);
  end, 100);
end;

procedure TWbShellWindow.HandleDiconnected(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    if TVariant.ClassInstance(self) then
    begin
      Content.Enabled := false;
      Header.Title.Caption := 'Shell - Disconnected';
      Print("You have been disconnected from the server");
    end;
  end;
end;

function TWbShellWindow.GetEditor: TTerminalEditor;
begin
  result := TWbWindowContentEditor(Content).Editor;
end;

procedure TWbShellWindow.PrintF(Text: string; const Values: array of const);
begin
  Print(Format(Text, Values));
end;

procedure TWbShellWindow.Print(Text: string);
begin
  if not (csDestroying in ComponentState) then
  begin
    var LContent := TWbWindowContentEditor(Content);
    LContent.Editor.Print(Text, false);
    LContent.Invalidate();
  end;
end;

procedure TWbShellWindow.ExecShellCommand(CommandText: string);
begin
  CommandText := CommandText.trim();
  if CommandText.length < 1 then
    exit;

  // Fire off an event, good for logging
  if assigned(OnShellCommand) then
    OnShellCommand(Self, CommandText);

  // Check that we can actually send anything
  if FClient.Connection.SocketState <> stOpen then
  begin
    Print("You are not connected to the server");
    exit;
  end;

  var LCmdParts := CommandText.Split(#32);
  var LCommand := LCmdParts[0].ToLower();
  var LEditor := GetEditor();

  writeln("shipping command: " + LCommand);

  case LCommand of
  'hostinfo':
    begin
      var LAccess := GetDesktop();
      var LInfo := LAccess.GetHostInfo();
      GetEditor().NewTextBlock(
        procedure (Success: boolean)
        begin
          LEditor.Print('Host: ' + LInfo.hiHostName, true);
          LEditor.Print('Port: ' + LInfo.hiHostPort.ToString(), true);
          LEditor.Print('SSL:  ' + LInfo.hiSecure.ToString(), true);
          LEditor.Print('WSSE: ' + LInfo.htWebSocketEndpoint, true);
          LEditor.Print('WHPE: ' + LInfo.hiWebEndpoint, true);

          LEditor.ShowTerminalPrefix();
          Content.Invalidate();
        end);
    end;
  'cls':
    begin
      LEditor.Clear();
      LEditor.ShowTerminalPrefix();
      Content.Invalidate();
    end;
  'dir':
    begin
      FClient.FileIODir(CommandText,  null,
        procedure (Sender: TObject; TagValue: variant; Files: TNJFileItemList; Success: boolean)
        begin

          if not Success then
          begin
            if FClient.LastError.length > 0 then
              LEditor.Print(FClient.LastError, true);
            LEditor.ShowTerminalPrefix();
            exit;
          end;

          GetEditor().NewTextBlock(
            procedure (Success: boolean)
            begin
              case Success of
              true:
                begin
                  for var xitem in Files.dlItems do
                  begin
                    case xItem.diFileType of
                    wtFolder: LEditor.Print("[" + xItem.diFileName + "]", true);
                    wtFile:   LEditor.Print(xItem.diFileName, true);
                    end;
                  end;
                  LEditor.ShowTerminalPrefix();
                end;
              false:
                begin
                  LEditor.Print(FClient.LastError, true);
                  LEditor.ShowTerminalPrefix();
                end;
              end;

              Content.Invalidate();
            end);
        end);
    end;
  'read':
    begin
    end;
  'run':
    begin
    end;
  else
    begin
      { var LBytes := Packet.Attachment.Read(Packet.Attachment.Size);
      var LText := TDataType.BytesToString(LBytes);
      var LLines := LText.Split(#13);
      GetEditor().NewTextBlock(
        procedure (Success: boolean)
        begin
          var LEditor := GetEditor();
          for var xLine in LLines do
          begin
            LEditor.Print(xline, true);
          end;
          LEditor.ShowTerminalPrefix();
          Content.Invalidate();
        end); }
    end;
  end;

  exit;


  // Commands are parsed and processed server-side
  FClient.FileIO(CommandText, procedure (const Response: TQTXBaseMessage)
  begin
    if (response is TQTXServerError) then
    begin
      var packet := TQTXServerError(response);
      print("Error: " + Packet.Response);
      exit;
    end;

    if (response is TQTXFileIOResponse) then
    begin
      var packet := TQTXFileIOResponse(response);

      var LRes := Packet.Response.ToLower();
      if LRes <> 'ok' then
      begin
        var LExt := TPath.GetExtension(LRes);
        if LExt.length > 0 then
        begin
          if LExt <> '.txt' then
          begin
            Print(Format("Binary file %s cannot be emitted to shell",[LRes]));
            Print(Format("File size = %d bytes", [Packet.Attachment.Size]));
            Content.Invalidate();
            exit;
          end else
          begin
            if Packet.Attachment.Size > 1024 * 3 then
            begin
              Print(Format("Textfile %s exceeds 3kb and cannot be emitted to shell",[LRes]));
              Print(Format("File size = %d bytes", [Packet.Attachment.Size]));
              Content.Invalidate();
              exit;
            end;
          end;
        end;
      end;

      //var LBytes := Packet.Attachment.Read(Packet.Attachment.Size);
      var LBytes := Packet.Attachment.ToBytes();
      var LText := TDataType.BytesToString(LBytes);
      var LLines := LText.Split(#13);
      LText := '';
      LBytes := [];

      GetEditor().NewTextBlock(
        procedure (Success: boolean)
        begin
          var LEditor := GetEditor();
          for var xLine in LLines do
          begin
            LEditor.Print(xline, true);
          end;
          LEditor.ShowTerminalPrefix();
        end);


      Content.Invalidate();
    end else
    begin
      Print("Unknown server response class: " + response.ClassName);
    end;
  end);

  if FClient.failed then
    Print("Failed:" + FClient.LastError);

  (*

  FClient.FileIO(CommandText, procedure (const Response: TBaseMessage)
  begin
    if (response is TServerError) then
    begin
      var packet := TServerError(response);
      print(Packet.Response);
      exit;
    end;

    if (response is TFileIOResponse) then
    begin
      var packet := TFileIOResponse(response);
      if Packet.Attachment.Size > 0 then
      begin
        var LRes := Packet.Response.ToLower();
        if LRes <> 'ok' then
        begin

          var LExt := TPath.GetExtension(LRes);
          if LExt.length > 0 then
          begin
            if LExt <> '.txt' then
            begin
              Print(Format("Binary file %s cannot be emitted to shell",[LRes]));
              Print(Format("File size = %d bytes", [Packet.Attachment.Size]));
              Content.Invalidate();
              exit;
            end else
            begin
              if Packet.Attachment.Size > 1024 * 3 then
              begin
                Print(Format("Textfile %s exceeds 3kb and cannot be emitted to shell",[LRes]));
                Print(Format("File size = %d bytes", [Packet.Attachment.Size]));
                Content.Invalidate();
                exit;
              end;
            end;
          end;
        end;

        var LBytes := Packet.Attachment.Read(Packet.Attachment.Size);
        var LText := TDataType.BytesToString(LBytes);
        LText := StringReplace(LText, #13, '<br>', [rfReplaceAll, rfIgnoreCase]);
        Print(LText);
      end;
      Content.Invalidate();
    end;
  end);
  if FClient.failed then
    Print("Failed:" + FClient.LastError);   *)
end;

//#############################################################################
// TWbWindowContentEditor
//#############################################################################

procedure TWbWindowContentEditor.InitializeObject;
begin
  inherited;
  FEditor := TTerminalEditor.Create(self);
  FEditor.ConsoleBehavior := true;

  FEditor.OnConsoleCmd := procedure (Sender: TObject; Cmd: string)
  begin
    if Parent <> nil then
    begin
      var LWindow := TWbShellWindow(Parent);
      LWindow.ExecShellCommand(Cmd);
    end;
  end;

  OnInputDisabled := @HandleBlocked;
  OnInputEnabled := @HandleUnBlocked;
end;

procedure TWbWindowContentEditor.FinalizeObject;
begin
  FEditor.free;
  inherited;
end;

procedure TWbWindowContentEditor.ObjectReady;
begin
  inherited;

  TW3Dispatch.WaitFor([FEditor], procedure ()
  begin
    Invalidate();
    if Parent <> nil then
    begin
      var LWindow := TWbShellWindow(Parent);
      LWindow.RightEdge.Scrollbar.OnChanged := @HandleScrollbarMoved;
    end;
  end);
end;

procedure TWbWindowContentEditor.HandleScrollbarMoved(Sender: TObject);
begin
  {if not (csDestroying in ComponentState) then
  begin
    var LScrollbar := TWbVerticalScrollbar(Sender);
    var Lpos := LScrollbar.Position;
    FScrollBox.ScrollController.ScrollTo(0, -LPos);
  end;  }
end;

procedure TWbWindowContentEditor.HandleBlocked(Sender: TObject);
begin
  FEditor.enabled := false;
end;

procedure TWbWindowContentEditor.HandleUnBlocked(Sender: TObject);
begin
  FEditor.Enabled := true;
  FEditor.SetFocus();
end;

procedure TWbWindowContentEditor.SetEnabled(const EnabledValue: boolean);
begin
  inherited SetEnabled(EnabledValue);
  FEditor.enabled := EnabledValue;
end;

procedure TWbWindowContentEditor.Resize;
begin
  inherited;
  if (csReady in ComponentState) then
    FEditor.SetBounds(ClientRect);
end;

end.
