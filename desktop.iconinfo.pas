unit desktop.iconinfo;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.memory.buffer,
  desktop.icons,
  desktop.mimetypes,
  Ragnarok.json,
  SmartCL.System;

  //Online tools when encoding URLS
  //  Step 1 - https://www.freeformatter.com/json-escape.html
  //  Step 2 - https://www.base64encode.org/


// Example of file-format
(*
  {
    "datatype": "link",
    "version": {
      "major": 1,
      "minor": 0,
      "revision": 0
    },
    "action": {
      "method": "open",
      "source": "http://www.wab.com/screen.php?screen=201" >>> Muste be ESCAPED + BAse64 encoded(!)
    },
    "container": {
      "type": "hosted",
      "title": "hello world",
      "width": 700,
      "height": 740,
      "fullscreen": true,
      "handshake": false,
      "keyboard": true,
      "resize": false
    },
    "graphics": {
      "imagedata": ""
    }
  }
*)

type

  // Exception type
  EWbLinkError = class(Exception);

  // Parsing result
  TWbLinkValidation = record
    Success: boolean;
    Error: string;
  end;

  // Wrapper for the version structure
  TWbLinkVersion = class(JObject)
  public
    property  Major: integer;
    property  Minor: integer;
    property  Revision: integer;
  end;

  // Wrapper for the action structure
  TWbLinkAction = class(JObject)
    property  &Method: string;
    property  Source: string;
  end;

  // Wrapper for the container structure
  TWbLinkContainer = class(JObject)
    property  &Type: string;
    property  Title: string;
    property  Width: integer;
    property  Height: integer;
    property  FullScreen: boolean;
    property  HandShake: boolean;
    property  Keyboard: boolean;
    property  Resize: boolean;
  end;

  // Link info class
  TWbLinkInfo = class(TObject)
  private
    FData:      TQTXJSONObject;
    FContainer: TWbLinkContainer;
    FAction:    TWbLinkAction;
    FVersion:   TWbLinkVersion;
  public
    property    Instance: TQTXJSONObject read FData;

    property    Version: TWbLinkVersion read FVersion write FVersion;
    property    Action: TWbLinkAction read FAction write FAction;
    property    Container: TWbLinkContainer read FContainer write FContainer;

    function    Validate(const Obj: TQTXJSONObject): TWbLinkValidation; virtual;
    procedure   FromJSON(IconData: string); virtual;
    procedure   LoadFromStream(const Stream: TStream); virtual;
    procedure   LoadFromBuffer(const Buffer: TBinaryData); virtual;

    function    ToJSON: string; virtual;
    procedure   SaveToStream(const Stream: TStream); virtual;
    procedure   SaveToBuffer(const Buffer: TBinaryData); virtual;

    function    ToBuffer: TBinaryData;
    function    ToStream: TStream;

    procedure   Clear; virtual;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

implementation

//############################################################################
// TWbLinkInfo
//###########################################################################

constructor TWbLinkInfo.Create;
begin
  inherited Create;
  FData := TQTXJSONObject.Create;
  FVersion := new TWbLinkVersion;
  FAction := new TWbLinkAction;
  FContainer := new TWbLinkContainer;
end;

destructor TWbLinkInfo.Destroy;
begin
  FContainer := nil;
  FAction := nil;
  FVersion := nil;
  FData.free;
  inherited;
end;

procedure TWbLinkInfo.Clear;
begin
  FData.Clear();

  // Release old data to the GC
  FContainer := nil;
  FAction := nil;
  FVersion := nil;

  // Setup fresh structures
  FVersion := new TWbLinkVersion;
  FAction := new TWbLinkAction;
  FContainer := new TWbLinkContainer;
end;

function TWbLinkInfo.Validate(const Obj: TQTXJSONObject): TWbLinkValidation;

  function  _ValidateVersion(const Branch: TQTXJSONObject): boolean;
  begin
    if Branch <> nil then
    begin
      if Branch.Exists('major') then
      begin
        if Branch.exists('minor') then
          result := Branch.exists('revision');
      end;
    end;
  end;

  function  _ValidateAction(const Branch: TQTXJSONObject): boolean;
  begin
    if Branch <> nil then
    begin
      if Branch.Exists('method') then
        result := Branch.exists('source');
    end;
  end;

  function  _ValidateContainer(const Branch: TQTXJSONObject): boolean;
  begin
    if Branch <> nil then
    begin
      if Branch.Exists('type') then
      begin
        if Branch.Exists('width') then
        begin
          if Branch.Exists('height') then
          begin
            if Branch.Exists('fullscreen') then
            begin
              if Branch.Exists('handshake') then
              begin
                if Branch.exists('keyboard') then
                begin
                  if Branch.exists('resize') then
                  begin
                    result := Branch.Exists('title');
                  end;
                end;
              end;
            end;
          end;
        end;
      end;
    end;
  end;

begin
  if obj = nil then
  begin
    result.Error := 'Invalid JSON object';
    exit;
  end;

  if not obj.Exists('datatype') then
  begin
    result.Error := 'field "datatype" could not be found';
    exit;
  end;

  try

    if not (obj.ReadString('datatype') in ['link', 'local']) then
    begin
      result.Error := 'Invalid datatype, expected "link" or "local" error';
      exit;
    end;

    if not _ValidateVersion( obj.Locate('version', false) ) then
    begin
      result.Error := 'Version structure failed to validate error';
      exit;
    end;

    if not _validateAction( obj.Locate('action', false) ) then
    begin
      result.Error := 'Action structure failed to validate error';
      exit;
    end;

    if not _validateContainer( obj.Locate('container', false) ) then
    begin
      result.Error := 'Container structure failed to validate error';
      exit;
    end;

  except
    exit;
  end;

  result.Error := '';
  result.Success := true;
end;


function TWbLinkInfo.ToJSON: string;
begin
  var LObj := FData.Locate('version', true);
  LObj.WriteOrAdd('major', FVersion.Major);
  LObj.WriteOrAdd('minor', FVersion.Minor);
  LObj.WriteOrAdd('revision', FVersion.Revision);

  LObj := FData.Locate('action', true);
  LObj.WriteOrAdd('method', FAction.&Method);
  LObj.WriteOrAdd('source', FAction.Source);

  LObj := FData.Locate('container', true);
  LObj.WriteOrAdd('type', FContainer.&Type);
  LObj.WriteOrAdd('title', FContainer.Title);
  LObj.WriteOrAdd('width', FContainer.width);
  LObj.WriteOrAdd('height', FContainer.height);
  LObj.WriteOrAdd('fullscreen', FContainer.FullScreen);
  LObj.WriteOrAdd('handshake', FContainer.HandShake);
  LObj.WriteOrAdd('keyboard', FContainer.Keyboard);
  LObj.WriteOrAdd('resize', FContainer.Resize);

  result := LObj.ToJSON(2);
end;

function TWbLinkInfo.ToBuffer: TBinaryData;
begin
  result := TBinaryData.Create();
  SaveToBuffer(result);
end;

function TWbLinkInfo.ToStream: TStream;
begin
  result := TMemoryStream.Create();
  SaveToStream(result);
  result.Position := 0;
end;

procedure TWbLinkInfo.SaveToStream(const Stream: TStream);
begin
  Stream.Write(Stream.StringToBytes( ToJSON() ));
end;

procedure TWbLinkInfo.SaveToBuffer(const Buffer: TBinaryData);
begin
  Buffer.AppendBytes( Buffer.StringToBytes( ToJSON() ) );
end;

procedure TWbLinkInfo.FromJSON(IconData: string);
begin
  // Clear current info
  Clear();

  // Parse JSON
  try
    FData.FromJSON(IconData);
  except
    on e: exception do
      raise EWbLinkError.Create('Failed to load icon-info:' + e.message);
  end;

  // Validate structure
  var LInfo := Validate(FData);
  if not LInfo.Success then
  begin
    Clear();
    raise EWbLinkError.Create('Failed to load link info: ' + LInfo.Error);
  end;

  try
    var LObj := FData.Locate('version', false);
    FVersion.Major := LObj.ReadInteger('major');
    FVersion.Minor := LObj.ReadInteger('minor');
    FVersion.Revision := LObj.ReadInteger('revision');
  except
    on e: exception do
    raise EWbLinkError.Create('Failed to load version values: ' + e.message);
  end;

  try
    var LObj := FData.Locate('action', false);
    FAction.&Method := LObj.ReadString('method');
    FAction.Source := LObj.ReadString('source');
  except
    on e: exception do
    raise EWbLinkError.Create('Failed to load action values: ' + e.message);
  end;

  try
    var LObj := FData.Locate('container', false);
    FContainer.&Type := LObj.ReadString('type');
    FContainer.Title := LObj.ReadString('title');
    FContainer.Width := LObj.ReadInteger('width');
    FContainer.Height := LObj.ReadInteger('height');
    FContainer.FullScreen := LObj.ReadBoolean('fullscreen');
    FContainer.HandShake := LObj.ReadBoolean('handshake');
    FContainer.Keyboard := LObj.ReadBoolean('keyboard');
    FContainer.Resize := LObj.ReadBoolean('resize');
  except
    on e: exception do
    raise EWbLinkError.Create('Failed to load container values: ' + e.message);
  end;
end;

procedure TWbLinkInfo.LoadFromStream(const Stream: TStream);
begin
  if Stream <> nil then
  begin
    var LBytes := Stream.Read(Stream.Size);
    FromJSON( Stream.BytesToString(LBytes) );
  end else
  raise EWbLinkError.Create('Failed to load link data, buffer was nil error');
end;

procedure TWbLinkInfo.LoadFromBuffer(const Buffer: TBinaryData);
begin
  if Buffer <> nil then
  begin
    FromJSON( Buffer.ToString() );
  end else
  raise EWbLinkError.Create('Failed to load link data, buffer was nil error');
end;

end.
