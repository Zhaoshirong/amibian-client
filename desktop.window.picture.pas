unit desktop.window.picture;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.Memory.Buffer,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Device.Storage,

  Desktop.Types,
  Desktop.Control,
  Desktop.Window,

  SmartCL.Theme,

  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System,
  SmartCL.Scroll,
  SmartCL.Controls.Scrollbox,
  SmartCL.Controls.Panel,
  SmartCL.Controls.Toolbar,
  SmartCL.Controls.Image;

type

  TWbImageDisplayMode = (
    pdFull      = 0,
    pdFit       = 1,
    pdCover     = 2
    );

  TWbImageViewer = class(TWbWindow)
  private
    FToolbox: TW3Toolbar;
    FBox:     TW3Scrollbox;
    FImage:   TW3Image;
    FMode:    TWbImageDisplayMode;
    procedure HandleScaleBtnClicked(Sender: TObject);
    procedure SetMode(const NewMode: TWbImageDisplayMode);
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure Resize; override;
    procedure ObjectReady; override;

  public
    property  ToolBox: TW3Toolbar read FToolbox;
    property  Scrollbox: TW3Scrollbox read FBox;
    property  Image: TW3Image read FImage;
    property  ImageDisplayMode: TWbImageDisplayMode read FMode write SetMode;

    procedure SetupSelectedMode;

    procedure LoadImageFromStream(const Title: string; const Stream: TStream);
    procedure LoadFromBuffer(const Title: string; const Buffer: TBinaryData);
  end;

implementation

//############################################################################
// TWbImageViewer
//############################################################################

procedure TWbImageViewer.InitializeObject;
begin
  inherited;
  TransparentEvents := true;
  Options := [woSizeable];
  MinimumWidth := 200; //550;
  MinimumHeight := 200;

  Header.Title.Caption := 'Image viewer';

  FToolbox := TW3Toolbar.Create(Content);

  FMode := pdFit;
  FBox := TW3Scrollbox.Create(Content);
  FBox.TransparentEvents := true;
  FBox.AutoAdjustToContent := true;
  FBox.ScrollController.Enabled := true;
  FBox.ThemeReset();
  FBox.ScrollBars := TW3ScrollBarType.sbIndicator;
  FBox.AutoAdjustToContent := false;

  FImage := TW3Image.Create(FBox.Content);


  // firefox demands this, or it goes nuts
  FImage.handle.ondragstart := function (event: variant): boolean
  begin
    result := false;
  end;

end;

procedure TWbImageViewer.FinalizeObject;
begin
  FToolbox.free;
  FImage.free;
  FBox.free;
  inherited;
end;

procedure TWbImageViewer.ObjectReady;
begin
  inherited;

  TW3Dispatch.WaitFor([FBox, FImage, FToolbox],
  procedure ()
  begin
    FToolbox.ButtonWidth := 64;

    var LBtnCover := FToolbox.Add();
    LBtnCover.Caption := 'Cover';
    LBtnCover.GroupIndex := 1;
    LBtnCover.TagValue := ord(TWbImageDisplayMode.pdCover);
    LBtnCover.AllowAllUp := false;
    LBtnCover.OnClick := @HandleScaleBtnClicked;

    var LBtnFull := FToolbox.Add();
    LBtnFull.Caption := 'Full';
    LBtnFull.GroupIndex := 1;
    LBtnFull.TagValue := ord(TWbImageDisplayMode.pdFull);
    LBtnFull.AllowAllUp := false;
    LBtnFull.OnClick := @HandleScaleBtnClicked;

    var LBtnFit := FToolbox.Add();
    LBtnFit.Caption := 'Fit';
    LBtnFit.GroupIndex := 1;
    LBtnFit.TagValue := ord(TWbImageDisplayMode.pdFit);
    LBtnFit.AllowAllUp := false;
    LBtnFit.OnClick := @HandleScaleBtnClicked;

    FToolbox.AddSeparator();

    var LBtnBack := FToolbox.Add();
    LBtnBack.Caption := 'Back';
    LBtnBack.GroupIndex := 2;
    LBtnBack.TagValue := -1;

    var LBtnNext := FToolbox.Add();
    LBtnNext.Caption := 'Next';
    LBtnNext.GroupIndex := 2;
    LBtnNext.TagValue := -1;


    LBtnFit.Down := true;

    Invalidate();
    SetupSelectedMode();
  end);
end;

procedure TWbImageViewer.SetupSelectedMode;
begin
  var LBounds := FBox.Content.ClientRect();
  case FMode of
  pdFit:
    begin
      FBox.ScrollBars := TW3ScrollBarType.sbNone;
      FBox.Content.Width := FBox.Container.ClientWidth;
      FBox.Content.Height := FBox.Container.ClientHeight;
      FImage.SetBounds(LBounds.left, LBounds.top, FBox.Container.ClientWidth, FBox.Container.ClientHeight);

      FImage.handle.style['object-fit'] := 'contain';
      FImage.Background.Size.Mode := smAuto;
      FImage.Background.Size.Apply();
    end;

  pdFull:
    begin
      FBox.ScrollBars := TW3ScrollBarType.sbIndicator;
      var wd := FImage.PixelWidth;
      var hd := FImage.PixelHeight;
      FImage.SetBounds(LBounds.left, LBounds.top, wd, hd);
      FBox.Content.Width := wd;
      FBox.Content.Height := hd;

      FImage.handle.style['object-fit'] := 'none';
      FImage.Background.Size.Mode := smAuto;
      FImage.Background.Size.Apply();

      writelnF("Setup pdNone @ %d-%d", [wd, hd]);
    end;

  pdCover:
    begin
      FBox.ScrollBars := TW3ScrollBarType.sbNone;
      FBox.Content.Width := FBox.Container.ClientWidth;
      FBox.Content.Height := FBox.Container.ClientHeight;
      FImage.SetBounds(LBounds.left, LBounds.top, FBox.Container.ClientWidth, FBox.Container.ClientHeight);
      FImage.handle.style['object-fit'] := 'cover';
      FImage.Background.Size.Mode := smAuto;
      FImage.Background.Size.Apply();

    end;
  end;
  FBox.UpdateContent();
end;

procedure TWbImageViewer.Setmode(const NewMode: TWbImageDisplayMode);
begin
  if not (csDestroying in ComponentState) then
  begin
    FMode := NewMode;
    case FMode of
    pdCover, pdFit: FImage.Cursor := TCursor.crDefault;
    pdFull:         FImage.Cursor := TCursor.crMove;
    end;

    SetupSelectedMode();
  end;
end;


procedure TWbImageViewer.HandleScaleBtnClicked(Sender: TObject);
begin
  case TW3ToolbarButton(sender).TagValue of
  ord(pdFit):
    begin
      BeginUpdate();
      SetMode(pdFit);
      AddToComponentState([csSized]);
      EndUpdate();
      FBox.Invalidate();
    end;
  ord(pdFull):
    begin
      BeginUpdate();
      //TWbGraphicsContent(Content).DisplayMode := pdNone;
      SetMode(pdFull);

      AddToComponentState([csSized]);
      EndUpdate();
      FBox.Invalidate();
    end;
  ord(pdCover):
    begin
      BeginUpdate();
      SetMode(pdCover);
      AddToComponentState([csSized]);
      EndUpdate();
      FBox.Invalidate();
    end;
  end;
end;

procedure TWbImageViewer.Resize;
begin
  inherited;
  if (csReady in Componentstate) then
  begin
    if TW3Dispatch.AssignedAndReady([Content, FImage, FBox, FToolbox]) then
    begin
      var LBounds := Content.AdjustClientRect(Content.ClientRect);
      FToolbox.BeginUpdate();
      FToolbox.SetBounds(LBounds.left, LBounds.top, LBounds.width, 40);
      FToolbox.EndUpdate();
      inc(LBounds.top, FToolbox.height);
      FBox.SetBounds(LBounds);

      case FMode of
      pdFull:
        begin
          var wd := FImage.PixelWidth;
          var hd := FImage.PixelHeight;
          FBox.Content.Width := wd;
          FBox.Content.Height := hd;
          FBox.UpdateContent();
        end;
      pdCover, pdFit:
        begin
          FBox.Content.Width := FBox.Container.ClientWidth;
          FBox.Content.Height := FBox.Container.ClientHeight;
          FImage.SetBounds(FBox.Content.BoundsRect);
          FBox.UpdateContent();
        end;
      end;


      FBox.ScrollController.Refresh();
    end;
  end;
end;

procedure TWbImageViewer.LoadFromBuffer(const Title: string; const Buffer: TBinaryData);
begin
  if Buffer <> nil then
  begin
    Header.Title.Caption := 'Image viewer ' + Title.trim();

    FImage.LoadFromBinaryData(Buffer, procedure (success: boolean)
    begin
      BeginUpdate();
      //FImage.handle.style['object-fit'] := 'contain';
      //FImage.Background.Size.Mode := smCover;
      //FImage.Background.Size.Apply();
      AddToComponentState([csSized]);
      EndUpdate();

      //FMode := pdFull;
      SetupSelectedMode();
    end);

  end else
  begin
    Header.Title.Caption := 'Image viewer';
    FImage.Clear();
  end;
end;

procedure TWbImageViewer.LoadImageFromStream(const Title: string; const Stream: TStream);
begin
  if Stream <> nil then
  begin
    Header.Title.Caption := 'Image viewer ' + Title.trim();
    FImage.LoadFromStream(Stream, procedure (success: boolean)
    begin
      BeginUpdate();
      //FImage.handle.style['object-fit'] := 'contain';
      //FImage.Background.Size.Mode := smCover;
      //FImage.Background.Size.Apply();

      AddToComponentState([csSized]);
      EndUpdate();

      //FMode := pdFull;
      SetupSelectedMode();
    end);

  end else
  begin
    Header.Title.Caption := 'Image viewer';
    FImage.Clear();
  end;
end;

end.
