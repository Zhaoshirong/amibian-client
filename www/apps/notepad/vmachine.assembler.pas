unit vmachine.assembler;

interface

{$I 'vmachine.inc'}
{.$DEFINE DEBUG_OUT}

uses
  W3C.TypedArray,
  System.Types,
  System.Types.Convert,
  System.Reader,
  System.Writer,
  System.Memory.Buffer,
  System.Text.Parser,
  System.Time,
  System.Hash,
  System.Dictionaries,

  {$IFDEF DEBUG_OUT}
  SmartCL.System,
  {$ENDIF}

  vmachine.types,
  vmachine.data,
  vmachine.model;

type

  /* The instruction-info record holds information about the
    expected syntax for an instruction. This includes the name, the
    number of parameters it requires and the opcode [bytecode].
    This info is used to hold registered instructions for the assembler */
  TInstructionInfo = record
    Name: string;
    params: integer;
    Opcode: integer;
  end;
  TInstructionInfoArray = array of TInstructionInfo;

  // Exception types
  EAssemblerError = class(EW3Exception);

  TAssembler = class(TObject)
  private
    FBuffer:    TBinaryData;
    FContext:   TAsmParserContext;
    FModel:     TAsmModelSourceFile;
  public
    property    Buffer: TBinaryData read FBuffer;

    procedure   Emit(const Bytes: TByteArray);
    procedure   EmitIndex(const Index: longword);
    procedure   EmitConst(const Index: longword);
    procedure   EmitLength(const ThisLength: byte);

    procedure   cAlloc(const id: TParamInfo; const &DataType: TParamInfo);
    procedure   cVFree(const id: TParamInfo);
    procedure   cMove(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cPush(const Source: TParamInfo);
    procedure   cPop(const Target: TParamInfo);
    procedure   cAdd(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cSub(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cMul(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cDiv(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cMod(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cLsR(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cLsL(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cBtst(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cBset(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cBclr(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cAnd(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cOR(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cNot(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cXOR(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cCMP(const Target: TParamInfo; const Source: TParamInfo);
    procedure   cSYS(const ProcId: integer);
    procedure   cNOOP;
    procedure   cRts;

    procedure   cJMP(Offset: integer);
    procedure   cJsr(Offset: integer);
    procedure   cBne(Offset: integer);
    procedure   cBeq(Offset: integer);
    procedure   cBle(Offset: integer);
    procedure   cBgt(Offset: integer);

    procedure   Assemble(const Context: TAsmParserContext; Data: TAsmInstrArray);

    procedure   Clear;

    constructor Create; virtual;
    destructor  Destroy; override;
  end;

  TAsmToolbox = static class
  private
    class var __Instr_info: TInstructionInfoArray;
  public
    class function  ValidateInstruction(ThisName: string): integer;
    class function  GetInstructionParamsFor(ThisName: string): integer;
    class procedure RegisterInstruction(const Name: string;
              const Params: integer; const opcode: integer);

    class function  ReadCommaList(const Buffer: TTextBuffer): TStrArray;
    class function  ReadParamList(const Buffer: TTextBuffer): TStrArray;
    class function  InstructionLayoutToStr(const Layout: byte): string;

    class function  Encode(const Opcode: word;
                const Layout: TOpCodeLayout;
                const Target, Source: TAsmParameterMode): longword;

    class procedure Decode(const Instr: longword;
                var Opcode: word;
                var Layout: byte;
                var Target, Source: byte);

    class function  BinToInt(text: string): integer;
    class function  ValidHexChars(const Text: string): boolean;
    class function  ValidDecChars(const Text: string): boolean;
    class function  ValidConstText(const Text: string): boolean;
    class function  ValidBinChars(const Text: string): boolean;
    class function  ValidLabelChars(Text: string): boolean;
  end;

  function  MkInstLayout(const Target, Source: TParamInfo): TOpCodeLayout;

implementation

//############################################################################
// Global functions used with manual assembly
//############################################################################

(* Purpose: "Generate instruction layout byte"
   About:   Used by assembler methods to create the instruction
            layout.

            All instructions operate with a layout, which defines what
            the source and target parameters are. Instructions that does not
            have parameters simply use the layout "clVoid".

   Rules:   1. Only variables and registers can be targets
            2. Only registers can act as pointers
            3. For pointers to be used, the CPU's "DC" register must
               be set first. The DC (data control) must be set to a
               previously allocated memory block. *)
function MkInstLayout(const Target, Source: TParamInfo): TOpCodeLayout;
begin
  case Target.piType of
  pkValue:
    begin
      case Source.piType of
      pkValue:  result := TOpCodeLayout.clValue_Value;
      else
        begin
          raise EAssemblerError.Create
          ('Failed to generate instruction layout, target must be register or variable error');
        end;
      end;
    end;
  pkDC:
    begin
      case source.piType of
      pkRegister: result := TOpCodeLayout.clDC_Reg;
      pkVariable: result := TOpCodeLayout.clDC_Var;
      pkConst:    result := TOpCodeLayout.clDC_Const;
      pkValue:    result := TOpCodeLayout.clDC_Value;
      else
        raise EAssemblerError.Create
        ('Failed to generate instruction layout, invalid source for DC');
      end;
    end;
  pkRegister:
    begin
      case source.piType of
      pkVariable: result := TOpCodeLayout.clReg_Var;
      pkRegister: result := TOpCodeLayout.clReg_Reg;
      pkConst:    result := TOpCodeLayout.clReg_Const;
      pkValue:    result := TOpCodeLayout.clReg_Value;
      end;
    end;
  pkVariable:
    begin
      case source.piType of
      pkRegister: result := TOpCodeLayout.clVar_Reg;
      pkVariable: result := TOpCodeLayout.clVar_Var;
      pkConst:    result := TOpCodeLayout.clVar_Const;
      pkValue:    result := TOpCodeLayout.clVar_Value;
      end;
    end;
  else
    begin
      raise EAssemblerError.Create
      ('Failed to generate instruction layout, target must be register or variable error');
    end;
  end;
end;

//############################################################################
// TAsmToolbox
//############################################################################

class function TAsmToolbox.Encode(const OpCode: word;
  const Layout: TOpCodeLayout;
  const Target, Source: TAsmParameterMode): longword;
begin
  // pack parameter modes [4 bits each] into a single byte
  var ModesByte := ord(source) or ord(target) shl 4;
  result := Opcode or (ord(Layout) shl 16) or (ModesByte shl 24);
end;

class procedure TAsmToolbox.Decode(const Instr: longword;
         var Opcode: word;
         var Layout: byte;
         var Target, Source: byte);
begin
  opcode := Instr and $0000FFFF;
  layout := Instr and $00FF0000 shr 16;
  target := (Instr and $FF000000 shr 24) shr 4;
  Source := (Instr and $FF000000 shl 4) shr 28;
end;

class function TAsmToolbox.BinToInt(text: string): integer;

  function __InitUint32: longword;
  begin
    var temp := new JUint32Array(1);
    result := temp[0];
    temp := nil;
  end;

begin
  if text.StartsWith('%') then
    Text := Text.DeleteLeft(1)
  else
  if text.StartsWith('0b') then
    Text := Text.DeleteLeft(2);

  // ensure result is a full longword
  result := __InitUint32();
  var bitindex := 1;
  for var x:=high(text) downto low(text) do
  begin
    var switch := text[x];
    if switch in ['1','0'] then
    begin
      if text[x] = '1' then
        TInteger.SetBit(bitindex, true, result);
      inc(bitindex);
    end else
    raise Exception.Create("BinToInt() failed, invalid binary string error");
  end;
end;

class procedure TAsmToolbox.RegisterInstruction(const Name: string;
  const Params: integer; const opcode: integer);
var
  Info: TInstructionInfo;
begin
  if Name.length > 0 then
  begin
    info.Name := Name;
    info.Params := Params;
    info.opcode := Opcode;
    __Instr_info.add(Info);
  end else
  raise EAssemblerError.Create('Registration failed, invalid instruction name error');
end;

class function TAsmToolbox.GetInstructionParamsFor(ThisName: string): integer;
begin
  ThisName := ThisName.ToLower().Trim();
  if ThisName.Length > 0 then
  begin
    for var x := low(__Instr_info) to high(__Instr_info) do
    begin
      if __Instr_info[x].Name = ThisName then
      begin
        result := __Instr_info[x].params;
        break;
      end;
    end;
  end;
end;


class function TAsmToolbox.InstructionLayoutToStr(const Layout: byte): string;
begin
  case Layout of
  clVoid:         result := "void";
  clRegOnly:      result := "R[%d]";
  clDatOnly:      result := "D[%s]";
  clCntOnly:      result := "C[%d]";
  clVarOnly:      result := "V[%d]";
  clReg_Value:    result := "R[%d], D[%s]";
  clReg_Reg:      result := "R[%d], R[%d]";
  clReg_Var:      result := "R[%d], V[%d]";
  clReg_Const:    result := "R[%d], C[%d]";
  clVar_Const:    result := "V[%d], C[%d]";
  clVar_Reg:      result := "V[%d], R[%d]";
  clVar_Var:      result := "V[%d], V[%d]";
  clVar_Value:    result := "R[%s], D[%s]";
  clDCOnly:       result := "DC";
  clDC_REG:       result := "DC, R[%d]";
  clDC_VAR:       result := "DC, V[%d]";
  clDC_VALUE:     result := "DC, D[%s]";
  clDC_CONST:     result := "DC, C[%d]";
  clValue_Value:  result := "D[%s], D[%s]";
  else
    raise Exception.Create('Unknown instruction layout error');
  end;
end;

class function TAsmToolbox.ReadParamList(const Buffer: TTextBuffer): TStrArray;
var
  LCache: string;
  FInStr: boolean;

  procedure CacheToResult;
  begin
    LCache := LCache.trim();
    if LCache.Length > 0 then
    begin
      result.add(LCache);
      LCache := '';
    end;
  end;

begin
  Buffer.ConsumeJunk();

  FInStr := false;

  While not Buffer.EOF do
  begin
    case Buffer.Current of
    #09:
      begin
        // tab, just skip
      end;
    '"':
      begin
        if not FInStr then
        begin
          FInStr := true;
        end else
        begin
          if LCache.Length > 0 then
          begin
            LCache := ('"' + LCache + '"');
            CacheToResult();
          end;

          FInStr := false;
        end;
      end;
    ')':
      begin
        // inside a string? No problem, collect and go
        if FInStr then
          LCache += Buffer.Current
        else
        begin

          if LCache.Length > 0 then
            CacheToResult();

          // Skip the ) which marks the end of
          // the parameter list
          Buffer.NextNoCrLf();

          break;
        end;
      end;
    ' ':
      begin
        //if FInStr then
        LCache += Buffer.Current;
      end;
    ',':
      begin
        if LCache.Length > 0 then
          CacheToResult();
      end;
    else
      LCache := LCache + Buffer.Current;
    end;

    if not Buffer.NextNoCrLf() then
      break;
  end;

  // force an error
  if FInStr then
    exit;

  if buffer.EOF then
  begin
    if LCache.Length > 0 then
      CacheToResult();
  end;
end;


class function TAsmToolbox.ReadCommaList(const Buffer: TTextBuffer): TStrArray;
var
  LCache: string;
  FInStr: boolean;

  procedure CacheToResult;
  begin
    LCache := LCache.trim();
    if LCache.Length > 0 then
    begin
      result.add(LCache);
      LCache := '';
    end;
  end;

begin
  Buffer.ConsumeJunk();

  FInStr := false;

  var LPrefixStack: TStrArray;

  While not Buffer.EOF do
  begin
    case Buffer.Current of
    #09:
      begin
        // tab, just skip
      end;
    ';':
      begin
        if not FInStr then
        begin
          // in assembly the ; char is a valid remark, so
          // we just skip whatever comes after
          Buffer.ReadToEOL();
        end else
        LCache := LCache + Buffer.Current;
      end;
    #13, #10:
      begin
        // Consume CRLF
        if Buffer.Current = #13 then
          Buffer.ConsumeCRLF
        else
          Buffer.NextNoCrLf();

        if not Buffer.EOF then
        begin
          if LCache.Length > 0 then
            CacheToResult();
        end;

        break;
      end;
    '"':
      begin
        if not FInStr then
        begin
          if LCache.length > 0 then
          begin
            LPrefixStack.push(LCache);
            LCache := '';
          end;
          FInStr := true;
        end else
        begin
          if LCache.Length > 0 then
          begin
            if LPrefixStack.length > 0 then
            begin
              var LTemp := LPrefixStack.pop();
              LTemp += TString.QuoteString(LCache);
              LCache := LTemp;
              CacheToResult();
            end else
            begin
              LCache := ('"' + LCache + '"');
              CacheToResult();
            end;
          end;

          FInStr := false;
        end;
      end;
    ' ':
      begin
        if FInStr then
          LCache += Buffer.Current;
      end;
    ',':
      begin
        if LCache.Length > 0 then
          CacheToResult();
      end;
    else
      LCache := LCache + Buffer.Current;
    end;

    if not Buffer.NextNoCrLf() then
      break;
  end;

  // force an error
  if FInStr then
    exit;

  if buffer.EOF then
  begin
    if LCache.Length > 0 then
      CacheToResult();
  end;
end;

class function TAsmToolbox.ValidateInstruction(ThisName: string): integer;
begin
  result := -1;
  ThisName := ThisName.ToLower().Trim();
  if ThisName.Length > 0 then
  begin
    for var item in __Instr_info do
    begin
      if item.name = thisname then
      begin
        result := item.Opcode;
        break;
      end;
    end;
  end;
end;

class function TAsmToolbox.ValidBinChars(const Text: string): boolean;
var
  TextLen:  integer;
  x:        integer;
begin
  TextLen := length(Text);
  if TextLen > 0 then
  begin
    for x := 1 to TextLen do
    begin
      result := (Text[x] in ['0'..'9', 'a'..'f', 'A'..'F']);
      if not result then
        break;
    end;
  end else
    result := false;
end;

class function TAsmToolbox.ValidHexChars(const Text: string): boolean;
begin
  var TextLen := length(Text);
  if TextLen > 0 then
  begin
    for var x := 1 to TextLen do
    begin
      result := (Text[x] in ['0'..'9', 'A'..'Z', 'a'..'z']);
      if not result then
        break;
    end;
  end else
    result := false;
end;

class function TAsmToolbox.ValidDecChars(const Text: string): boolean;
begin
  var TextLen := length(Text);
  if TextLen > 0 then
  begin
    for var x := 1 to TextLen do
    begin
      result := (Text[x] in ['0'..'9']);
      if not result then
        break;
    end;
  end else
    result := false;
end;

class function TAsmToolbox.ValidLabelChars(Text: string): boolean;
begin
  if (Text[1] in ['A'..'Z', 'a'..'z', '_']) then
  begin
    delete(Text, 1, 1);
    for var character in text do
    begin
      result := character in ['0'..'9', 'A'..'Z', 'a'..'z', '_'];
      if not result then
        break;
    end;
  end;
end;

class function TAsmToolbox.ValidConstText(const Text: string): boolean;
begin
  for var character in text do
  begin
    result := character in ['0'..'9', 'A'..'Z', 'a'..'z', '_'];
    if not result then
      break;
  end;
end;

//#############################################################################
// TAssembler
//#############################################################################

constructor TAssembler.Create;
begin
  inherited Create;
  FBuffer := TBinaryData.Create;
end;

destructor TAssembler.Destroy;
begin
  FBuffer.free;
  inherited;
end;

procedure TAssembler.Clear;
begin
  FBuffer.Release();
end;

procedure TAssembler.Assemble(const Context: TAsmParserContext; Data: TAsmInstrArray);
var
  ParamCache: array of TParamInfo;

begin
  if context <> nil then
  begin
    if Data.length > 0 then
    begin
      var Filemodel := TAsmModelSourceFile(Context.Model);
      if FileModel <> nil then
      begin

        FContext := Context;
        FModel := FileModel;

        // process each parsed / tokenized instruction
        for var x := low(Data) to high(data) do
        begin

          // Keep the byte-offset for where the
          // instruction is written inside the target buffer
          Data[x].aiOffset := FBuffer.Size;

          // Include-file injection-point?
          // If so, skip it
          if Data[x].aiOpcode = TCPUInstruction.ocInclude then
          begin
            {$IFDEF DEBUG_OUT}
            var LIncFileName := Data[x].aiParams[0].SymbolValue;
            writeln("Include-instruction for file:" + Data[x].aiParams[0].SymbolValue);
            {$ENDIF}
            continue;
          end;

          // Label injection point? If so, populate the label in the model
          // with the correct offset so that jump instructions can be
          // written with correct offsets without the need for a second pass
          if Data[x].aiOpcode = TCPUInstruction.ocLabel then
          begin
            var labObj := FileModel.Labels[Data[x].aiLineNr];
            labObj.ByteOffset := FBuffer.Size;

            {$IFDEF DEBUG_OUT}
            writelnF("Mapped offset for label [%s] to %s",
            [labObj.Name, IntToHex(labObj.ByteOffset, 8)]);
            {$ENDIF}
            continue;
          end;

          ParamCache.Clear();
          if Data[x].aiParams.length > 0 then
          begin
            for var epar in Data[x].aiParams do
            begin
              ParamCache.Add( EPar.ToParamInfo(
              function (const NamedConstant: string): variant
              begin
                {$IFDEF DEBUG_OUT}
                writeln("Paramcache asked about constant:" + NamedConstant);
                {$ENDIF}
                var cobj := Filemodel.GetNamedConstant(NamedConstant);
                if cobj <> nil then
                begin
                  {$IFDEF DEBUG_OUT}
                  writeln("constant found, text is: " + cObj.Value);
                  {$ENDIF}
                  result := cObj.Value;
                end;
              end));
            end;
          end;

          case Data[x].aiOpcode of
          ocJMP:    cJMP(ParamCache[0].piData);
          ocJSR:    cJSR(ParamCache[0].piData);
          ocBNE:    cBNE(ParamCache[0].piData);
          ocBEQ:    cBEQ(ParamCache[0].piData);
          ocBLE:    cBLE(ParamCache[0].piData);
          ocBGT:    cBGT(ParamCache[0].piData);

          ocSys:    cSYS(ParamCache[0].piData);
          ocMove:   cMove(ParamCache[0], ParamCache[1]);
          ocPUSH:   cPush(ParamCache[0]);
          ocPOP:    cPop(ParamCache[0]);
          ocADD:    cAdd(ParamCache[0], ParamCache[1]);
          ocSUB:    cSub(ParamCache[0], ParamCache[1]);
          ocCMP:    cCmp(ParamCache[0], ParamCache[1]);
          ocAlloc:  cAlloc(ParamCache[0], ParamCache[1]);
          ocFree:   cVFree(ParamCache[0]);

          ocRTS:    cRTS;
          ocNOOP:   cNOOP;
          ocMUL:    cMul(ParamCache[0], ParamCache[1]);
          ocDIV:    cDIV(ParamCache[0], ParamCache[1]);
          ocMOD:    cMod(ParamCache[0], ParamCache[1]);
          ocLSR:    cLSR(ParamCache[0], ParamCache[1]);
          ocLSL:    cLSL(ParamCache[0], ParamCache[1]);
          ocBSet:   cBSet(ParamCache[0], ParamCache[1]);
          ocBTst:   cbtst(ParamCache[0], ParamCache[1]);
          ocBClr:   cbclr(ParamCache[0], ParamCache[1]);
          ocAnd:    cAnd(ParamCache[0], ParamCache[1]);
          ocOr:     cOR(ParamCache[0], ParamCache[1]);
          ocNot:    cNOT(ParamCache[0], ParamCache[1]);
          ocXOR:    cXOR(ParamCache[0], ParamCache[0]);
          end;
        end;

        // The second pass is to patch up all jumps and branches
        // with the correct offsets, which is now known from the first one

        for var x := low(Data) to high(data) do
        begin
          case data[x].aiOpcode of
          ocJMP, ocJSR, ocBNE, ocBEQ, ocBLE, ocBGT:
            begin
              var labName := Data[x].aiParams[0].SymbolValue;
              var labObj := FileModel.GetLabelByName(LabName);
              var LJumpTarget := LabObj.ByteOffset;
              var LWriteOffset := data[x].aiOffset + 4;

              {$IFDEF DEBUG_OUT}
              writeln("Patching branch at offset #" + data[x].aiOffset.ToString() + ' for jump to #' + LJumpTarget.ToString());
              {$ENDIF}

              // Write correct offset
              FBuffer.WriteInt32(LWriteOffset, LJumpTarget); // was write()
            end;
          end;
        end;

      end else
      raise Exception.Create('Assemble failed, context-model not assigned to context error');
    end;
  end else
  raise Exception.Create('Assemble failed, context was nil error');
end;

procedure TAssembler.Emit(const Bytes: TByteArray);
begin
  if Bytes.Length > 0 then
    FBuffer.AppendBytes( Bytes );
end;

procedure TAssembler.EmitConst(const Index: longword);
begin
  Emit( FBuffer.UInt32ToBytes(Index) );
end;

procedure TAssembler.EmitIndex(const Index: longword);
begin
  Emit( FBuffer.UInt32ToBytes(Index) );
  //Emit( FBuffer.UInt16ToBytes(Index) );
end;

procedure TAssembler.EmitLength(const ThisLength: byte);
begin
  Emit( [ FBuffer.InitInt08(ThisLength) ] );
end;

procedure TAssembler.cVFree(const id: TParamInfo);
const
  CNT_INVALID_DataType = 'VFree generate failed (invalid datatype) '
                     + 'expected register, variable, constant or data error';
var
  LLayout: TOpCodeLayout;
begin
  if (id.piType in [pkRegister, pkValue, pkVariable, pkConst]) then
  begin
    case id.piType of
    pkRegister: LLayout := clRegOnly;
    pkValue:    LLayout := clDatOnly;
    pkVariable: LLayout := clVarOnly;
    pkConst:    LLayout := clCntOnly;
    end;

    var LCODE := TAsmToolbox.Encode(
        TCPUInstruction.ocFree,
        LLayout,
        if id.PIPtr then ckReference else ckDirect,
        ckVoid);

    Emit( FBuffer.UInt32ToBytes(LCODE) );

    case id.piType of
    pkValue:
      begin
        var DData := FBuffer.VariantToBytes(Id.PIData);
        EmitLength( DData.Length );
        Emit( DData );
      end;
    pkConst:  EmitConst( Id.PIIndex);
    else      EmitIndex( Id.PIIndex );
    end;

  end else
  raise EAssemblerError.Create(CNT_INVALID_DataType);
end;

procedure TAssembler.cAlloc(const id: TParamInfo; const &DataType: TParamInfo);
const
  CNT_INVALID_ID = 'Alloc generate failed, invalid id, expected '
                     + 'register, variable, constant or data';

  CNT_INVALID_DataType = 'Alloc generate failed, invalid datatype source, '
                     + 'expected register, variable, constant or data';
begin
  if (id.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
  begin
    if (&DataType.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocAlloc,
          MkInstLayout(Id, &datatype),
          if id.PIPtr then ckReference else ckDirect,
          if &DataType.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      case id.piType of
      pkValue:
        begin
          var DData := FBuffer.VariantToBytes(id.PIData);
          EmitLength( ddata.length );
          Emit( DData );
        end;
      pkConst:  EmitConst(id.piIndex);
      else      EmitIndex(id.PIIndex);
      end;

      case &Datatype.piType of
      pkValue:
        begin
          var DData := FBuffer.VariantToBytes(&Datatype.PIData);
          EmitLength( ddata.length );
          Emit( DData );
        end;
      pkConst:  EmitConst(&Datatype.piIndex);
      else      EmitIndex(&Datatype.PIIndex);
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_DataType);
  end else
  raise EAssemblerError.Create(CNT_INVALID_ID);
end;

procedure TAssembler.cSub(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'SUB generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'SUB generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkConst, pkRegister, pkVariable, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocSUB,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex( Target.piIndex );

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cMul(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'MUL generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'MUL generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkConst, pkRegister, pkVariable, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocMUL,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex( target.piIndex );

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cDiv(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'DIV generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'DIV generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkConst, pkRegister, pkVariable, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocDIV,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex( target.piIndex );

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cAnd(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'AND generate failed, invalid target, expected '
                     + 'register or variable';

  CNT_INVALID_SOURCE = 'AND generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkConst, pkRegister, pkVariable, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocAND,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex( Target.piIndex );

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cOR(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'OR generate failed, invalid target, expected '
                     + 'register or variable';

  CNT_INVALID_SOURCE = 'OR generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkConst, pkRegister, pkVariable, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocOR,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex( Target.piIndex );

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cNot(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'NOT generate failed, invalid target, expected '
                     + 'register or variable';

  CNT_INVALID_SOURCE = 'NOT generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocNOT,
          if target.piType = pkRegister then clRegOnly else clVarOnly,
          if Target.PIPtr then ckReference else ckDirect,
          ckVoid);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      EmitIndex(target.piIndex);

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cXOR(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'XOR generate failed, invalid target, expected '
                     + 'register or variable';

  CNT_INVALID_SOURCE = 'XOR generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocXOR,
          if target.piType = pkRegister then clRegOnly else clVarOnly,
          if Target.PIPtr then ckReference else ckDirect,
          ckVoid);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      EmitIndex(target.piIndex);

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cMod(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'MOD generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'MOD generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocMOD,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex( target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cLsR(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'LSR generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'LSR generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocLSR,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(Target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cLsL(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'LSL generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'LSL generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocLSL,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(Target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cBtst(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'BSET generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'BSET generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocBSet,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cBset(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'BTST generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'BTST generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocBTst,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cBclr(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'BCLR generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'BCLR generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocBClr,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(Target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cPop(const Target: TParamInfo);
const
  CNT_INVALID_TARGET = 'Pop generate failed, invalid target, '
                     + 'expected register or variable to receive data';
var
  LLayout:  TOpCodeLayout;
begin
  if (Target.piType in [pkRegister, pkVariable]) then
  begin

    case Target.piType of
    pkRegister: LLayout := TOpCodeLayout.clRegOnly;
    pkVariable: LLayout := TOpCodeLayout.clVarOnly;
    end;

    // Generate the cpu-instruction
    var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocPUSH,
      LLayout,
      if Target.PIPtr then ckReference else ckDirect,
      if Target.PIPtr then ckReference else ckDirect
    );

    // write instruction bytes
    Emit( FBuffer.UInt32ToBytes(LCode) );

    // Write reg / var index
    EmitIndex(Target.piIndex);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cPush(const Source: TParamInfo);
const
  CNT_INVALID_SOURCE = 'Push generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
var
  LLayout:  TOpCodeLayout;
begin
  if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
  begin

    case Source.piType of
    pkRegister: LLayout := TOpCodeLayout.clRegOnly;
    pkVariable: LLayout := TOpCodeLayout.clVarOnly;
    pkConst:    LLayout := TOpCodeLayout.clCntOnly;
    pkValue:    LLayout := TOpCodeLayout.clDatOnly;
    end;

    // Generate the cpu-instruction
    var LCODE := TAsmToolbox.Encode(TCPUInstruction.ocPUSH, LLayout,
      if Source.PIPtr then ckReference else ckDirect,
      if Source.PIPtr then ckReference else ckDirect);

    // write instruction bytes
    Emit( FBuffer.UInt32ToBytes(LCode) );

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:
        EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

  end else
  raise EAssemblerError.Create(CNT_INVALID_SOURCE);
end;

procedure TAssembler.cCMP(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'ADD generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'ADD generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocCMP,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(Target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkConst:      EmitConst( Source.piIndex );
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cAdd(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'ADD generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'ADD generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocADD,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(Target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case source.piType of
      pkRegister,
      pkVariable:   EmitIndex( Source.piIndex );
      pkConst:      EmitConst( Source.piIndex );
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( DData );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cMove(const Target: TParamInfo; const Source: TParamInfo);
const
  CNT_INVALID_TARGET = 'Move generate failed, invalid target, expected '
                     + 'register, variable or DC';

  CNT_INVALID_SOURCE = 'Move generate failed, invalid source, '
                     + 'expected register, variable, constant or data';
begin
  if (target.piType in [pkRegister, pkVariable, pkDC]) then
  begin
    if (source.piType in [pkRegister, pkVariable, pkConst, pkValue]) then
    begin
      // Generate the cpu-instruction
      var LCODE := TAsmToolbox.Encode(
          TCPUInstruction.ocMOVE,
          MkInstLayout(Target, Source),
          if Target.PIPtr then ckReference else ckDirect,
          if Source.PIPtr then ckReference else ckDirect);

      // write instruction bytes
      Emit( FBuffer.UInt32ToBytes(LCode) );

      // If the target is DC, thats already defined in the
      // instruction layout. No need to write any data.
      // Otherwise, its all index based, so we just dump the index
      if (target.piType <> pkDC) then
        EmitIndex(Target.piIndex);

      // Since source can be a variant, we need to differenciate that
      // and write the bytes. Otherwise, its again just index based and
      // the meaning declared in the instruction layout byte
      case Source.piType of
      pkRegister,
      pkVariable: EmitIndex(Source.piIndex);
      pkConst:    EmitConst(Source.piIndex);
      pkValue:
        begin
          var ddata := FBuffer.VariantToBytes(Source.PIData);
          EmitLength(ddata.length);
          Emit( ddata );
        end;
      end;

    end else
    raise EAssemblerError.Create(CNT_INVALID_SOURCE);
  end else
  raise EAssemblerError.Create(CNT_INVALID_TARGET);
end;

procedure TAssembler.cJMP(Offset: integer);
begin
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocJMP,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckDirect,
      TAsmParameterMode.ckVoid);

  Emit( FBuffer.UInt32ToBytes(LCODE) );
  Emit( FBuffer.UInt32ToBytes(Offset));
end;

procedure TAssembler.cJsr(Offset: integer);
begin
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocJSR,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckDirect,
      TAsmParameterMode.ckVoid);

  Emit( FBuffer.UInt32ToBytes(LCODE) );
  Emit( FBuffer.UInt32ToBytes(Offset));
end;

procedure TAssembler.cBle(Offset: integer);
begin
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocBLE,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckDirect,
      TAsmParameterMode.ckVoid);
  Emit( FBuffer.UInt32ToBytes(LCODE) );
  Emit( FBuffer.UInt32ToBytes(Offset));
end;

procedure TAssembler.cBgt(Offset: integer);
begin
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocBGT,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckDirect,
      TAsmParameterMode.ckVoid);
  Emit( FBuffer.UInt32ToBytes(LCODE) );
  Emit( FBuffer.UInt32ToBytes(Offset));
end;

procedure TAssembler.cBne(Offset: integer);
begin
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocBNE,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckDirect,
      TAsmParameterMode.ckVoid);
  Emit( FBuffer.UInt32ToBytes(LCODE) );
  Emit( FBuffer.UInt32ToBytes(Offset));
end;

procedure TAssembler.cBeq(Offset: integer);
begin
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocBEQ,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckDirect,
      TAsmParameterMode.ckVoid);
  Emit( FBuffer.UInt32ToBytes(LCODE) );
  Emit( FBuffer.UInt32ToBytes(Offset));
end;

procedure TAssembler.cNOOP;
begin
  // Generate the opcode, expect first parameter to be register
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocNOOP,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckVoid,
      TAsmParameterMode.ckVoid);

  Emit( FBuffer.UInt32ToBytes(LCODE) );
end;

procedure TAssembler.cRts;
begin
  // Generate the opcode, expect first parameter to be register
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocRTS,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckVoid,
      TAsmParameterMode.ckVoid);
  Emit( FBuffer.UInt32ToBytes(LCODE) );
end;

procedure TAssembler.cSYS(const procId: integer);
begin
  var LCODE := TAsmToolbox.Encode(
      TCPUInstruction.ocSys,
      TOpCodeLayout.clVoid,
      TAsmParameterMode.ckDirect,
      TAsmParameterMode.ckVoid);

  Emit(FBuffer.UInt32ToBytes(LCode));
  Emit(FBuffer.UInt32ToBytes(procId));
end;

initialization
begin
  TAsmToolbox.RegisterInstruction('alloc',  2,  TCPUInstruction.ocAlloc);
  TAsmToolbox.RegisterInstruction('vfree',  1,  TCPUInstruction.ocFree);
  TAsmToolbox.RegisterInstruction('move',   2,  TCPUInstruction.ocMOVE);
  TAsmToolbox.RegisterInstruction('push',   1,  TCPUInstruction.ocPUSH);
  TAsmToolbox.RegisterInstruction('pop',    1,  TCPUInstruction.ocPOP);
  TAsmToolbox.RegisterInstruction('add',    2,  TCPUInstruction.ocADD);
  TAsmToolbox.RegisterInstruction('sub',    2,  TCPUInstruction.ocSUB);
  TAsmToolbox.RegisterInstruction('mul',    2,  TCPUInstruction.ocMUL);
  TAsmToolbox.RegisterInstruction('div',    2,  TCPUInstruction.ocDIV);
  TAsmToolbox.RegisterInstruction('mod',    2,  TCPUInstruction.ocMOD);
  TAsmToolbox.RegisterInstruction('lsl',    2,  TCPUInstruction.ocLSL);
  TAsmToolbox.RegisterInstruction('lsr',    2,  TCPUInstruction.ocLSR);
  TAsmToolbox.RegisterInstruction('btst',   2,  TCPUInstruction.ocBTst);
  TAsmToolbox.RegisterInstruction('bset',   2,  TCPUInstruction.ocBSet);
  TAsmToolbox.RegisterInstruction('bclr',   2,  TCPUInstruction.ocBClr);
  TAsmToolbox.RegisterInstruction('and',    2,  TCPUInstruction.ocAnd);
  TAsmToolbox.RegisterInstruction('or',     2,  TCPUInstruction.ocOr);
  TAsmToolbox.RegisterInstruction('not',    1,  TCPUInstruction.ocNot);
  TAsmToolbox.RegisterInstruction('xor',    1,  TCPUInstruction.ocXOR);
  TAsmToolbox.RegisterInstruction('cmp',    2,  TCPUInstruction.ocCMP);
  TAsmToolbox.RegisterInstruction('noop',   0,  TCPUInstruction.ocNOOP);

  TAsmToolbox.RegisterInstruction('jsr',    1,  TCPUInstruction.ocJSR);
  TAsmToolbox.RegisterInstruction('jmp',    1,  TCPUInstruction.ocJMP);

  TAsmToolbox.RegisterInstruction('bne',    1,  TCPUInstruction.ocBNE);
  TAsmToolbox.RegisterInstruction('beq',    1,  TCPUInstruction.ocBEQ);
  TAsmToolbox.RegisterInstruction('ble',    1,  TCPUInstruction.ocBLE);
  TAsmToolbox.RegisterInstruction('bgt',    1,  TCPUInstruction.ocBLE);

  TAsmToolbox.RegisterInstruction('rts',    0,  TCPUInstruction.ocRTS);
  TAsmToolbox.RegisterInstruction('sys',    1,  TCPUInstruction.ocSys);
end;

end.
