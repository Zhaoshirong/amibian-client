unit vmachine.cpu;

interface

{$I 'vmachine.inc'}

uses
  {$IFDEF DEBUG_OUT}
  SmartCL.System,
  {$ENDIF}
    System.Types
  , System.Types.Convert
  , System.Streams
  , System.Stream.Reader
  , System.Stream.Writer
  , System.Memory.Allocation
  , System.Memory.Buffer
  , System.dictionaries
  , vmachine.types
  , vmachine.data
  ;

type

  // Exception classes
  EVirtualMachine = class(EW3Exception);

  // Forward declarations
  TVirtualMachine = class;

  TSysCallMethod = procedure (const CPU: TVirtualMachine);

  TSysCalls = class(TObject)
  private
    FLut:       variant;
    FCPU:       TVirtualMachine;
  public
    procedure   RegisterCall(const Id: integer; const Callback: TSysCallMethod);

    procedure   Invoke(const Id: integer); overload;
    procedure   Invoke(const Id: integer; const ToStack: array of const); overload;

    constructor Create(const CPU: TVirtualMachine); virtual;
    destructor  Destroy; override;
  end;

  TDataBlock = class(TObject)
  private
    FData:      TBinaryData;
  public
    property    Id: integer;
    property    Data: TBinaryData read FData;
    constructor Create;virtual;
    destructor  Destroy;override;
  end;

  TDataControl = class(TObject)
  private
    FBlocks:    array of TDataBlock;
    FLUT:       variant;
  public
    property    Block[const Index: integer]: TDataBlock read (FBlocks[index]); default;
    property    BlockCount: integer read (FBlocks.length);

    function    GetBlockById(const id: integer): TDataBlock; virtual;
    function    Add(const id: integer): TDataBlock;overload;
    procedure   Delete(const Index: integer);
    procedure   DeleteById(const id: integer);
    procedure   Clear;
    constructor Create;virtual;
    destructor  Destroy;override;
  end;

  TCPUInstrEntry = procedure (const Layout, ParamA, ParamB: byte);

  TInstrInfo = record
    itId:     byte;
    itEntry:  TCPUInstrEntry;
  end;

  (* Changes to the CPU:
     ===================
     If the instruction-set is known, with a fixed number of instructions,
     identifiers and entrypoints - this allows the cpu to execute these
     very quickly.
     However, being able to register instructions and their entrypoints
     makes it much easier to work with the cpu.
     As of 22.09.2016 I have added the ability to register instructions
     and their entrypoints.

     Internally the class stores everything in a normal array. It uses a
     range to keep track of the lowest and highest values for the opcodes
     it knows.

     When the CPU is initialized, it will take this array and build a lookup
     table. During execution it will use the lookup table (LUT) which is
     naturally very fast. *)

  TCPUCompareFlag = set of (ccEqual, ccLess, ccGreater);

  TVMDebugOutEvent = procedure (Sender: TObject; Text: string);

  TVirtualMachine = class(TObject)
  private
    FBuffer:    TBinaryData;
    FRegisters: TCPURegisters;
    FStack:     TCPUStack;
    FVariables: TCPUVarStorage;
    FSystem:    TSysCalls;
    FConstants: TASMConstants;
    FData:      TDataControl;
    FPC:        longword;
    FDC:        longword;

    FJumpStack: array of integer;

    (*  Instruction set opcode range
        Opcodes are linear values, and they form a range that can be checked.
        If an instruction is outside this range, there is something wrong
        with the bytecode - or we have read beyond the program, or the runtime
        method implementation has not read all the data. *)
    FInstCodeRange:    TRange;

    (* List of callback to each bytecode instruction implementation.
       Which is the code that is executed and that each bytecode represents.
       It is stored in a simple array *)
    FITable:    Array of TInstrInfo;

    (* This is a lookup table for quickly finding the index of an opcode.
       This index is used with the FITable array (above) to get the entry
       address (reference) to the instruction implementation. *)
    FILUT:      variant;

    (* A simple flag that says you need to rebuild the instruction table.
       This also calculates the min-max range value that we use to check
       if an instruction is valid or not *)
    FIReBuildNeeded: boolean;
  protected
    function    LoadIndexValue: longword;
    function    LoadConstValue: longword;
    function    LoadLengthValue: byte;

    function    LoadAndReadReg: variant;
    function    LoadAndReadVar: variant;
    function    LoadAndReadConst: variant;
    function    LoadAndReadValue: variant;
    function    LoadAndRead(const Layout: byte): TVarArray;
    procedure   ESysCall(const layout, paramA, paramB: byte); virtual;
  public
    property    PC: longword read FPC write FPC;
    property    DC: longword read FDC write FDC;
    property    CompareFlag: TCPUCompareFlag;
    property    Stack: TCPUStack read FStack;
    property    Registers: TCPURegisters read FRegisters;
    property    Variables: TCPUVarStorage read FVariables;
    property    Constants: TASMConstants read FConstants;
    property    DataControl: TDataControl read FData;

    property    Buffer: TBinaryData read Fbuffer;
    property    System: TSysCalls read FSystem;

    procedure   RegisterInstr(const Opcode: word; const EntryPoint: TCPUInstrEntry);
    function    InstrExists(const Opcode: word): boolean;
    procedure   Execute(const Opcode: word; const Layout, ParamA, ParamB: byte);
    procedure   RebuildInstructionTable;

    procedure   ENOOP(const layout,paramA,paramB:byte);
    procedure   EVar(const layout,paramA,paramB:byte);
    procedure   EVFree(const layout,paramA,paramB:byte);
    procedure   ESub(const layout, paramA, paramB: byte);
    procedure   EPush(const layout, paramA, paramB: byte);
    procedure   EPop(const layout, paramA, paramB: byte);
    procedure   EMove(const layout, paramA, paramB: byte);

    procedure   ECmp(const layout, paramA, paramB: byte);   // Compareflag logic

    procedure   EAdd(const layout, paramA, paramB: byte);

    procedure   EMul(const layout, paramA, paramB: byte);
    procedure   EDiv(const layout, paramA, paramB: byte);
    procedure   EMod(const layout, paramA, paramB: byte);
    procedure   ELsR(const layout, paramA, paramB: byte);
    procedure   ELsL(const layout, paramA, paramB: byte);
    procedure   EBtst(const layout, paramA, paramB: byte);  // Compareflag logic
    procedure   EBset(const layout, paramA, paramB: byte);
    procedure   EBclr(const layout, paramA, paramB: byte);

    procedure   EAND(const layout, paramA, paramB: byte);
    procedure   EOR(const layout, paramA, paramB: byte);
    procedure   ENOT(const layout, paramA, paramB: byte);
    procedure   EXOR(const layout, paramA, paramB: byte);

    procedure   ERTS(const layout, paramA, paramB: byte);

    procedure   EJMP(const layout, paramA, paramB: byte);
    procedure   EJSR(const layout, paramA, paramB: byte);

    // Branching based on compare-flag state
    procedure   eBEQ(const layout, paramA, paramB: byte);
    procedure   eBNE(const layout, paramA, paramB: byte);
    procedure   eBLE(const layout, paramA, paramB: byte);
    procedure   eBGT(const layout, paramA, paramB: byte);

    function    Step:Boolean;
    procedure   CoreDump;

    constructor Create; virtual;
    destructor  Destroy;Override;

    property    OnDebugOut: TVMDebugOutEvent;

  end;

function CompareFlagToString(const Flag:   TCPUCompareFlag): string;

implementation

uses vmachine.assembler;

function CompareFlagToString(const Flag:   TCPUCompareFlag): string;
begin
  if ccEqual in Flag then result += 'ccEqual ';
  if ccLess in Flag then result += 'ccLess ';
  if ccGreater in Flag then result += 'ccGreater';
end;

//#############################################################################
// TSysCalls
//#############################################################################

constructor TSysCalls.Create(const CPU:TVirtualMachine);
begin
  inherited Create;
  FCPU := CPU;
  FLut := TVariant.CreateObject;
end;

destructor TSysCalls.Destroy;
begin
  FLut := null;
  inherited;
end;

procedure TSysCalls.RegisterCall(const Id: integer;
  const Callback: TSysCallMethod);
begin
  if not FLut.hasOwnProperty(id) then
    FLut[id] := @callback;
end;

procedure TSysCalls.Invoke(const Id: integer;
  const ToStack: array of const);
var
  LRef: TSysCallMethod;
begin
  // Lookup syscall in lookup-table
  var LTemp := FLut[id];

  // Known syscall?
  if (LTemp) then
  begin
    // Typecast [sigh]
    asm
    @LRef = @LTemp;
    end;

    // Check for nil. Yes this is JavaScript
    if assigned(LRef) then
    begin
      // Push elements to stack if any
      for var el in ToStack do
      begin
        FCPU.Stack.Push(el);
      end;

      // And call the sucker
      LRef(FCPU);
    end else
    begin
      {$IFDEF DEBUG_OUT}
      writeln("Syscall #" + IntToHex(id,2) + " callback is nil error")
      {$ENDIF}
    end;
  end else
  begin
    {$IFDEF DEBUG_OUT}
    writeln("Syscall #" + IntToHex(id,2) + " does not have a callback handler");
    {$ENDIF}
  end;
end;

procedure TSysCalls.Invoke(const id:integer);
var
  LRef: TSysCallMethod;
begin
  var LTemp := FLut[id];
  if (LTemp) then
  begin
    asm
    @LRef = @LTemp;
    end;

    if assigned(LRef) then
      LRef(FCPU)
    else
    begin
      {$IFDEF DEBUG_OUT}
      writeln("Syscall #" + IntToHex(id,2) + " callback is nil error");
      {$ENDIF}
    end;
  end else
  begin
    {$IFDEF DEBUG_OUT}
    writeln("Syscall #" + IntToHex(id,2) + " does not have a callback handler");
    {$ENDIF}
  end;
end;

//#############################################################################
// TVirtualMachine
//#############################################################################

constructor TVirtualMachine.Create;
begin
  inherited Create;
  FStack := TCPUStack.Create;
  FBuffer := TBinaryData.Create;
  FRegisters := TCPURegisters.Create(16);
  FVariables := TCPUVarStorage.Create;
  FConstants := TASMConstants.Create;
  FSystem := TSysCalls.Create(self);
  FData := TDataControl.Create;

  // The syscall is universal, meaning that it will remain the same
  // not matter what instruction-set you implement
  RegisterInstr(ocSys,    @ESysCall);
  RegisterInstr(ocAlloc,  @EVar);
  RegisterInstr(ocFree,   @EVFree);
  RegisterInstr(ocMOVE,   @EMOVE);
  RegisterInstr(ocPUSH,   @EPush);
  RegisterInstr(ocPOP,    @EPop);
  RegisterInstr(ocSub,    @ESub);
  RegisterInstr(ocCMP,    @ECmp);
  RegisterInstr(ocNOOP,   @ENOOP);
  RegisterInstr(ocMul,    @EMul);
  RegisterInstr(ocDiv,    @EDiv);
  RegisterInstr(ocLSR,    @ELSR);
  RegisterInstr(ocLSL,    @ELSL);
  RegisterInstr(ocBTST,   @EBTST);
  RegisterInstr(ocBSET,   @eBSET);
  RegisterInstr(ocBCLR,   @eBCLR);
  RegisterInstr(ocAND,    @EAND);
  RegisterInstr(ocOR,     @EOR);
  RegisterInstr(ocNOT,    @eNOT);
  RegisterInstr(ocXOR,    @eXOR);
  RegisterInstr(ocJMP,    @eJMP);
  RegisterInstr(ocJSR,    @eJSR);
  RegisterInstr(ocRTS,    @eRTS);

  RegisterInstr(ocAdd, @EAdd);

  RegisterInstr(ocBEQ,    @eBEQ);
  RegisterInstr(ocBNE,    @eBNE);
  RegisterInstr(ocBLE,    @eBLE);
  RegisterInstr(ocBGT,    @eBGT);

  System.RegisterCall($44, procedure (const CPU: TVirtualMachine)
  begin
    {$IFDEF DEBUG_OUT}
    writeln("Syscall $44 called");
    {$ENDIF}
  end);

end;

destructor TVirtualMachine.Destroy;
begin
  FSystem.free;
  Fbuffer.free;

  FRegisters.Clear();
  FRegisters.free;
  FRegisters := nil;

  FStack.Clear();
  FStack.free;
  FStack := nil;

  FVariables.Clear();
  FVariables.free;
  FVariables := nil;

  FConstants.free;
  FData.free;
  inherited;
end;

uses SmartCL.System;

function TVirtualMachine.Step: boolean;
begin
  //Check if program-counter [offset] is beyond code-buffer.
  //If it is, we should throw a seg-fault error
  if PC >= buffer.Size then
  begin
    // We dont raise an exception here, because this is a natural
    // stopping point
    exit;
  end;

  // Read the instruction
  var LFetch := Buffer.ReadLong(PC);
  FPC += Buffer.SizeOfType(itInt32);

  var LOpcode := lFetch and $0000FFFF;
  var LLayout := lFetch and $00FF0000 shr 16;
  var LParamA := (lFetch and $FF000000 shr 24) shr 4;
  var LParamB := (lFetch and $FF000000 shl 4) shr 28;

  try
    Execute(LOpcode, LLayout, LParamA, LParamB);
  except
    on e: exception do
      raise EVirtualMachine.CreateFmt('Failed to execute instruction [%d]: %s', [LOpCode, e.message]);
  end;

  result := true;
end;

procedure TVirtualMachine.ENOOP(const layout, ParamA, ParamB:byte);
begin
  // no operation
end;

function TVirtualMachine.LoadIndexValue: longword;
begin
  result := Buffer.ReadLong(PC);
  FPC += Buffer.SizeOfType(itInt32);
end;

function TVirtualMachine.LoadConstValue: longword;
begin
  result := Buffer.ReadLong(PC);
  FPC += Buffer.SizeOfType(itInt32);
end;

function TVirtualMachine.LoadLengthValue: byte;
begin
  var LLen := Buffer.SizeOfType(itByte);
  result := Buffer.ReadBytes(PC, LLen)[0];
  FPC += LLen;
end;

function TVirtualMachine.LoadAndReadReg: variant;
begin
  // Get REG identifier
  var RegId := LoadIndexValue();

  // Return value in register
  result := FRegisters.Read(RegId);
end;

function TVirtualMachine.LoadAndReadVar: variant;
begin
  // Get VAR identifier
  var RegId := LoadIndexValue();

  // Return value in Var
  result := FVariables.Read(RegId);
end;

function TVirtualMachine.LoadAndReadConst: variant;
begin
  // Get CONST identifier
  var RegId := LoadConstValue();

  // Return value in CONST
  result := FConstants.Read(RegId);
end;

function TVirtualMachine.LoadAndReadValue: variant;
begin
  var DataLen  := LoadLengthValue();
  if DataLen > 0 then
  begin
    var bytes := Buffer.ReadBytes(PC, DataLen);
    result := Buffer.BytesToVariant(bytes);
    FPC += DataLen;
  end;
end;

function TVirtualMachine.LoadAndRead(const Layout: byte): TVarArray;
begin
  case Layout of
  clRegOnly:
    begin
      result.add( LoadAndReadReg );
    end;
  clDatOnly:
    begin
      result.add( LoadAndReadValue );
    end;
  clCntOnly:
    begin
      result.add( LoadAndReadConst );
    end;
  clVarOnly:
    begin
      result.add( LoadAndReadVar );
    end;
  clReg_Value:
    begin
      result.add( LoadAndReadReg );
      result.add( LoadAndReadValue );
    end;
  clValue_Value:
    begin
      result.add( LoadAndReadValue );
      result.add( LoadAndReadValue );
    end;
  clReg_Reg:
    begin
      result.add( LoadAndReadReg );
      result.add( LoadAndReadReg );
    end;
  clReg_Var:
    begin
      result.add( LoadAndReadReg );
      result.add( LoadAndReadVar );
    end;
  clReg_Const:
    begin
      result.add( LoadAndReadReg );
      result.add( LoadAndReadConst );
    end;
  clVar_Const:
    begin
      result.add( LoadAndReadVar );
      result.add( LoadAndReadConst );
    end;
  clVar_Reg:
    begin
      result.add( LoadAndReadVar );
      result.add( LoadAndReadReg );
    end;
  clVar_Var:
    begin
      result.add( LoadAndReadVar );
      result.add( LoadAndReadVar );
    end;
  clVar_Value:
    begin
      result.add( LoadAndReadVar );
      result.add( LoadAndReadValue );
    end;
  end;
end;

procedure TVirtualMachine.EVar(const layout, ParamA, ParamB: byte);
begin
  var LayoutData := LoadAndRead(Layout);
  Variables.Alloc(
    LayoutData[0],
    TVMDataType(LayoutData[1])
    );
end;

procedure TVirtualMachine.EVFree(const layout, paramA, paramB: byte);
begin
  var LayoutData := LoadAndRead(Layout);
  Variables.Release(TVariant.AsInteger(LayoutData[0]));
  {$IFDEF DEBUG_OUT}
  writeln('VFREE #' + TVariant.AsString(LayoutData[0]));
  {$ENDIF}
end;

procedure TVirtualMachine.ESysCall(const Layout, ParamA, ParamB: byte);
begin
  var LSysId := Buffer.ReadInt(PC);
  FPC += Buffer.SizeOfType(itInt32);
  System.Invoke(LSysId);
end;

function TVirtualMachine.InstrExists(const Opcode: byte): boolean;
begin
  if FITable.length>0 then
  begin
    for var x:=low(FITable) to high(FITable) do
    begin
      result := FITable[x].ITId = OpCode;
      if result then
      break;
    end;
  end;
end;

procedure TVirtualMachine.RegisterInstr(const Opcode: word;
  const EntryPoint: TCPUInstrEntry);
var
  LEntry: TInstrInfo;
begin
  if not InstrExists(OpCode) then
  begin

    LEntry.itId := Opcode and $0000FFFF;
    LEntry.itEntry := Entrypoint;
    FITable.Add(LEntry);

    FIReBuildNeeded := true;

  end else
  raise EW3Exception.CreateFmt
  ('Instruction %s [%d] is already registered errror',
  ['$' + OpCode.ToHexString(2), Opcode]);
end;

(* This procedure builds a JS lookup table [dictionary] from
   the registered instruction set *)
procedure TVirtualMachine.RebuildInstructionTable;
begin
  var LLow := MAX_INT;
  var LHigh := 0;
  FILut := TVariant.CreateObject;
  if FITable.Length >0 then
  begin
    for var x:=low(FITable) to high(FITable) do
    begin
      // Does this opcode affect the range?
      if FITable[x].ITId < LLow then
        LLow := FITable[x].itId;

      if FITable[x].ITId > LHigh then
        LHigh := FITable[x].itId;

      // Map entrypoint to ID in dictionary *)
      FILut[FITable[x].ITId] := FITable[x].ITEntry;
    end;
  end;

  (* Now set the range of the instruction opcodes *)
  FInstCodeRange := TRange.Create(LLow, LHigh);

  (* Mark as rebuild required *)
  FIReBuildNeeded := false;
end;

procedure TVirtualMachine.Execute(const Opcode: word; const Layout, ParamA, ParamB: byte);
var
  LExec: TCPUInstrEntry;
begin
  if FIReBuildNeeded then
  begin
    {$IFDEF DEBUG_OUT}
    writeln("You forgot to rebuild the instruction table, doing that now");
    {$ENDIF}
    RebuildInstructionTable();
  end;

  if FInstCodeRange.Within(Opcode) then
  begin
    (* Get the entrypoint untyped *)
    var temp := FILUT[Opcode];

    (* assign to typed via JS *)
    asm
      @LExec = @temp;
    end;

    (* Invoke instruction *)
    try
      //writeln("Executing: " + IntToHex(OpCode, 2));
      LExec(Layout, ParamA, ParamB);
    except
      on e: exception do
      begin
        {$IFDEF DEBUG_OUT}
        WritelnF("Opcode: $%s, thew exception %s with message [%s]",
        [Opcode.ToHexString(2), e.classname, e.message]);
        {$ELSE}
        raise EVirtualMachine.CreateFmt("Opcode: $%s, thew exception %s with message [%s]",
        [Opcode.ToHexString(2), e.classname, e.message]);
        {$ENDIF}
      end;
    end;

  end else
  Raise EW3Exception.CreateFmt
  ('Invalid instruction, opcode [%s] not in instruction-set [%d..%d] error',
  ['$' + opcode.ToHexString(2), FInstCodeRange.Minimum, FInstCodeRange.Maximum]);
end;


procedure TVirtualMachine.CoreDump;

  procedure OutText(Text: string);
  begin
    if assigned(OnDebugOut) then
      OnDebugOut(self, Text);
  end;

  procedure OutTextF(Text: string; const Values: array of const);
  begin
    OutText(format(Text, Values));
  end;

begin

  OutText('---------------------------------');
  OutText("FLAGS:");
  OutText('Compare-Flag = ' + CompareFlagToString(CompareFlag));

  OutText('---------------------------------');
  OutText('REGISTERS:');
  for var x:=0 to registers.Count-1 do
  begin
    OutText('R[' + x.tostring +']=' + TVariant.AsString(Registers.Read(x)));
  end;

  OutText('---------------------------------');
  OutText('VARIABLES:');
  variables.ForEach(
    function (const Item: TAsmVariableData): TEnumResult
    begin
      OutTextF('Id: %d   Value: %s',
        [Item.vId, TVariant.AsString(Item.vData)]);
      result := TEnumResult.erContinue; // dont stop
    end);

  OutText('---------------------------------');
  OutText("STACK:");
  for var x:=0 to FStack.Count-1 do
  begin
    OutText(x.ToHexString(2) + ':' + TVariant.Asstring( FStack.Items[x]) );
  end;

end;

//-----------------------------------------------------------------------------
// Move [target], [source]
//-----------------------------------------------------------------------------

procedure TVirtualMachine.EMOVE(const layout, paramA, paramB: byte);
var
  regid: integer;
  cntid: integer;
  varid: integer;
  DataLen: integer;
begin
  case TOpCodeLayout(layout) of
  clVar_Value:
    begin
      (* Read variable id *)
      varid := LoadIndexValue();

      (* Read the length of data ahead *)
      Datalen := LoadLengthValue();

      (* Any data at all? *)
      if DataLen > 0 then
      begin
        (* Read the raw bytes. This is a binary stored variant *)
        var RawBytes := Buffer.ReadBytes(PC, DataLen);
        FPC += DataLen;

        (* Convert from variant to intrinsic datatype, and write to variable *)
        Variables.Write(VarId, Buffer.BytesToVariant(RawBytes) );
      end else
      Variables.Write(varid, null);
    end;
  clReg_Const:
    begin
      (* Read register id *)
      regid := LoadIndexValue();

      (* Read the constant id *)
      cntid := LoadConstValue();

      (* Assign constant data to register *)
      Registers.Write(RegId, Constants.Read(cntid));
    end;
  clReg_Var:
    begin
      (* Read the register-id TARGET *)
      regid := LoadIndexValue();

      (* Read the variable-id SOURCE *)
      varid := LoadIndexValue();

      (* Write the actual value to the register *)
      Registers.Write(RegId, Variables.Read(VarId) );
    end;
  clReg_Value:
    begin
      (* Read the register-id to recieve the data *)
      regid := LoadIndexValue();

      (* Read the length of data ahead *)
      Datalen := LoadLengthValue();

      (* Any data at all? *)
      if Datalen > 0 then
      begin
        (* Read the raw bytes. This is a binary stored variant *)
        var rawbytes := buffer.ReadBytes(PC, Datalen);
        FPC += datalen;

        (* Convert from variant to intrinsic datatype, and write to register *)
        Registers.Write(RegId, Buffer.BytesToVariant(rawbytes));
      end else
      Registers.Write(RegId, null);
    end;
  end;
end;

//-----------------------------------------------------------------------------
// POP [TARGET]
//-----------------------------------------------------------------------------

procedure TVirtualMachine.EPop(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clRegOnly:
    begin
      // Read the register-id to put the data
      var regid := LoadIndexValue();
      Registers.Write(regId, Stack.Pop() );
    end;
  clVarOnly:
    begin
      var VarId := LoadIndexValue();
      Variables.Write(varId, Stack.Pop() );
    end;
  end;
end;

//-----------------------------------------------------------------------------
// PUSH [SOURCE] (To Stack)
//-----------------------------------------------------------------------------

procedure TVirtualMachine.EPush(const layout, paramA, paramB: byte);
begin
  var LayoutData := LoadAndRead(Layout);
  Stack.Push(LayoutData[0]);
end;

//-----------------------------------------------------------------------------
// Branching (branch equal, not equal, less, greater)
//-----------------------------------------------------------------------------

procedure TVirtualMachine.eBEQ(const layout, paramA, paramB: byte);
begin
  // Read entrypoint offset
  var LOffset := Buffer.ReadInt(PC);

  // Update program-counter
  FPC += Buffer.SizeOfType(itInt32);

  if (ccEqual in CompareFlag) then
  begin
    // Push re-entry offset to stack
    FJumpStack.Push(PC);

    // adjust offset to redirect execution at this point
    FPC := LOffset;
  end;
end;

procedure TVirtualMachine.eBNE(const layout, paramA, paramB: byte);
begin
  // Read entrypoint offset
  var LOffset := Buffer.ReadInt(PC);

  // Update program-counter
  FPC += Buffer.SizeOfType(itInt32);

  if not (ccEqual in CompareFlag) then
  begin
    // Push re-entry offset to stack
    FJumpStack.Push(PC);

    // adjust offset to redirect execution at this point
    FPC := LOffset;
  end;
end;

procedure TVirtualMachine.eBLE(const layout, paramA, paramB: byte);
begin
  // Read entrypoint offset
  var LOffset := Buffer.ReadInt(PC);

  // Update program-counter
  FPC += Buffer.SizeOfType(itInt32);

  if (ccLess in CompareFlag) then
  begin
    // Push re-entry offset to stack
    FJumpStack.Push(PC);

    // adjust offset to redirect execution at this point
    FPC := LOffset;
  end;
end;

procedure TVirtualMachine.eBGT(const layout, paramA, paramB: byte);
begin
  // Read entrypoint offset
  var LOffset := Buffer.ReadInt(PC);

  // Update program-counter
  FPC += Buffer.SizeOfType(itInt32);

  if (ccGreater in CompareFlag) then
  begin
    // Push re-entry offset to stack
    FJumpStack.Push(PC);

    // adjust offset to redirect execution at this point
    FPC := LOffset;
  end;
end;

//-----------------------------------------------------------------------------
// JSR, Jump subroutine
//-----------------------------------------------------------------------------

procedure TVirtualMachine.EJSR(const layout, paramA, paramB: byte);
begin
  // Read entrypoint offset
  var LOffset := Buffer.ReadLong(PC);

  // Update program-counter
  FPC += Buffer.SizeOfType(itInt32);

  // Push re-entry offset to stack
  FJumpStack.Push(FPC);

  // adjust offset to redirect execution at this point
  FPC := LOffset;
end;

//-----------------------------------------------------------------------------
// RTS, return to start [or stack]
//-----------------------------------------------------------------------------

procedure TVirtualMachine.ERTS(const layout, paramA, paramB: byte);
begin
  if FJumpStack.Length > 0 then
  begin
    var LOffset := FJumpStack.Pop();
    FPC := LOffset;
  end;
  // Fetch previous jsr addr
end;

//-----------------------------------------------------------------------------
// JMP, Jump to position (offset based on label)
//-----------------------------------------------------------------------------

procedure TVirtualMachine.EJMP(const Layout, ParamA, ParamB: byte);
begin
  var LOffset := Buffer.ReadLong(FPC);
  FPC := LOffset;
end;

procedure TVirtualMachine.EMul(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_reg:
    begin
      var VarId := LoadIndexValue();
      var RegId := LoadIndexValue();
      var Temp :=  Variables.Read(varid) * Registers.Read(RegId);
      Variables.Write(Varid, Temp);
    end;
  clReg_Var:
    begin
      var RegId := LoadIndexValue();
      var Varid := LoadIndexValue();
      var Temp :=  Registers.Read(RegId) * Variables.Read(VarId);
      Registers.Write(RegId, temp);
    end;
  clReg_Const:
    begin
      var RegId := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(RegId) * Constants.Read(ConstId);
      Registers.write(RegId, temp);
    end;
  clReg_Reg:
    begin
      var RegTarget := LoadIndexValue();
      var RegSource := LoadIndexValue();
      var temp :=  Registers.Read(RegTarget) * Registers.Read(RegSource);
      Registers.write(RegTarget, Temp);
    end;
  clReg_Value:
    begin
      var RegTarget := LoadIndexValue();
      var DataLen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var ByteData := Buffer.ReadBytes(PC,datalen);
        FPC := FPC + DataLen;

        var Temp :=  Registers.Read(RegTarget) * Buffer.BytesToVariant(ByteData);
        Registers.write(Regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.EDiv(const layout, paramA, paramB: byte);
{var
  RegSource:  integer;
  RegTarget:  integer;
  varid:      integer;
  temp:       integer;
  DataLen:    integer;
  ByteData:   TByteArray;  }
begin
  case TOpCodeLayout(layout) of
  clReg_var:
    begin
      var RegTarget := LoadIndexValue();
      var VarId := LoadIndexValue();
      var Temp :=  Registers.Read(Regtarget) div Variables.Read(Varid);
      Registers.Write(RegTarget, Temp);
    end;
  clVar_reg:
    begin
      var Varid := LoadIndexValue();
      var RegSource := LoadIndexValue();
      var temp :=  Variables.Read(VarId) div Registers.Read(RegSource);
      Variables.Write(Varid, Temp);
    end;
  clReg_Const:
    begin
      var RegTarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(RegTarget) div Constants.Read(ConstId);
      Registers.Write(RegTarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) div Registers.Read(regsource);
      Registers.write(regtarget, temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var Datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC := FPC + datalen;

        var temp :=  Registers.Read(regtarget) div Buffer.BytesToVariant(bytedata);
        Registers.Write(regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.EAND(const layout, paramA, paramB: byte);
{var
  VarId:      integer;
  Regtarget:  integer;
  RegSource:  integer;
  constid:    integer;
  Temp:       integer;
  DataLen:    integer;
  ByteData:   TByteArray;   }
begin
  case TOpCodeLayout(layout) of
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var RegSource := LoadIndexValue();
      var temp :=  Variables.Read(varid) and Registers.Read(regSource);
      Variables.write(varid, temp);
    end;
  clReg_Var:
    begin
      var Regtarget := LoadIndexValue();
      var Varid := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) and Variables.Read(varid);
      Registers.write(regtarget, temp);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(regtarget) and Constants.Read(constid);
      Registers.write(regtarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) and Registers.Read(regsource);
      Registers.write(regtarget, temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var DataLen := LoadLengthValue();

      if DataLen > 0 then
      begin
        var ByteData := buffer.ReadBytes(PC,datalen);
        FPC += Datalen;
        var temp :=  Registers.Read(regtarget) and Buffer.BytesToVariant(bytedata);
        Registers.write(regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.EOR(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clReg_var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) or Variables.Read(varid);
      Registers.write(regtarget, temp);
    end;
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var regSource := LoadIndexValue();
      var temp :=  Variables.Read(varid) or Registers.Read(regSource);
      Variables.write(varid, temp);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(regtarget) or Constants.Read(constid);
      Registers.write(regtarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) or Registers.Read(regsource);
      Registers.write(regtarget, temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadIndexValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC += datalen;

        var temp :=  Registers.Read(regtarget) or Buffer.BytesToVariant(bytedata);
        Registers.write(regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.ENOT(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clRegOnly:
    begin
      var regtarget := LoadIndexValue();
      var temp := not Registers.Read(regtarget);
      Registers.write(regtarget, temp);
    end;
  clVarOnly:
    begin
      var varid := LoadIndexValue();
      var temp := not Variables.Read(varid);
      Variables.write(varid, temp);
    end;
  end;
end;

procedure TVirtualMachine.EXOR(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Variables.Read(varid) xor Registers.Read(regsource);
      Variables.Write(varid, temp);
    end;
  clReg_Var:
    begin
      // XOR register with variable
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) xor Variables.Read(varid);
      Registers.write(regtarget, temp);
    end;
  clReg_Const:
    begin
      // XOR register with predefined constant
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(regtarget) xor Constants.Read(constid);
      Registers.write(regtarget, temp);
    end;
  clReg_Reg:
    begin
      // XOR register with register
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) xor Registers.Read(regsource);
      Registers.write(regtarget, temp);
    end;
  clReg_value:
    begin
      // XOR register with inline-value
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC += datalen;
        var temp :=  Registers.Read(regtarget) xor Buffer.BytesToVariant(bytedata);
        Registers.write(regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.EMod(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_Reg:
    begin
      var varid :=LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Variables.Read(varid) mod Registers.Read(regsource);
      Variables.write(varid, temp);
    end;
  clReg_Var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) mod Variables.Read(varid);
      Registers.write(regtarget, temp);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(regtarget) mod Constants.Read(constid);
      Registers.write(regtarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) mod Registers.Read(regsource);
      Registers.write(regtarget, temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC += datalen;
        var temp :=  Registers.Read(regtarget) mod Buffer.BytesToVariant(bytedata);
        Registers.write(regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.ELsR(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_Value:
    begin
      var VarId := LoadIndexValue();
      var DataLen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var ByteData := buffer.ReadBytes(FPC, DataLen);
        FPC += Datalen;
        var Value := Buffer.BytesToVariant(ByteData);
        var temp :=  Variables.Read(varid) shr value;
        Variables.write(varid, temp);
      end;
    end;
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var regSource := LoadIndexValue();
      var temp :=  Variables.Read(varid) shr Registers.Read(regSource);
      Variables.write(varid, temp);
    end;
  clReg_Var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) shr Variables.Read(varid);
      Registers.write(regtarget, temp);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(regtarget) shr Constants.Read(constid);
      Registers.write(regtarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) shr Registers.Read(regsource);
      Registers.write(regtarget, temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(FPC, Datalen);
        FPC += datalen;
        var temp :=  Registers.Read(regtarget) shr Buffer.BytesToVariant(bytedata);
        Registers.write(regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.ELsL(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_Value:
    begin
      var varid := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(FPC, DataLen);
        FPC += datalen;
        var Value := Buffer.BytesToVariant(bytedata);
        var temp :=  Variables.Read(varid) shl value;
        Variables.write(varid, temp);
      end;
    end;
  clVar_Reg:
    begin
      var varid := LoadIndexValue();
      var regSource := LoadIndexValue();
      var temp :=  Variables.Read(varid) SHL Registers.Read(regSource);
      Variables.write(varid, temp);
    end;
  clReg_Var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) shL Variables.Read(varid);
      Registers.write(regtarget, temp);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp :=  Registers.Read(regtarget) shl Constants.Read(constid);
      Registers.write(regtarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp :=  Registers.Read(regtarget) shl Registers.Read(regsource);
      Registers.write(regtarget, temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(FPC, datalen);
        FPC += datalen;
        var Value := Buffer.BytesToVariant(bytedata);
        var temp :=  Registers.Read(regtarget) shl value;
        Registers.write(regtarget, temp);
      end;
    end;
  end;
end;

procedure TVirtualMachine.EBtst(const layout, paramA, paramB: byte);
begin
  CompareFlag := [];
  case TOpCodeLayout(layout) of
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var regsource := LoadIndexValue();
      if TInteger.GetBit( Registers.Read(RegSource), Variables.Read(varid) ) then
        CompareFlag := [ccEqual];
    end;
  clreg_var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      if TInteger.GetBit( Variables.Read(varid), Registers.Read(regtarget) ) then
        CompareFlag := [ccEqual];
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      if TInteger.GetBit( Registers.Read(regtarget), Constants.Read(constid) ) then
        CompareFlag := [ccEqual];
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      if TInteger.GetBit( Registers.Read(regtarget), Registers.Read(regsource) ) then
        CompareFlag := [ccEqual];
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC += datalen;
        if TInteger.GetBit( Registers.Read(regtarget), Buffer.BytesToVariant(bytedata) ) then
          CompareFlag := [ccEqual];
      end;
    end;
  end;
end;

procedure TVirtualMachine.EBset(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_Reg:
    begin
      var varid := LoadIndexValue();
      var regsource := LoadIndexValue();
      var BitBuffer := TVariant.AsInteger( Variables.Read(varid) );
      var BitToSet := Registers.Read(regsource);
      TInteger.SetBit(BitToSet, true, bitbuffer);
      Variables.write(varId, BitBuffer);
    end;
  clReg_Var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
      var BitToSet := Variables.Read(varid);
      TInteger.SetBit(BitToSet, true, bitbuffer);
      Registers.write(regtarget, BitBuffer);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
      var BitToSet := TVariant.AsInteger( Constants.Read(constid) );
      TInteger.SetBit(BitToSet, true, bitbuffer);
      Registers.write(regtarget, BitBuffer);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
      var BitToSet := TVariant.AsInteger( Registers.Read(regsource) );
      TInteger.SetBit(BitToSet, true, bitbuffer);
      Registers.write(regtarget, BitBuffer);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC += DataLen;
        var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
        var BitToSet := TVariant.AsInteger( Buffer.BytesToVariant(bytedata));
        TInteger.SetBit(BitToSet, true, bitbuffer);
        Registers.write(regtarget, BitBuffer);
      end;
    end;
  end;
end;

procedure TVirtualMachine.EBclr(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clReg_Var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
      var BitToSet := TVariant.AsInteger( Variables.Read(varid) );
      TInteger.SetBit(BitToSet, false, bitbuffer);
      Registers.write(regtarget, BitBuffer);
    end;
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var regsource := LoadIndexValue();
      var BitBuffer := TVariant.AsInteger( Variables.Read(varid) );
      var BitToSet := TVariant.AsInteger( Registers.Read(regsource) );
      TInteger.SetBit(BitToSet, false, bitbuffer);
      Variables.write(varid, BitBuffer);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadIndexValue();
      var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
      var BitToSet := TVariant.AsInteger( Constants.Read(constid) );
      TInteger.SetBit(BitToSet, false, bitbuffer);
      Registers.write(regtarget, BitBuffer);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
      var BitToSet := TVariant.AsInteger( Registers.Read(regsource) );
      TInteger.SetBit(BitToSet, false, bitbuffer);
      Registers.write(regtarget, BitBuffer);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC += datalen;
        var BitBuffer := TVariant.AsInteger( Registers.Read(regtarget) );
        var BitToSet := TVariant.AsInteger( Buffer.BytesToVariant(bytedata) );
        TInteger.SetBit(BitToSet, false, bitbuffer);
        Registers.write(regtarget, BitBuffer);
      end;
    end;
  end;
end;

procedure TVirtualMachine.EAdd(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var regSource := LoadIndexValue();
      var temp := Variables.Read(varId);
      temp := temp + Registers.read(regSource);
      Variables.write(varid, temp);
    end;
  clreg_var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var temp := registers.Read(regTarget);
      temp := temp + Variables.Read(VarId);
      Registers.write(RegTarget, temp);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();

      var Temp := Registers.Read(RegTarget);
      Temp := temp + Constants.Read(ConstId);
      Registers.Write(RegTarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();

      var Temp := Registers.Read(RegTarget);
      Temp := temp + Registers.Read(regsource);
      Registers.Write(RegTarget, temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var DataLen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var ByteData := buffer.ReadBytes(PC, DataLen);
        FPC += DataLen;
        var temp := Buffer.BytesToVariant(ByteData);

        var LValue := Registers.Read(RegTarget);
        LValue := LValue + Temp;
        Registers.Write(RegTarget, LValue);
      end;
    end;
  end;
end;

procedure TVirtualMachine.ECmp(const layout, paramA, paramB: byte);
begin
  CompareFlag := [];
  case TOpCodeLayout(layout) of
  clVar_reg:
    begin
      var varid := LoadIndexValue();
      var regSource := LoadIndexValue();

      if Variables.Read(varid) = Registers.Read(regSource) then
        Include(CompareFlag, ccEqual)
      else
      if ( Variables.Read(varid) < Registers.Read(regSource) ) then
        Include(Compareflag, ccLess)
      else
      if Variables.Read(varid) > Registers.Read(regSource) then
        Include(Compareflag, ccGreater);
    end;
  clreg_var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      if Registers.Read(regtarget) = Variables.Read(varid) then Include(CompareFlag, ccEqual) else
      if Registers.Read(regtarget) < Variables.Read(varid) then Include(Compareflag, ccLess) else
      if Registers.Read(regtarget) > Variables.Read(varid) then Include(Compareflag, ccGreater);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      if Registers.Read(regtarget) = Constants.Read(constid) then Include(CompareFlag, ccEqual) else
      if Registers.Read(regtarget) < Constants.Read(constid) then Include(Compareflag, ccLess) else
      if Registers.Read(regtarget) > Constants.Read(constid) then Include(Compareflag, ccGreater);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      if Registers.Read(regtarget) = Registers.Read(regsource) then Include(CompareFlag, ccEqual) else
      if Registers.Read(regtarget) < Registers.Read(regsource) then Include(Compareflag, ccLess) else
      if Registers.Read(regtarget) > Registers.Read(regsource) then Include(Compareflag, ccGreater);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var DataLen := LoadLengthValue();
      if DataLen > 0 then
      begin
        var ByteData := buffer.ReadBytes(PC, DataLen);
        FPC += DataLen;
        var temp := Buffer.BytesToVariant(ByteData);
        if Registers.Read(RegTarget) = temp then Include(CompareFlag, ccEqual) else
        if Registers.Read(RegTarget) < temp then Include(Compareflag, ccLess) else
        if Registers.Read(RegTarget) > temp then Include(Compareflag, ccGreater);
      end;
    end;
  end;
end;

procedure TVirtualMachine.ESub(const layout, paramA, paramB: byte);
begin
  case TOpCodeLayout(layout) of
  clVar_Reg:
    begin
      var varid := LoadIndexValue();
      var regSource := LoadIndexValue();
      var temp := Variables.Read(varid);
      temp := temp - Registers.Read(regSource);
      Variables.Write(varid, temp);
    end;
  clreg_var:
    begin
      var regtarget := LoadIndexValue();
      var varid := LoadIndexValue();
      var temp := Registers.Read(regtarget);
      temp := temp - Variables.Read(varid);
      Registers.Write(Regtarget, temp);
    end;
  clReg_Const:
    begin
      var regtarget := LoadIndexValue();
      var constid := LoadConstValue();
      var temp := Registers.Read(RegTarget);
      temp := temp - Constants.Read(constid);
      Registers.Write(RegTarget, temp);
    end;
  clReg_Reg:
    begin
      var regtarget := LoadIndexValue();
      var regsource := LoadIndexValue();
      var temp := Registers.Read(RegTarget);
      temp := temp - Registers.Read(regsource);
      Registers.Write(RegTarget, Temp);
    end;
  clReg_value:
    begin
      var regtarget := LoadIndexValue();
      var datalen := LoadLengthValue();
      if datalen > 0 then
      begin
        var bytedata := buffer.ReadBytes(PC,datalen);
        FPC += datalen;
        var stamp := registers.Read(regtarget);
        var temp := Buffer.BytesToVariant(bytedata);
        Registers.Write(RegTarget, (sTamp-temp) );
      end;
    end;
  end;
end;

//#############################################################################
//
//#############################################################################

constructor TDataBlock.Create;
begin
  inherited Create;
  FData := TBinaryData.Create;
end;

destructor TDataBlock.Destroy;
begin
  FData.free;
  inherited;
end;

//#############################################################################
// TDataControl
//#############################################################################

constructor TDataControl.Create;
begin
  inherited Create;
  FLUT := TVariant.CreateObject;
end;

procedure TDataControl.Clear;
begin
  try
    for var item in FBlocks do
    begin
      item.free;
      item := nil;
    end;
  finally
    FBlocks.Clear();
  end;
end;

destructor TDataControl.Destroy;
begin
  if FBlocks.length > 0 then
    Clear();
  inherited;
end;

function TDataControl.GetBlockById(const id: integer): TDataBlock;
begin
  var LValue := FLut[id];
  asm
    @result = @LValue;
  end;
end;

function TDataControl.Add(const id: integer): TDataBlock;
begin
  result := TDatablock.Create;
  result.Id := id;
  FBlocks.add(result);
end;

procedure TDataControl.Delete(const Index: integer);
begin
  if (index >=0) and (index < FBlocks.length) then
  begin
    FBlocks[index].free;
    FBlocks.delete(index, 1);
  end else
  raise EW3Exception.CreateFmt
  ('Failed to delete memory block #%d, index out of bounds error',[index]);
end;

procedure TDataControl.DeleteById(const id: integer);
begin
  var LFound := false;
  if FBlocks.Length > 0 then
  begin
    for var x := low(FBlocks) to high(FBlocks) do
    begin
      var LItem := FBlocks[x];
      LFound := LItem.id = id;
      if LFound then
      begin
        LItem.free;
        FBlocks.delete(x,1);
        break;
      end;
    end;
  end;

  if not LFound then
    raise EW3Exception.CreateFmt
    ('Failed to delete memory block [%d], block not found error',[id]);
end;



end.
