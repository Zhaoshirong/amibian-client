unit vmachine.types;

interface

{$I 'vmachine.inc'}

uses
  {$IFDEF DEBUG_OUT}
  SmartCL.System,
  {$ENDIF}

  System.Types,
  System.Types.Convert,
  System.Reader,
  System.Writer,
  System.Streams,
  System.Stream.Reader,
  System.Stream.Writer,
  System.Memory.Allocation,
  System.Memory.Buffer;


const
  instr_low  = $79;
  instr_high = $99;


const
  CNT_PREFIX_INCLUDE  = '#';
  CNT_PREFIX_EXPORT   = '@';
  CNT_PREFIX_LABEL    = '.';

  CNT_PREFIX_HEXVALUE = '$';
  CNT_PREFIX_BINVALUE = '%';

type

  // Forward declarations
  TIOObject           = class;
  TBCSymbol           = class;
  TBCFile             = class;
  TBCCodeSymbol       = class;
  TBCSymbolCollection = class;
  TBCHeader           = class;
  TBCTypeSymbol       = class;
  TBCTypeFieldSymbol  = class;


  TVMDataType = (
    vdtByte     = 1,  //  uInt8
    vdtWord     = 2,  //  uInt16
    vdtLong     = 3,  //  uInt32
    vdtInt      = 4,  //  int32
    vdtFloat    = 5,  //  double
    vdtBoolean  = 6,  //  boolean
    vdtString   = 7,  //  string
    vdtVariant  = 8   //  default JS variable
    );

  TParamType = (
    pkRegister  = $22, // R[x]
    pkVariable  = $23, // V[x]
    pkConst     = $24, // C[x]
    pkValue     = $25, // inline
    pkDC        = $26  // DC
    );

  EParamInfo = class(EW3Exception);

  TParamInfo = record
    piType:   TParamType;
    piPtr:    boolean;
    piIndex:  integer;
    piData:   variant;
  end;


  TCPUInstruction = (
    ocLabel = $00,  // INTERNAL, Used for label mapping
    ocSys   = $79,  // sys -[functionid]
    ocMOVE  = $7A,  // load (or move)
    ocPUSH  = $7B,  // push to stack
    ocPOP   = $7C,
    ocADD   = $7D,  // add
    ocSUB   = $7E,  // subtract
    ocCMP   = $7F,  // compare
    ocAlloc = $80,  // alloc variable
    ocFree  = $81,  // free variable
    ocJSR   = $82,  // jump subroutine
    ocBNE   = $83,  // branch not equal
    ocBEQ   = $84,  // branch equal
    ocRTS   = $85,  // Return to start [actually, stack, but whatever]
    ocNOOP  = $86,  // No operation, used for alignment typically
    ocMUL   = $87,
    ocDIV   = $88,
    ocMOD   = $89,
    ocLSR   = $90,
    ocLSL   = $91,
    ocBSet  = $92,
    ocBTst  = $93,
    ocBClr  = $94,
    ocAnd   = $95,
    ocOr    = $96,
    ocNot   = $97,
    ocXOR   = $98,
    ocJMP   = $99,
    ocBSR   = $A1,
    ocBLE   = $A2,
    ocBGT   = $A3
  );

  TOpCodeLayout = (
    clVoid      = $71,    // Instruction doesnt need layout
    clRegOnly   = $72,    // One param, register only
    clDatOnly   = $73,    // One param, value data only
    clCntOnly   = $74,    // One param, constant only
    clVarOnly   = $75,    // One param, variable only
    clReg_Value = $76,    // TARGET = register, SOURCE = value [inline data]
    clReg_Reg   = $77,    // TARGET = register, SOURCE = register
    clReg_Var   = $78,    // TARGET = register, SOURCE = variable
    clReg_Const = $79,    // TARGET = register, SOURCE = Constant

    clVar_Const = $80,    // TARGET = variable, SOURCE = Constant
    clVar_Reg   = $81,    // TARGET = variable, SOURCE = register
    clVar_Var   = $82,    // TARGET = variable, SOURCE = variable
    clVar_Value = $83,    // TARGET = variable, SOURCE = Value [inline data]

    clDCOnly    = $AA,    // one param, data-control [dc] only
    clDC_REG    = $A1,    // TARGET = DC, SOURCE = register
    clDC_VAR    = $A2,    // TARGET = DC, SOURCE = variable
    clDC_VALUE  = $A3,    // TARGET = DC, SOURCE = value [variant]
    clDC_CONST  = $A4,    // TARGET = DC, SOURCE = constant [resource]

    clValue_Value = $A5   // TARGET = Value, SOURCE = Value
    );

  TAsmParameterMode   = (
      ckVoid      = $00,  // Parameter is irellevant
      ckDirect    = $01,  // Parameter is value [byval]
      ckReference = $02   // Parameter is reference [byref]
      );

  /*  Assembly symbols are not as elaborate as those needed
      by high-level languages. Our assembly model have a
      fixed set of operations (instructions), where the
      parameters are the symbols. The parameters for assembly
      instructions are of various sorts:

        1.  Registers [R0 - R16]
        2.  DC [data control register]
        3.  Inline data (string, bytes and numbers)
        4.  codeseg constants C[id]
        5.  Local or global variales var[id]

      This sounds easy enough, but tokenizing and validating
      the various combinations does require a fair bit of work.
      The parser must distinguish between each, and also
      (in case of a constant value) the datatype in question.

      The SymbolType datatype thus represents the tokenized
      version of a text fragment. It applies to parameters
      but is also used in other aspects, like tokenizing
      declared constants (name equ value). */


  TAsmSymbolType = (
    cstInvalid,       // unknown or invalid symbol
    cstRegister,      // R[id]
    cstVariable,      // V[id]
    cstConstant,      // C[id]
    cstDC,            // DC
    cstNamedConstant, // #named_constant
    cstString,        // "text"
    cstNumber,        // #123 or 123
    cstHexNumber,     // $Hex
    cstBinNumber,     // %1010101
    cstLabel          // a label [name]
    );

  TGetNamedConstantCallback = function (const ConstName: string): variant;

  TAsmSymbolInfo = record
    SymbolType:  TAsmSymbolType;
    EntryIndex: integer;
    SymbolValue: string;
    SymbolPTR:   boolean;
    function ToParamInfo(const CB: TGetNamedConstantCallback): TParamInfo;
  end;

  /* Before emitting an instruction as bytecode, the
     instruction is parsed, tokenized and its parameters
     are processed. It is then stored as an TAsmInstruction
     record (below) and will be emitted to the codegen
     for assembly. The reason for this secondary buffer is
     primarly for optimization -- but also to retain some
     abstraction and distance between parser, model and
     auxillary classes */

  TAsmInstruction = record
    aiOpcode: TCPUInstruction;
    aiParams: array of TAsmSymbolInfo;
    aiLineNr: integer;
    aiOffset: integer;
  end;

  TAsmInstrArray = array of TAsmInstruction;

  TIOObject = class(TW3OwnedObject)
  protected
    function  GetSignature: integer; virtual;
    procedure ReadData(const Reader: TW3CustomReader); virtual;
    procedure WriteData(const Writer: TW3CustomWriter); virtual;
  public
    property  Signature: integer read GetSignature;
    procedure LoadFromStream(const Stream: TStream);
    procedure SaveToStream(const Stream: TStream);
  end;

  TBCSymbol = class(TIOObject)
  protected
    function  GetSignature: integer; override;
    procedure ReadData(const Reader: TW3CustomReader); override;
    procedure WriteData(const Writer: TW3CustomWriter); override;
  public
    property  SymbolType: integer;
    property  Identifier: string;
  end;

  TBCDataSymbol = class(TBCSymbol)
  private
    FBuffer:    TBinaryData;
  protected
    function    GetSignature: integer; override;
    procedure   ReadData(const Reader: TW3CustomReader); override;
    procedure   WriteData(const Writer: TW3CustomWriter); override;
  public
    property    Data: TBinaryData read FBuffer;
    constructor Create; reintroduce; virtual;
    destructor  Destroy; override;
  end;

  TBCCodeSymbol = class(TBCDataSymbol)
  protected
    function    GetSignature: integer; override;
  end;

  TBCConstSymbol = class(TBCDataSymbol)
  protected
    function    GetSignature: integer; override;
    procedure   ReadData(const Reader: TW3CustomReader); override;
    procedure   WriteData(const Writer: TW3CustomWriter); override;
  public
    property    ConstId: integer;
  end;

  TBCTypeFieldSymbol = class(TIOObject)
  protected
    function    GetOwner: TBCTypeSymbol; reintroduce;
  public
    property    Name: string;
    property    DataTypeId: integer;
    property    InitialValue: string;
    constructor Create(const ThisType: TBCTypeSymbol); reintroduce; virtual;
    destructor  Destroy; override;
  end;

  TBCTypeSymbol = class(TIOObject)
  private
    FFields:    array of TBCTypeFieldSymbol;
    function    GetItem(const Index: integer): TBCTypeFieldSymbol;
    function    GetCount: integer;
  public
    property    Fields[const index: integer]: TBCTypeFieldSymbol read GetItem;
    property    Count: integer read GetCount;

    function    Add(FieldName: string; const DataTypeId: integer): TBCTypeFieldSymbol; overload;
    function    Add(FieldName: string): TBCTypeFieldSymbol; overload;

    procedure   Clear;

    destructor  Destroy; override;
  end;

  TBCSymbolEnumProc = function (const Symbol: TBCSymbol): TEnumResult;
  TBCSymbolArray = array of TBCSymbol;

  TBCSymbolCollection = class(TIOObject)
  private
    FItems:     TBCSymbolArray;
    function    GetItem(const Index: integer): TBCSymbol;
    function    GetCount: integer;
  protected
    function    GetOwner: TBCFile; reintroduce;
  public
    property    Owner: TBCFile read GetOwner;
    property    Item[const Index: integer]: TBCSymbol read GetItem;
    property    Count: integer read GetCount;

    procedure   ForEach(const Process: TBCSymbolEnumProc);

    function    ExtractBySignature(const Signature: integer;
                var List: TBCSymbolArray): boolean;

    procedure   Clear;
    constructor Create(const ThisOwner: TBCFile); reintroduce; virtual;
    destructor  Destroy; override;
  end;

  TBCHeader = class(TIOObject)
  protected
    function    GetOwner: TBCFile; reintroduce;
    function    GetSignature: integer; override;
    procedure   ReadData(const Reader: TW3CustomReader); override;
    procedure   WriteData(const Writer: TW3CustomWriter); override;
  public
    property    Owner: TBCFile read GetOwner;
    constructor Create(const ThisOwner: TBCFile); reintroduce; virtual;
  end;

  TBCFile = class(TIOObject)
  private
    FHeader:    TBCHeader;
    FSymbols:   TBCSymbolCollection;
  protected
    procedure   ReadData(const Reader: TW3CustomReader); override;
    procedure   WriteData(const Writer: TW3CustomWriter); override;
  public
    property    Header: TBCHeader read FHeader;
    property    Symbols: TBCSymbolCollection read FSymbols;
    constructor Create; reintroduce; virtual;
    destructor  Destroy; override;
  end;

implementation

uses VMachine.Assembler;

const
  IOOBJ_MAGIC = $ABBABABE;

  SYMBOL_MAGIC_BASE   = $FF00BABE;
  SYMBOL_MAGIC_DATA   = $FF01BABE;
  SYMBOL_MAGIC_CODE   = $FF02BABE;
  SYMBOL_MAGIC_CONST  = $FF03BABE;

  BYTECODE_HEADER_MAGIC = $AA00FFCC;

//#############################################################################
// TBCFile
//#############################################################################

constructor TBCFile.Create;
begin
  inherited Create(nil);
  FHeader := TBCHeader.Create(self);
  FSymbols := TBCSymbolCollection.Create(self);
end;

destructor TBCFile.Destroy;
begin
  FHeader.free;
  FSymbols.free;
  inherited;
end;

procedure TBCFile.WriteData(const Writer: TW3CustomWriter);
begin
  FHeader.WriteData(Writer);
  FSymbols.WriteData(Writer);
end;

procedure TBCFile.ReadData(const Reader: TW3CustomReader);
begin
  FHeader.ReadData(Reader);
  FSymbols.ReadData(Reader);
end;

//#############################################################################
// TBCHeader
//#############################################################################

constructor TBCHeader.Create(const ThisOwner: TBCFile);
begin
  inherited Create(ThisOwner);
end;

function TBCHeader.GetOwner: TBCFile;
begin
  result := TBCFile(inherited GetOwner());
end;

function TBCHeader.GetSignature: integer;
begin
  result := BYTECODE_HEADER_MAGIC;
end;

procedure TBCHeader.WriteData(const Writer: TW3CustomWriter);
begin
  writer.WriteInteger(GetSignature());
end;

procedure TBCHeader.ReadData(const Reader: TW3CustomReader);
begin
  var LMagic := reader.ReadInteger();
  if LMagic = GetSignature() then
  begin
  end else
  raise EW3Exception.CreateFmt
  ('Invalid header signature, expected $%s not $%s error',
  [IntToHex(GetSignature(), 8), IntToHex(LMagic, 8)]);
end;

//#############################################################################
// TBCSymbolCollection
//#############################################################################

constructor TBCSymbolCollection.Create(const ThisOwner: TBCFile);
begin
  inherited Create(ThisOwner);
end;

destructor TBCSymbolCollection.Destroy;
begin
  if FItems.Count > 0 then
    Clear();
  inherited;
end;

procedure TBCSymbolCollection.Clear;
begin
  try
    for var Item in FItems do
      Item.free;
  finally
    FItems.clear();
  end;
end;

function TBCSymbolCollection.ExtractBySignature(const Signature: integer;
         var List: TBCSymbolArray): boolean;
begin
  list.clear();
  for var el in FItems do
  begin
    if el.GetSignature = Signature then
      List.add(el);
  end;
  result := list.count > 0;
end;

procedure TBCSymbolCollection.ForEach(const Process: TBCSymbolEnumProc);
begin
  if assigned(Process) then
  begin
    for var el in FItems do
    begin
      if Process(el) = erBreak then
        break;
    end;
  end;
end;

function TBCSymbolCollection.GetOwner: TBCFile;
begin
  result := TBCFile(inherited GetOwner);
end;

function TBCSymbolCollection.GetCount: integer;
begin
  result := FItems.Count;
end;

function TBCSymbolCollection.GetItem(const Index: integer): TBCSymbol;
begin
  result := FItems[index];
end;

//#############################################################################
// TBCConstSymbol
//#############################################################################

function  TBCConstSymbol.GetSignature: integer;
begin
  result := SYMBOL_MAGIC_CONST;
end;

procedure TBCConstSymbol.WriteData(const Writer: TW3CustomWriter);
begin
  inherited WriteData(Writer);
  Writer.WriteInteger(ConstId);
end;

procedure TBCConstSymbol.ReadData(const Reader: TW3CustomReader);
begin
  inherited ReadData(Reader);
  ConstId := Reader.ReadInteger();
end;

//#############################################################################
// TBCCodeSymbol
//#############################################################################

function TBCCodeSymbol.GetSignature: integer;
begin
  result := SYMBOL_MAGIC_CODE;
end;

//#############################################################################
// TBCDataSymbol
//#############################################################################

constructor TBCDataSymbol.Create;
begin
  inherited Create(nil);
  FBuffer := TBinaryData.Create;
end;

destructor TBCDataSymbol.Destroy;
begin
  FBuffer.free;
  inherited;
end;

function TBCDataSymbol.GetSignature: integer;
begin
  result := SYMBOL_MAGIC_DATA;
end;

procedure TBCDataSymbol.WriteData(const Writer: TW3CustomWriter);
begin
  inherited WriteData(Writer);
  Writer.WriteInteger(FBuffer.size);
  if FBuffer.Size > 0 then
    Writer.Write(FBuffer.ToBytes());
end;

procedure TBCDataSymbol.ReadData(const Reader: TW3CustomReader);
begin
  inherited ReadData(Reader);

  if FBuffer.Size > 0 then
    FBuffer.Release();

  var LSize := Reader.ReadInteger();
  if LSize > 0 then
    FBuffer.AppendBytes(Reader.Read(LSize));
end;

//#############################################################################
// TBCSymbol
//#############################################################################

function TBCSymbol.GetSignature: integer;
begin
  result := SYMBOL_MAGIC_BASE;
end;

procedure TBCSymbol.WriteData(const Writer: TW3CustomWriter);
begin
  inherited WriteData(Writer);
  Writer.WriteInteger(SymbolType);
  Writer.WriteString(Identifier);
end;

procedure TBCSymbol.ReadData(const Reader: TW3CustomReader);
begin
  inherited ReadData(Reader);
  SymbolType := Reader.ReadInteger();
  Identifier := Reader.ReadString();
end;

//#############################################################################
// TIOObject
//#############################################################################

procedure TIOObject.LoadFromStream(const Stream: TStream);
var
  LReader:  TStreamReader;
begin
  LReader := TStreamReader.Create(Stream);
  try
    ReadData( TW3CustomReader(LReader) );
  finally
    LReader.free;
  end;
end;

procedure TIOObject.SaveToStream(const Stream: TStream);
var
  LWriter:  TStreamWriter;
begin
  LWriter := TStreamWriter.Create(Stream);
  try
    WriteData( TW3CustomWriter(LWriter) );
  finally
    LWriter.free;
  end;
end;

procedure TIOObject.ReadData(const Reader: TW3CustomReader);
begin
  var LMagic := Reader.ReadInteger();
  if LMagic <> GetSignature() then
  raise EW3Exception.CreateFmt
  ('Invalid %s signature, expected $%s not $%s error',
  [ClassName, IntToHex(GetSignature(),8), IntToHex(LMagic,8)]);
end;

procedure TIOObject.WriteData(const Writer: TW3CustomWriter);
begin
  Writer.WriteInteger(GetSignature);
end;

function TIOObject.GetSignature: integer;
begin
  result := IOOBJ_MAGIC;
end;

//#############################################################################
// TAsmSymbolInfo
//#############################################################################

function TAsmSymbolInfo.ToParamInfo(const CB: TGetNamedConstantCallback): TParamInfo;
begin
  if SymbolType = cstInvalid then
    raise Exception.Create('SymbolInfo is invalid, ToParamInfo failed');

  //    cstInvalid,       // unknown or invalid symbol
  case SymbolType of
  cstRegister:  result.piType := pkRegister;
  cstVariable:  result.piType := pkVariable;
  cstConstant:  result.piType := pkConst;
  cstDC:        result.piType := pkDC;
  cstLabel:
    begin
      result.piType := pkValue;
      result.piData := SymbolValue;
      result.piIndex := EntryIndex;
    end;
  cstNamedConstant:
    begin
      result.piType := pkValue;
      if not assigned(CB) then
        raise Exception.Create
        ("Failed to convert SymbolInfo to ParamInfo, callback for named constant was nil error");

      result.piData := CB(Self.SymbolValue);
    end;
  cstString:
    begin
      result.piType := pkValue;
      result.piData := string(self.SymbolValue);
    end;
  cstNumber:
    begin
      result.piType := pkValue;
      result.piData := StrToInt(SymbolValue);
    end;
  cstBinNumber:
    begin
      result.piType := pkValue;
      result.piData := TAsmToolbox.BinToInt(SymbolValue);
    end;
  cstHexNumber:
    begin
      result.piType := pkValue;
      var temp := SymbolValue.DeleteLeft(1);
      result.piData := HexToInt(temp)
    end;
  end;

  // Purely register based? Set index
  if (SymbolType in [cstRegister, cstVariable, cstConstant]) then
    result.piIndex := self.EntryIndex;

  // pointer? () ? Mark it so
  if (SymbolType in [cstRegister, cstVariable]) then
    result.piPtr := SymbolPTR;
end;

{ TBCTypeSymbol }

//#############################################################################
// TBCTypeSymbol
//#############################################################################

destructor TBCTypeSymbol.Destroy;
begin
  Clear();
  inherited;
end;

procedure TBCTypeSymbol.Clear;
begin
  try
    for var LField in FFields do
    begin
      LField.free;
    end;
  finally
    FFields.clear();
  end;
end;

function TBCTypeSymbol.GetCount: Integer;
begin
  result := FFields.Count;
end;

function TBCTypeSymbol.GetItem(const Index: Integer): TBCTypeFieldSymbol;
begin
  result := FFields[index];
end;

function TBCTypeSymbol.Add(FieldName: String): TBCTypeFieldSymbol;
begin
end;

function TBCTypeSymbol.Add(FieldName: String; const DataTypeId: Integer): TBCTypeFieldSymbol;
begin

end;

//#############################################################################
// TBCTypeFieldSymbol
//#############################################################################

constructor TBCTypeFieldSymbol.Create(const ThisType: TBCTypeSymbol);
begin
  inherited Create(ThisType);
end;

destructor TBCTypeFieldSymbol.Destroy;
begin
  inherited;
end;

function TBCTypeFieldSymbol.GetOwner: TBCTypeSymbol;
begin
  result := TBCTypeSymbol( inherited GetOwner() );
end;

end.
