unit vmachine.buffers;

interface

uses
  System.Streams,
  System.Stream.Reader,
  System.Stream.Writer,
  System.Types,
  System.Types.Convert,
  System.Reader,
  System.Writer,
  SmartCL.System;

type

  TCodemapElement = record
    miTag:    integer;
    miOffset: integer;
  end;

  ECodemap = class(EW3Exception);

  TCodemap = class(TObject)
  private
    FItems:   array of TCodemapElement;
  protected
    function  GetElement(const Index: integer): TCodemapElement;
    procedure SetElement(const Index: integer; const NewElement: TCodemapElement);
  public
    property  Item[const index:Integer]: TCodemapElement read GetElement write SetElement;

    function  Add(const Data: TCodemapElement): integer; overload;
    function  Add(const Tag, Offset: integer): integer; overload;

    procedure Clear;
    procedure SaveToStream(const Stream: TStream);
    procedure LoadFromStream(const Stream: TStream);
  end;


implementation

//#############################################################################
// TCodemap
//#############################################################################

procedure TCodemap.Clear;
begin
  FItems.Clear;
end;

function TCodemap.Add(const Data: TCodemapElement): integer;
begin
  result := FItems.Count;
  FItems.add(Data);
end;

function  TCodemap.GetElement(const Index: integer): TCodemapElement;
begin
  result := FItems[index];
end;

procedure TCodemap.SetElement(const Index: integer; const NewElement: TCodemapElement);
begin
  FItems[index] := NewElement;
end;

function TCodemap.Add(const Tag, Offset: integer):Integer;
var
  LItem: TCodemapElement;
begin
  result := FItems.Count;
  LItem.miTag := Tag;
  LItem.miOffset := offset;
  FItems.add(LItem);
end;

procedure TCodemap.SaveToStream(const Stream: TStream);
begin
  var LWriter := TWriter.Create(stream as IBinaryTransport);
  try
    // write header
    LWriter.WriteInteger($BADBABE);
    LWriter.WriteInteger(FItems.Count);

    for var x:=0 to FItems.Count-1 do
    begin
      LWriter.WriteInteger(FItems[x].miTag);
      LWriter.WriteInteger(FItems[x].miOffset);
    end;

  finally
    LWriter.free;
  end;
end;

procedure TCodemap.LoadFromStream(const Stream: TStream);
{ var
  LItem:  TCodemapElement; }
begin
  Clear();
  var LReader := TReader.Create(Stream as IBinaryTransport);
  try
    var LTemp := LReader.ReadInteger;
    if (LTemp = $BADBABE) then
    begin
      var LCount := LReader.ReadInteger;
      while LCount > 0 do
      begin
        var LItem: TCodemapElement;
        LItem.miTag := LReader.ReadInteger;
        LItem.miOffset := LReader.ReadInteger;
        FItems.add(LItem);
        dec(LCount);
      end;
    end else
    raise ECodemap.CreateFmt
    ('Failed to read codemap header, expected $badbabe not $%s',
    [IntToHex(LTemp, 8)]);
  finally
    LReader.free;
  end;
end;

end.