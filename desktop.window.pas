unit desktop.window;

interface

{.$DEFINE DEBUG}

{$DEFINE USE_FLEX}
{$DEFINE USE_CSSMOVE}

uses
  W3C.DOM,
  System.Widget,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Colors,
  System.Time,

  System.Events,
  SmartCL.Events,

  SmartCL.Flexbox,

  desktop.control,
  desktop.iconView,
  desktop.panel,
  desktop.button,
  desktop.switch,
  desktop.scrollbar,

  SmartCL.System,
  SmartCL.Time,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Effects,
  SmartCL.Fonts,
  SmartCL.Borders,
  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet,
  SmartCL.Scroll,
  SmartCL.Controls.ScrollBox,
  SmartCL.Controls.Image,
  SmartCL.Controls.Label,
  SmartCL.Controls.SimpleLabel,
  SmartCL.Controls.Editbox,
  SmartCL.Controls.Panel,
  SmartCL.Controls.Toolbar;

type

  TWbCustomWindow = partial class(TWbCustomControl)
  end;

  TWbWindowElement = class(TWbCustomControl)
  public
    procedure StyleAsEnabled; virtual;
    procedure StyleAsDisabled; virtual;
  end;

  TWbWindowDecorElement = class(TWbCustomControl)
  end;

  TWbWindowGlyph = class(TWbWindowDecorElement)
  protected
    // These should never be used with flex, so we shortcircuit them
    // You can *ONLY* change the width of an element
    {$IFDEF USE_FLEX}
    procedure SetLeft(const NewLeft: integer); override;
    procedure SetTop(const NewTop: integer); override;
    procedure MoveTo(const NewLeft, NewTop: integer); override;
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer); overload; override;
    procedure SetBounds(const NewBounds: TRect); overload; override;
    procedure SetWidth(const NewWidth: integer); override;
    procedure InitializeObject; override;
    {$ENDIF}
  public
    {$IFDEF USE_FLEX}
    procedure UpdateBaseSize; virtual;
    function CreationFlags: TW3CreationFlags; override;
    {$ENDIF}
  end;

  TWbWindowCloseGlyph = class(TWbWindowGlyph)
  public
    {$IFDEF USE_FLEX}
    procedure UpdateBaseSize; override;
    {$ELSE}
    procedure InitializeObject; override;
    {$ENDIF}
  end;

  TWbWindowZOrderGlyph = class(TWbWindowGlyph)
  public
    {$IFDEF USE_FLEX}
    procedure UpdateBaseSize; override;
    {$ELSE}
    procedure InitializeObject; override;
    {$ENDIF}
  end;

  TWbWindowMinimizeGlyph = class(TWbWindowGlyph)
  public
    {$IFDEF USE_FLEX}
    procedure UpdateBaseSize; override;
    {$ELSE}
    procedure InitializeObject; override;
    {$ENDIF}
  end;

  TWbWindowFullScreenGlyph = class(TWbWindowGlyph)
  public
    {$IFDEF USE_FLEX}
    procedure UpdateBaseSize; override;
    {$ELSE}
    procedure InitializeObject; override;
    {$ENDIF}
  end;

  TWbWindowSizerGlyph = class(TWbWindowDecorElement)
  end;

  TWbWindowHeaderTitle = class(TW3Label)
  protected
    // These should never be used with flex, so we shortcircuit them
    // You can *ONLY* change the width of an element
    {$IFDEF USE_FLEX}
    procedure SetLeft(const NewLeft: integer); override;
    procedure SetTop(const NewTop: integer); override;
    procedure MoveTo(const NewLeft, NewTop: integer); override;
    procedure SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer); overload; override;
    procedure SetBounds(const NewBounds: TRect); overload; override;
    procedure InitializeObject; override;
    {$ENDIF}
  public
    {$IFDEF USE_FLEX}
    function CreationFlags: TW3CreationFlags; override;
    {$ENDIF}
  end;

  TWbWindowHeader = class(TWbWindowElement)
  private
    FTitle:     TWbWindowHeaderTitle;
    FClose:     TWbWindowCloseGlyph;
    FZOrder:    TWbWindowZOrderGlyph;
    FMinimize:  TWbWindowMinimizeGlyph;
    FFull:      TWbWindowFullScreenGlyph;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure Resize; override;
    procedure ObjectReady; override;
    procedure StyleTagObject; override;

    procedure StyleAsEnabled; override;
    procedure StyleAsDisabled; override;

  public
    function  CreationFlags: TW3CreationFlags; override;

    property  Title: TWbWindowHeaderTitle read FTitle;
    property  CloseGlyph: TWbWindowCloseGlyph read FClose;
    property  MinimizeGlyph: TWbWindowMinimizeGlyph read FMinimize;
    property  ZOrderGlyph: TWbWindowZOrderGlyph read FZOrder;
    property  FullScreen: TWbWindowFullScreenGlyph read FFull;
  end;

  TWbWindowLeftSide = class(TWbWindowElement)
  end;

  TWbWindowRightSide = class(TWbWindowElement)
  private
    FScrollbar: TWbVerticalScrollbar;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure Resize; override;
  public
    property  Scrollbar: TWbVerticalScrollbar read FScrollbar;
    procedure ShowScrollbar;
    procedure HideScrollbar;
    function  GetCalculatedWidth: integer;

    procedure StyleAsEnabled; override;
    procedure StyleAsDisabled; override;
  end;

  TWbWindowFooter = class(TWbWindowElement)
  private
    FScrollbar: TWbHorizontalScrollbar;
  protected
    procedure FinalizeObject; override;
    procedure Resize; override;
  public
    property  Scrollbar: TWbHorizontalScrollbar read FScrollbar;
    procedure ShowScrollbar;
    procedure HideScrollbar;
    function  GetCalculatedHeight: integer;

    procedure StyleAsEnabled; override;
    procedure StyleAsDisabled; override;
  end;

  TW3WindowFocusBlock = class(TWbWindowElement)
  end;

  TWbWindowContent = class(TWbWindowElement)
  private
    FPermanent: boolean;
    FBlocker:   TW3WindowFocusBlock;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
  public
    property  InputDisabledPermanent: boolean read FPermanent;
    property  InputDisabled: boolean read ( FBlocker.visible );

    procedure DisableInput(const BlockPermanent: boolean);
    procedure EnableInput;
  published
    property  OnInputDisabled: TNotifyEvent;
    property  OnInputEnabled: TNotifyEvent;
  end;

  TWbWindowMoveBeginsEvent  = TNotifyEvent;
  TWbWindowMoveEndsEvent    = TNotifyEvent;
  TWbWindowSizeBeginsEvent  = TNotifyEvent;
  TWbWindowSizeEndsEvent    = TNotifyEvent;

  TWbWindowOptions = set of (
    woAutoInitSize,
    woSizeable,
    woHScroll,
    woVScroll
  );

  TWbWindowState = (
    wsNormal,
    wsMinimized,
    wsMaximized
  );

  TWbCustomWindow = partial class(TWbCustomControl)
  private
    FHeader:      TWbWindowHeader;
    FLeft:        TWbWindowLeftSide;
    FRight:       TWbWindowRightSide;
    FFooter:      TWbWindowFooter;
    FContent:     TWbWindowContent;
    FSizer:       TWbWindowSizerGlyph;
    FBringToFrontEvent: TW3ElementDblClickEvent;
    FMoveActive:  boolean;
    FSizeActive:  boolean;
    FStartPos:    TPoint;
    FMinWidth:    integer;
    FMinHeight:   integer;
    FOptions:     TWbWindowOptions;
    FOldSize:     TRect; // Size of window before maximize
    FStyled:      boolean;
    FState:       TWbWindowState = wsNormal;
  protected
    procedure HandleCloseClick(Sender: TObject); virtual;
    procedure HandleZOrderClick(Sender: TObject); virtual;
    procedure HandleMinimizeClick(Sender: TObject); virtual;
    procedure HandleFullScreenClick(Sender: TObject); virtual;
    procedure HandleGotFocus(Sender: TObject); virtual;

    procedure WindowStateChanged(const NewState: TWbWindowState); virtual;

    // If you require a special content-container type then
    // override this. For example, if you want one that allows selection
    // of text, then you would create one with the cfAllowSelection flag
    // set [TW3tagObj.CreationFlags() method]
    function  CreateContentInstance: TWbWindowContent; virtual;

  protected
    procedure HandleMouseDown(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: integer); virtual;

    procedure HandleMouseMove(Sender: TObject;
              Shift: TShiftState; X, Y: integer); virtual;

    procedure HandleMouseUp(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: integer); virtual;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;

    procedure BeforeResize; virtual;
    procedure AfterResize; virtual;

    function  GetFullScreenElement: TControlHandle; virtual;

  public
    class var CSSTransform: string;

    property  State: TWbWindowState read FState;
    property  Content: TWbWindowContent read FContent;
    property  Header: TWbWindowHeader read FHeader;
    property  Footer: TWbWindowFooter read FFooter;
    property  Sizer: TWbWindowSizerGlyph read FSizer;
    property  LeftEdge: TWbWindowLeftSide read FLeft;
    property  RightEdge: TWbWindowRightSide read FRight;
    property  Styled: boolean read FStyled;

    property  SizeActive: boolean read FSizeActive;
    property  MoveActive: boolean read FMoveActive;

    property  MinimumWidth: integer read FMinWidth write FMinWidth;
    property  MinimumHeight: integer read FMinHeight write FMinHeight;

    procedure StyleAsFocused; overload;
    procedure StyleAsUnFocused; overload;

    procedure StyleAsFocused(const CB: TProcedureRef); overload;
    procedure StyleAsUnFocused(const CB: TProcedureRef); overload;

    procedure CenterOnParent;

    procedure CloseWindow; virtual;

    procedure UnSelect;
    procedure SetOptions(const NewOptions: TWbWindowOptions); virtual;

    function CreationFlags: TW3CreationFlags; override;
  published
    property  Options: TWbWindowOptions read FOptions write SetOptions;
    property  OnWindowMoveBegins:   TWbWindowMoveBeginsEvent;
    property  OnWindowMoveEnds:     TWbWindowMoveEndsEvent;
    property  OnWindowReSizeBegins: TWbWindowSizeBeginsEvent;
    property  OnWindowReSizeEnds:   TWbWindowSizeEndsEvent;
    property  OnWindowClose: TNotifyevent;
  end;

  TWbWindow = class(TWbCustomWindow)
  protected
    function  GetHScroll: TWbHorizontalScrollbar;
    function  GetVScroll: TWbVerticalScrollbar;
    procedure HandleMouseDown(Sender: TObject; Button: TMouseButton;
              Shift: TShiftState; X, Y: integer); override;
  public
    property  HorizontalScroll: TWbHorizontalScrollbar read GetHScroll;
    property  VerticalScroll: TWbVerticalScrollbar read GetVScroll;
  end;

  TWbWindowList = array of TWbCustomWindow;


implementation

uses
  desktop.types;

//#############################################################################
// TWbWindowCloseGlyph
//#############################################################################

{$IFDEF USE_FLEX}
procedure TWbWindowCloseGlyph.UpdateBaseSize;
begin
  Handle.style['flex-basis'] := '24px';
end;
{$ELSE}
procedure TWbWindowCloseGlyph.InitializeObject;
begin
  inherited;
  width := 24;
end;
{$ENDIF}

//#############################################################################
// TWbWindowZOrderGlyph
//#############################################################################

{$IFDEF USE_FLEX}
procedure TWbWindowZOrderGlyph.UpdateBaseSize;
begin
  Handle.style['flex-basis'] := '28px';
end;
{$ELSE}
procedure TWbWindowZOrderGlyph.InitializeObject;
begin
  inherited;
  width := 28;
end;
{$ENDIF}

//#############################################################################
// TWbWindowFullScreenGlyph
//#############################################################################

{$IFDEF USE_FLEX}
procedure TWbWindowFullScreenGlyph.UpdateBaseSize;
begin
  Handle.style['flex-basis'] := '23px';
end;
{$ELSE}
procedure TWbWindowFullScreenGlyph.InitializeObject;
begin
  inherited;
  width := 23;
end;
{$ENDIF}

//#############################################################################
// TWbWindowSizerGlyph
//#############################################################################

{$IFDEF USE_FLEX}
procedure TWbWindowMinimizeGlyph.UpdateBaseSize;
begin
  Handle.style['flex-basis'] := '23px';
end;
{$ELSE}
procedure TWbWindowMinimizeGlyph.InitializeObject;
begin
  inherited;
  width := 23;
end;
{$ENDIF}

//#############################################################################
// TWbWindowHeaderTitle
//#############################################################################

{$IFDEF USE_FLEX}
procedure TWbWindowHeaderTitle.InitializeObject;
begin
  inherited;
  PositionMode := pmRelative;
  DisplayMode := TW3TagDisplayMode.dmBlock;
  Handle.style['flex-shrink'] := '0';

  Handle.style['flex-grow'] := '1';
  Handle.style['flex-basis'] := '50%';
end;

function TWbWindowHeaderTitle.CreationFlags: TW3CreationFlags;
begin
  result := [];
end;

procedure TWbWindowHeaderTitle.SetLeft(const NewLeft: integer);
begin
end;

procedure TWbWindowHeaderTitle.SetTop(const NewTop: integer);
begin
end;

procedure TWbWindowHeaderTitle.MoveTo(const NewLeft, NewTop: integer);
begin
end;

procedure TWbWindowHeaderTitle.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
end;

procedure TWbWindowHeaderTitle.SetBounds(const NewBounds: TRect);
begin
end;
{$ENDIF}

//#############################################################################
// TWbWindowGlyph
//#############################################################################

{$IFDEF USE_FLEX}
procedure TWbWindowGlyph.InitializeObject;
begin
  inherited;
  PositionMode := pmRelative;
  DisplayMode := TW3TagDisplayMode.dmBlock;
  Handle.style['flex-shrink'] := '0';

  Handle.style['flex-grow'] := '0';
  UpdateBaseSize();
end;

function TWbWindowGlyph.CreationFlags: TW3CreationFlags;
begin
  result := [];
end;

procedure TWbWindowGlyph.UpdateBaseSize;
begin
  Handle.style['flex-basis'] := Width.ToString() + 'px';
end;

procedure TWbWindowGlyph.SetWidth(const NewWidth: integer);
begin
  inherited SetWidth(NewWidth);
end;

procedure TWbWindowGlyph.SetLeft(const NewLeft: integer);
begin
end;

procedure TWbWindowGlyph.SetTop(const NewTop: integer);
begin
end;

procedure TWbWindowGlyph.MoveTo(const NewLeft, NewTop: integer);
begin
end;

procedure TWbWindowGlyph.SetBounds(const NewLeft, NewTop, NewWidth, NewHeight: integer);
begin
end;

procedure TWbWindowGlyph.SetBounds(const NewBounds: TRect);
begin
end;
{$ENDIF}

//#############################################################################
// TWbWindowElement
//#############################################################################

procedure TWbWindowElement.StyleAsEnabled;
begin
end;

procedure TWbWindowElement.StyleAsDisabled;
begin
end;

//#############################################################################
// TWbWindowRightSide
//#############################################################################

procedure TWbWindowRightSide.InitializeObject;
begin
  inherited;
  TransparentEvents := true;
end;

procedure TWbWindowRightSide.FinalizeObject;
begin
  if FScrollbar <> nil then
    FScrollbar.free;
  inherited;
end;

procedure TWbWindowRightSide.StyleAsEnabled;
begin
  Background.FromColor(CNT_STYLE_WINDOW_BASE_SELECTED);
  if FScrollbar <> nil then
    FScrollbar.StyleAsEnabled();
end;

procedure TWbWindowRightSide.StyleAsDisabled;
begin
  Background.FromColor(CNT_STYLE_WINDOW_BASE_UNSELECTED);
  if Scrollbar <> nil then
    Scrollbar.StyleAsDisabled();
end;

procedure TWbWindowRightSide.ShowScrollbar;
begin
  if FScrollbar = nil then
  begin
    FScrollbar := TWbVerticalScrollbar.Create(self);
    FScrollbar.TransparentEvents := true;
    FScrollbar.Total := 2000;
    FScrollbar.PageSize := 600;
    FScrollbar.Position := 200;
  end;
end;

procedure TWbWindowRightSide.HideScrollbar;
begin
  if FScrollbar <> nil then
  begin
    FScrollbar.free;
    FScrollbar := nil;
    invalidate;
  end;
end;

function TWbWindowRightSide.GetCalculatedWidth: integer;
begin
  result := if (FScrollbar <> nil) then
    if FScrollbar.width < 16 then 16 else FScrollbar.width
    //TInteger.EnsureRange(FScrollbar.width, 16, MAX_INT)
  else
    0;
end;

procedure TWbWindowRightSide.Resize;
begin
  //inherited;
  if FScrollbar <> nil then
  begin
    var wd := GetCalculatedWidth();
    var dx := if wd > 0 then ((ClientWidth div 2) - (wd div 2)) else (ClientWidth div 2);
    FScrollbar.SetBounds(dx, 0, wd, ClientHeight);
  end;
end;

//#############################################################################
// TWbWindowFooter
//#############################################################################

procedure TWbWindowFooter.FinalizeObject;
begin
  if FScrollbar <> nil then
    FScrollbar.free;
  inherited;
end;

procedure TWbWindowFooter.StyleAsEnabled;
begin
  Background.FromColor(CNT_STYLE_WINDOW_BASE_SELECTED);
  if FScrollbar <> nil then
    FScrollbar.StyleAsEnabled();
end;

procedure TWbWindowFooter.StyleAsDisabled;
begin
  Background.FromColor(CNT_STYLE_WINDOW_BASE_UNSELECTED);
  if FScrollbar <> nil then
    FScrollbar.StyleAsDisabled();
end;

procedure TWbWindowFooter.ShowScrollbar;
begin
  if not (csDestroying in ComponentState) then
  begin
    if FScrollbar = nil then
    begin
      FScrollbar := TWbHorizontalScrollbar.Create(self);
      FScrollbar.Total := 2000;
      FScrollbar.PageSize := 600;
      FScrollbar.Position := 200;
      if Visible then
        Invalidate();
    end;
  end;
end;

procedure TWbWindowFooter.HideScrollbar;
begin
  if not (csDestroying in ComponentState) then
  begin
    if FScrollbar <> nil then
    begin
      try
        try
          FScrollbar.free;
        except
          // sink exception
        end;
      finally
        FScrollbar := nil;
      end;

      if Visible then
        Invalidate();
    end;
  end;
end;

function TWbWindowFooter.GetCalculatedHeight: integer;
begin
  result := if (FScrollbar <> nil) then
    if FScrollbar.height < 16 then 16 else FScrollbar.height
    //TInteger.EnsureRange(FScrollbar.height, 16, MAX_INT)
  else
    0;
end;

procedure TWbWindowFooter.Resize;
begin
  //inherited;
  if FScrollbar <> nil then
  begin
    var LBounds := ClientRect;
    var hd := GetCalculatedHeight();
    var dy := if hd > 0 then
      ( (LBounds.Height div 2) - (hd div 2) )
    else
      ( LBounds.Height div 2 );

    FScrollbar.SetBounds(0, dy, clientwidth, hd);
  end;
end;

//#############################################################################
// TWbWindow
//#############################################################################

function TWbWindow.GetHScroll: TWbHorizontalScrollbar;
begin
  result := Footer.Scrollbar;
end;

function TWbWindow.GetVScroll: TWbVerticalScrollbar;
begin
  result := RightEdge.Scrollbar;
end;

procedure TWbWindow.HandleMouseDown(Sender: TObject; Button: TMouseButton;
                    Shift: TShiftState; X, Y: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    var LAccess := GetDesktop();
    var LActive := LAccess.GetActiveWindow();

    if LActive <> nil then
    begin
      if LActive <> self then
      begin
        {$IFDEF DEBUG}
        WritelnF("[%s] Another window is set as active, changing", [{$I %FUNCTION%}]);
        {$ENDIF}
        if LAccess <> nil then
          LAccess.SetFocusedWindow(self);
        self.SetFocus();

        {$IFDEF DEBUG}
        Writeln("Calling DropFocus() on the previously active window");
        {$ENDIF}

        LActive.DropFocus();
        if LActive.Styled then
        begin
          {$IFDEF DEBUG}
          WritelnF("[%s] Other window styled as active, removing", [{$I %FUNCTION%}]);
          {$ENDIF}
          LActive.StyleAsUnFocused();
        end;

        if not Styled then
        begin
          {$IFDEF DEBUG}
          WritelnF("[%s] Applying focused styling to this window", [{$I %FUNCTION%}]);
          {$ENDIF}
          StyleAsFocused();
        end;

      end else
      begin
        {$IFDEF DEBUG}
        WritelnF("[%s] We are already set as the selected window", [{$I %FUNCTION%}]);
        {$ENDIF}
        if not Styled then
        begin
          {$IFDEF DEBUG}
          WritelnF("[%s] Styling missing somehow, applying", [{$I %FUNCTION%}]);
          {$ENDIF}
          StyleAsFocused();
        end;
      end;

    end else
    begin
      {$IFDEF DEBUG}
      WritelnF("[%s] No window set as focused, setting this one", [{$I %FUNCTION%}]);
      {$ENDIF}
      if LAccess <> nil then
        LAccess.SetFocusedWindow(self);
      SetFocus();

      if not Styled then
      begin
        {$IFDEF DEBUG}
        WritelnF("[%s] Frame not styled, applying that", [{$I %FUNCTION%}]);
        {$ENDIF}
        StyleAsFocused();
      end;
    end;
  end;

  inherited HandleMouseDown(Sender, Button, Shift, X, Y);
end;

//#############################################################################
// TWbCustomWindow
//#############################################################################

procedure TWbCustomWindow.InitializeObject;
begin
  inherited;

  SimulateMouseEvents := true;
  TransparentEvents := true;

  FMinWidth := 200;
  FMinHeight := 200;

  FLeft     := TWbWindowLeftSide.Create(self);
  FRight    := TWbWindowRightSide.Create(self);
  FRight.TransparentEvents := true;

  FFooter   := TWbWindowFooter.Create(self);
  FContent  := CreateContentInstance();
  FSizer    := TWbWindowSizerGlyph.Create(self);
  FHeader   := TWbWindowHeader.Create(self);

  GetDesktop().RegisterWindow(Self);

  var FBringToFrontEvent := TW3ElementDblClickEvent.Create(FHeader,TW3DomEventMode.dmCapture);
  FBringToFrontEvent.OnEvent := procedure (sender: TObject; EventObj: JEvent)
  begin
    if not TopMost() then
      BringToFront();
  end;

  SetInitialTransformationStyles();

  // Yeah, we want hardware accelleration
  //WillChange("left, top, width, height");

  TW3Dispatch.Execute(procedure ()
  begin
    SetFocus();
    Invalidate();
  end, 300);
end;

procedure TWbCustomWindow.FinalizeObject;
begin
  FBringToFrontEvent.free;
  FHeader.free;
  FContent.free;
  FFooter.free;
  FRight.free;
  FLeft.free;
  FSizer.free;
  inherited;
end;

function TWbCustomWindow.CreateContentInstance: TWbWindowContent;
begin
  result := TWbWindowContent.Create(self);
end;

procedure TWbCustomWindow.CenterOnParent;
begin
  if Parent <> nil then
  begin
    if parent is TW3MovableControl then
    begin
      var LParent := TW3MovableControl(Parent);
      var wd := LParent.ClientWidth;
      var hd := LParent.ClientHeight;
      var dx := (wd div 2) - (Width div 2);
      var dy := (hd div 2) - (Height div 2);


      self.MoveTo(if dx < 0 then 0 else dx, if dy < 0 then 0 else dy);
    end;
  end;
end;

procedure TWbCustomWindow.CloseWindow;
begin
  if not (csDestroying in ComponentState) then
  begin
    OnWindowMoveBegins := nil;
    OnWindowMoveEnds := nil;
    OnWindowReSizeBegins := nil;
    OnWindowReSizeEnds := nil;

    try
      if assigned(OnWindowClose) then
      begin
        OnWindowClose(self);
        OnWindowClose := nil;
      end;
    finally
      var LAccess := GetDesktop();
      LAccess.UnRegisterWindow(self);
      LAccess.SetFocusedWindow(nil);
      FreeAfter(100);
    end;
  end;
end;

function TWbCustomWindow.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  include(result, cfKeyCapture);
  include(result, cfAllowSelection);
end;

procedure TWbCustomWindow.SetOptions(const NewOptions: TWbWindowOptions);
begin
  FOptions := NewOptions;

  FSizer.visible := (woSizeable in FOptions);

  if (woHScroll in FOptions) then
    FFooter.ShowScrollbar()
  else
    FFooter.HideScrollbar();

  if (woVScroll in FOptions) then
    FRight.ShowScrollbar()
  else
    FRight.HideScrollbar();
end;

procedure TWbCustomWindow.ObjectReady;
begin
  inherited;

  // Experimental
  if (woAutoInitSize in FOptions) then
  begin
    var LHost := GetDesktop();
    if LHost <> nil then
    begin
      var LRect := LHost.GetInitialPosition(FMinWidth, FMinHeight);
      SetBounds(LRect);
    end;
  end;

  FHeader.CloseGlyph.OnClick := HandleCloseClick;
  FHeader.ZOrderGlyph.OnClick := HandleZOrderClick;
  FHeader.MinimizeGlyph.OnClick := HandleMinimizeClick;
  FHeader.FullScreen.OnClick := @HandleFullScreenClick;

  /* Setup event handlers */
  OnMouseDown := HandleMouseDown;
  OnMouseMove := HandleMouseMove;
  OnMouseUp   := HandleMouseUp;
  //OnGotFocus  := HandleGotFocus;

  FSizer.SetSize(20, 20);
  FSizer.Background.Size.Mode := smContain;

  Background.FromColor(CNT_STYLE_Background);

  EdgeRadius.Top.Left := 4;
  EdgeRadius.Top.Right := 4;

  Border.Size := 1;
  border.color := CNT_STYLE_EDGE_DARKEST;
  Border.Style := besSolid;
end;

procedure TWbCustomWindow.Resize;
begin
  //inherited;
  if not (csDestroying in ComponentState) then
  begin
    var LHeader_Height := 29;
    var LFooter_Height := 4 + FFooter.GetCalculatedHeight();
    var LRight_Width := 4 + FRight.GetCalculatedWidth();
    var LSizer_Width := 20;
    var LSizer_Height := 20;
    var LLeft_Width := 4;

    if (woSizeable in FOptions) then
    begin
      if LFooter_Height < 20 then
        LFooter_Height := 20;
    end;

    var LBounds := ClientRect;

    FHeader.SetBounds(
      LBounds.left,
      LBounds.top,
      LBounds.width,
      LHeader_Height);

    FContent.SetBounds(
      LLeft_Width,
      LHeader_Height,
      ClientWidth - (LRight_Width + LLeft_Width),
      ClientHeight -( LHeader_Height + LFooter_Height) );

    FLeft.SetBounds(
      0,
      LHeader_Height,
      LLeft_Width,
      ClientHeight - LHeader_Height );

    if ((woSizeable in FOptions) and FSizer.visible) then
    begin
      FRight.SetBounds(
        Clientwidth - LRight_Width,
        LHeader_Height,
        LRight_Width,
        ClientHeight - (LHeader_Height + LSizer_Height) );
    end else
    begin
      FRight.SetBounds(
        Clientwidth - LRight_Width,
        LHeader_Height,
        LRight_Width,
        ClientHeight - (LHeader_Height ) );
    end;

    FFooter.SetBounds(
      FContent.left,
      FContent.top + FContent.Height,
      FContent.width,
      LFooter_Height);

    if (woSizeable in FOptions) then
    begin
      if FSizer.visible then
        FSizer.SetBounds(
          Clientwidth - LSizer_Width,
          ClientHeight - LSizer_Height,
          LSizer_Width,
          LSizer_Height
          );
    end;
  end;
end;

procedure TWbCustomWindow.WindowStateChanged(const NewState: TWbWindowState);
begin
end;

function TWbCustomWindow.GetFullScreenElement: TControlHandle;
begin
  result := FContent.Handle;
end;

procedure TWbCustomWindow.HandleFullScreenClick(Sender: TObject);
begin
  var LMethod: TProcedureRef;
  var LRef := GetFullScreenElement();

  asm
    @LMethod = (@LRef).requestFullscreen ||
               (@LRef).mozRequestFullScreen ||
               (@LRef).webkitRequestFullscreen ||
               (@LRef).msRequestFullscreen;
  end;

  if assigned(LMethod) then
    LMethod() else
  writeln("The fullscreen API could not be found");
end;

procedure TWbCustomWindow.HandleMinimizeClick(Sender: TObject);
begin
  if Parent <> nil then
  begin

    var LAccess := GetDesktop();
    if not LAccess.GetModalIsActive() then
    begin

      var LRect := LAccess.GetApplicationRegion();

      case FState of
      wsNormal:
        begin
          FState := wsMaximized;
          FOldSize := BoundsRect;
           fxScaleTo(LRect.left, LRect.top, LRect.width, LRect.height, 0.2,
            procedure ()
            begin
              WindowStateChanged(FState);
            end);
          //SetBounds(LRect);
          //WindowStateChanged(FState);
        end;
      wsMaximized:
        begin
          FState := wsNormal;
          fxScaleTo(FOldSize.left, FOldSize.top, FOldSize.width, FOldSize.height, 0.2,
            procedure ()
            begin
              WindowStateChanged(FState);
            end);
        end;
      wsMinimized:
        begin
          WindowStateChanged(FState);
        end;
      end;
    end;
  end;
end;

procedure TWbCustomWindow.HandleZOrderClick(Sender: TObject);
begin
  var LAccess := GetDesktop();
  if not LAccess.GetModalIsActive() then
  begin
    case Topmost of
    true:   LAccess.SendWindowToBack(self);
    false:  LAccess.BringWindowToFront(self);
    end;
  end;
end;

procedure TWbCustomWindow.HandleCloseClick(Sender: TObject);
begin
  TW3Dispatch.Execute(CloseWindow, 100);
end;

procedure TWbCustomWindow.StyleAsFocused(const CB: TProcedureRef);
begin
  StyleAsFocused();
  if assigned(CB) then
    CB();
end;

procedure TWbCustomWindow.StyleAsUnFocused(const CB: TProcedureRef);
begin
  StyleAsUnFocused();
  if assigned(CB) then
    CB();
end;

procedure TWbCustomWindow.StyleAsFocused;
begin
  try
    Content.EnableInput();

    // Add active styles
    FHeader.StyleAsEnabled();

    FSizer.TagStyle.Add("TWbWindowSizerGlyph_focused");

    // Set focused colors
    Background.fromColor(CNT_STYLE_WINDOW_BASE_SELECTED);
    FLeft.Background.FromColor(CNT_STYLE_WINDOW_BASE_SELECTED);

    FRight.StyleAsEnabled();
    FFooter.StyleAsEnabled();
  finally
    FStyled := true;
  end;
end;

procedure TWbCustomWindow.StyleAsUnFocused;
begin
  try
    Content.DisableInput(false);
    FHeader.StyleAsDisabled();

    FSizer.TagStyle.RemoveByName("TWbWindowSizerGlyph_focused");

    // Set focused colors
    Background.fromColor(CNT_STYLE_WINDOW_BASE_UNSELECTED);
    FLeft.Background.FromColor(CNT_STYLE_WINDOW_BASE_UNSELECTED);

    FRight.StyleAsDisabled();
    FFooter.StyleAsDisabled();
  finally
    FStyled := false;
  end;
end;

procedure TWbCustomWindow.UnSelect;
begin
  // Remove styling by brute force. Actually quicker :/
  var LAccess := GetDesktop();
  LAccess.SetFocusedWindow(nil);

  var LList := LAccess.GetWindowList();
  if LList.count > 0 then
  begin
    for var x:=low(LList) to high(LList) do
    begin
      var LItem := LList[x];
      if LItem.Styled then
        LItem.StyleAsUnFocused();
    end;
  end;
end;

procedure TWbCustomWindow.HandleGotFocus(Sender: TObject);
begin
  {$IFDEF DEBUG}
  writelnF("[%s] GotFocus event catched",[{$I %FUNCTION%}]);
  {$ENDIF}
  if not Styled then
  begin
    var LAccess := GetDesktop();
    LAccess.SetFocusedWindow(self);

    // Remove styling by brute force. Actually quicker :/
    var LList := LAccess.GetWindowList();
    if LList.count > 0 then
    begin
      for var x:=low(LList) to high(LList) do
      begin
        var LItem := LList[x];
        if LItem <> self then
        begin
          if LItem.Styled then
            LItem.StyleAsUnFocused();
        end;
      end;
    end;
  end;
end;

procedure TWbCustomWindow.BeforeResize;
begin
  if not (csDestroying in ComponentState) then
  begin
    if assigned(OnWindowReSizeBegins) then
      OnWindowResizeBegins(self);
  end;
end;

procedure TWbCustomWindow.AfterResize;
begin
  if not (csDestroying in ComponentState) then
  begin
    if assigned(OnWindowResizeEnds) then
      OnWindowResizeEnds(self);
  end;
end;

procedure TWbCustomWindow.HandleMouseDown(Sender: TObject; Button: TMouseButton;
                    Shift: TShiftState; X, Y: integer);
begin
  if Button = TMouseButton.mbLeft then
  begin
    if FHeader.BoundsRect.ContainsPos(x, y) then
    begin
      /* Dont override glyph's natural behavior */
      if FHeader.ZOrderGlyph.BoundsRect.ContainsPos(x, y)
      or FHeader.MinimizeGlyph.BoundsRect.ContainsPos(x, y)
      or FHeader.CloseGlyph.BoundsRect.ContainsPos(x, y)
      or FHeader.FullScreen.BoundsRect.ContainsPos(x, y) then
      exit;

      if FHeader.FullScreen.BoundsRect.ContainsPos(x, y) then
      exit;

      if not Styled then
        StyleAsFocused();

      SetCapture();

      FContent.DisableInput(false);
      FStartPos := TPoint.Create(x, y);
      FMoveActive := true;

      if assigned(OnWindowMoveBegins) then
        OnWindowMoveBegins(self);

    end else
    if FSizer.BoundsRect.ContainsPos(x,y) then
    begin
      FSizeActive := true;

      FContent.DisableInput(false);

      if not Styled then
        StyleAsFocused();

      SetCapture();

      // Center of sizer glyph as origo
      FStartPos.x := FSizer.BoundsRect.left + (FSizer.BoundsRect.width div 2);
      FStartPos.y := FSizer.BoundsRect.top + (FSizer.BoundsRect.height div 2);
      FSizeActive := true;

      BeforeResize();
    end;
  end;
end;

procedure TWbCustomWindow.HandleMouseMove(Sender: TObject;
          Shift: TShiftState; X, Y: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FMoveActive then
    begin
      {$IFNDEF USE_CSSMOVE}
      var temp :=  left + (x - FStartPos.x);
      var dx := if temp < 0 then 0 else temp;

      temp := top + (y - FStartPos.y);
      var dy := if temp < 0 then 0 else temp;

      MoveTo(dx, dy);
      {$ELSE}
      var LCurrent := ClientToScreen(TPoint.Create(x, y));
      x := LCurrent.x;
      y := LCurrent.y;

      var FStartPos_New := ClientToScreen(FStartPos);
      dec(x, FStartPos_New.x);
      dec(y, FStartPos_New.y);

      var Temp := 'translate(';
      asm
        @temp += (@x).toString();
        @temp += "px,";
        @temp += (@y).toString();
        @temp += "px)";
      end;
      Handle.style[CSSTransform] := temp;
      {$ENDIF}

    end else
    if FSizeActive then
    begin
      var temp := x;
      dec(temp, FStartpos.x);
      inc(temp, width);
      var wd := if temp < FMinWidth then FMinWidth else temp;

      temp := y;
      dec(temp, FStartpos.y);
      inc(temp, height);
      var hd := if temp < FMinHeight then FMinHeight else temp;

      SetSize(wd, hd);

      FStartPos.x := if (width = FMinWidth) then ( FSizer.BoundsRect.left + (FSizer.BoundsRect.width div 2)) else x;
      FStartPos.y := if (height = FMinHeight) then ( FSizer.BoundsRect.top + (FSizer.BoundsRect.height div 2)) else y;
    end;
  end;
end;

procedure TWbCustomWindow.HandleMouseUp(Sender: TObject; Button: TMouseButton;
          Shift: TShiftState; X, Y: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FMoveActive then
    begin
      FMoveActive := false;
      ReleaseCapture();
      FContent.EnableInput();

      var temp :=  left + (x - FStartPos.x);
      var dx := if temp < 0 then 0 else temp;

      temp := top + (y - FStartPos.y);
      var dy := if temp < 0 then 0 else temp;

      MoveTo(dx, dy);

      {$IFDEF USE_CSSMOVE}
      var CSSTransform := BrowserAPI.Prefix('Transform');
      Handle.style[CSSTransform] := '';
      {$ENDIF}

      if assigned(OnWindowMoveEnds) then
        OnWindowMoveEnds(self);
    end else

    if FSizeActive then
    begin
      FSizeActive := false;
      ReleaseCapture();
      FContent.EnableInput();

      AfterResize();
    end;
  end;
end;

//#############################################################################
// TWbWindowContent
//#############################################################################

procedure TWbWindowContent.InitializeObject;
begin
  inherited;

  FBlocker := TW3WindowFocusBlock.Create(self);
  FBlocker.PositionMode := pmRelative;
  FBlocker.visible := false;

  FBlocker.Background.fromColor(clBlue);
  FBlocker.AlphaBlend := true;
  FBlocker.Opacity := 5;
end;

procedure TWbWindowContent.FinalizeObject;
begin
  FBlocker.free;
  inherited;
end;

procedure TWbWindowContent.DisableInput(const BlockPermanent: boolean);
begin
  if not InputDisabled then
  begin
    FPermanent := BlockPermanent;
    Handle.style["pointer-events"] := "none";
    FBlocker.BringToFront();
    FBlocker.visible := true;

    if assigned(OnInputDisabled) then
      OnInputDisabled(self);
  end;
end;

procedure TWbWindowContent.EnableInput;
begin
  if InputDisabled then
  begin
    if not FPermanent then
    begin
      FBlocker.SendToBack();
      FBlocker.visible := false;
      Handle.style["pointer-events"] := "initial";

      if assigned(OnInputEnabled) then
        OnInputEnabled(self);
    end;
  end;
end;

//#############################################################################
// TWbWindowHeader
//#############################################################################

procedure TWbWindowHeader.InitializeObject;
begin
  inherited;
  FClose := TWbWindowCloseGlyph.Create(self);
  FTitle := TWbWindowHeaderTitle.Create(self);
  FFull := TWbWindowFullScreenGlyph.Create(self);
  FFull.Visible := false;
  FMinimize := TWbWindowMinimizeGlyph.Create(self);
  FZOrder:= TWbWindowZOrderGlyph.Create(self);
  StyleAsEnabled();
  Displaymode := dmFlex;
end;

procedure TWbWindowHeader.FinalizeObject;
begin
  FTitle.free;
  FClose.free;
  FZOrder.free;
  FFull.free;
  FMinimize.free;
  inherited;
end;

function TWbWindowHeader.CreationFlags: TW3CreationFlags;
begin
  {$IFDEF USE_FLEX}
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);
  Exclude(result, cfReportChildAddition);
  Exclude(result, cfReportChildRemoval);
  Exclude(result, cfAllowSelection);
  Exclude(result,  cfReportResize);
  Exclude(result, cfKeyCapture);
  {$ELSE}
  result := [cfReportResize];
  {$ENDIF}
end;

procedure TWbWindowHeader.StyleTagObject;
begin
  inherited;
  ThemeReset();
  {$IFDEF USE_FLEX}
  handle.style['flex-flow'] := 'row nowrap';
  {$ENDIF}
end;

procedure TWbWindowHeader.StyleAsEnabled;
begin
  if not (csDestroying in ComponentState) then
  begin
    TagStyle.Add("TWbWindowHeader_focused");
    FZOrder.TagStyle.Add("TWbWindowZOrderGlyph_focused");
    FMinimize.TagStyle.Add("TWbWindowMinimizeGlyph_focused");
    FClose.TagStyle.Add("TWbWindowCloseGlyph_focused");
    FFull.TagStyle.Add("TWbWindowFullScreenGlyph_focused");
  end;
end;

procedure TWbWindowHeader.StyleAsDisabled;
begin
  if not (csDestroying in ComponentState) then
  begin
    TagStyle.RemoveByName("TWbWindowHeader_focused");
    FZOrder.TagStyle.RemoveByName("TWbWindowZOrderGlyph_focused");
    FMinimize.TagStyle.RemoveByName("TWbWindowMinimizeGlyph_focused");
    FClose.TagStyle.RemoveByName("TWbWindowCloseGlyph_focused");
    FFull.TagStyle.RemoveByName("TWbWindowFullScreenGlyph_focused");
  end;
end;

procedure TWbWindowHeader.ObjectReady;
begin
  inherited;

  FTitle.Font.Color := clWhite;
  FTitle.TextShadow.Shadow(1, 1, 0, clBlack);
  FTitle.AutoSize := false;
  FTitle.Ellipsis := true;

  Border.Bottom.Width:= 1;
  Border.Bottom.Color:= CNT_STYLE_EDGE_DARKEST;
  Border.Bottom.Style:= besSolid;
end;

procedure TWbWindowHeader.Resize;
begin
  {$IFNDEF USE_FLEX}
  var LRect := AdjustClientRect(ClientRect);
  var wd := LRect.width;

  if FClose.visible then dec(wd, FClose.width);
  if FFull.Visible then dec(wd, FFull.width);
  if FMinimize.visible then dec(wd, FMinimize.width);
  if FZOrder.visible then dec(wd, FZOrder.width);

  FClose.height := LRect.height;
  FMinimize.Height := LRect.height;
  FZOrder.Height := LRect.height;
  FFull.Height := LRect.height;

  var dx := LRect.left;
  if FClose.visible then
  begin
    FClose.MoveTo(dx, LRect.top);
    inc(dx, FClose.width);
  end;

  FTitle.SetBounds(dx, LRect.top, wd, LRect.height);
  inc(dx, FTitle.width);

  if FFull.Visible then
  begin
    FFull.MoveTo(dx, LRect.top);
    inc(dx, FFull.width);
  end;

  if FMinimize.Visible then
  begin
    FMinimize.MoveTo(dx, LRect.top);
    inc(dx, FMinimize.width);
  end;

  if FZOrder.Visible then
  begin
    FZOrder.MoveTo(dx, LRect.top);
  end;
  {$ENDIF}
end;

initialization
begin
  // Setup the pre-cache GPU styletext for windows
  TWbCustomWindow.CssTransform := BrowserAPI.Prefix('Transform');

  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var StyleCode := #'

    .TWbClickBlocker {
      overflow: hidden;
      -webkit-touch-callout: none;
	    background-color: rgba(0, 0, 0, 0.5);
    }

    .TWbWindowLeftSide {
      border-left: 1px solid #92BFFF;
    }

    .TWbWindowRightSide {
      border-left: 1px solid #92BFFF;
      border-right: 1px solid #204D8D;
    }

    .TWbWindowFooter {
      border-top: 1px solid #92BFFF;
    }

    .TW3WindowFocusBlock {
      padding: 0px !important;
      margin: 0px !important;
      overflow-x: hidden;
      overflow-y: hidden;
      width: 100% !important;
      height: 100% !important;
    }

    .TWbWindowContent, .TWbWindowContentText {
      padding: 0px !important;
      margin: 0px !important;

      background: #CFCFCF;
      border-bottom: 1px solid #19234D;
      border-right: 1px solid #19234D;
      border-left: 1px solid #19234D;
    }

    .TWbCustomWindow, .TWbWindow, .TWbExternalWindow,
    .TWbShellWindow, .TWbWindowDirectory {
      padding: 0px !important;
      margin: 0px !important;
      overflow-x: hidden;
      overflow-y: hidden;
      box-shadow: 4px 4px 4px rgba(0,0,0,0.2);
    }

    .TWbWindowHeader {
      cursor: default;
      padding: 0px !important;
      margin: 0px !important;
      background: rgb(76,120,179);
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWgAAAAdCAYAAACHffYTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAFJ0lEQVR4Xu2byUp2SQyG6/4XXsvvPM8TIgiCgoKiLty4ceO+mtMQCCF5K2XlNKe7s3iQ804Vvr3l4eGhJkmSJMujnJ2d1SRJkmR5lNXV1arx588fVY9kemOOd3p3KY96yPdq0Vj3jNK7S3nUQ75Xi8a6Z5TeXcqjHvK9WjTWPaP07lIe9ZDv1aKx7uGU9fX1qrG2tqbqv2HasvYi9Na+1ZEa6XPmf4P1Bnmjemvf6kiN9Dnzv8F6g7xRvbVvdaRG+pz532C9Qd6o3tq3OlIjfc78b7DeIA/pZXNzs25sbJhMPqHpXOMgT4P2+K7UiFZPIjMcLT/R8r1Yb7X2kadBe3xXakSrJ5EZjpafaPlerLda+8jToD2+KzWi1ZPIDEfLT7R8L9ZbrX3kadAe35Ua0epJZIaj5SdavhfrrdY+8jRoj+9Of8vW1lbVmExNj4SO0bwRencpj3rI92rRWPeM0rtLedRDvleLxrpnlN5dyqMe8r1aNNY9o/TuUh71kO/VorHu4ZSdnZ2qsb29reqRTG/M8U7vLuVRD/leLRrrnlF6dymPesj3atFY94zSu0t51EO+V4vGumeU3l3Kox7yvVo01j2csre3V3d3d00mn9B0rnGQp0F7fFdqRKsnkRmOlp9o+V6st1r7yNOgPb4rNaLVk8gMR8tPtHwv1lutfeRp0B7flRrR6klkhqPlJ1q+F+ut1j7yNGiP70qNaPUkMsPR8hMt34v1VmsfeRq0x3env+Xg4KBq7O/vq3ok0xtzvNO7S3nUQ75Xi8a6Z5TeXcqjHvK9WjTWPaP07lIe9ZDv1aKx7hmld5fyqId8rxaNdQ+nHB0dVY3Dw0NVj2R6Y453encpj3rI92rRWPeM0rtLedRDvleLxrpnlN5dyqMe8r1aNNY9o/TuUh71kO/VorHu4ZSTk5OqcXx8rOqRTG/M8U7vLuVRD/leLRrrnlF6dymPesj3atFY94zSu0t51EO+V4vGumeU3l3Kox7yvVo01j2cv/+T8PT01IT/V4tH49+aJ3My4wV1end5XushX/M42g7S+LfmyZzMeEGd3l2e13rI1zyOtoM0/q15MiczXlCnd5fntR7yNY+j7SCNf2uezMmMF9Tp3eV5rYd8zeNoO0jj35onczIjKRcXF/X8/Nxk8gmPxr81T+Zkxgvq9O7yvNZDvuZxtB2k8W/NkzmZ8YI6vbs8r/WQr3kcbQdp/FvzZE5mvKBO7y7Paz3kax5H20Ea/9Y8mZMZL6jTu8vzWg/5msfRdpDGvzVP5mRGUq6urqrG5eWlqkcyvTHHO727lEc95Hu1aKx7RundpTzqId+rRWPdM0rvLuVRD/leLRrrnlF6dymPesj3atFY93DK9fV11ZhMTY+EjtC8EXp3KY96yPdq0Vj3jNK7S3nUQ75Xi8a6Z5TeXcqjHvK9WjTWPaP07lIe9ZDv1aKx7uGUm5ubmiRJkiyPcnt7W5MkSZLlUe7u7mqSJEmyPMr9/X1NkiRJlkd5fHysSZIkyfIoz8/PdU6enp5U/Z9kCTf8lvz9xsjfb4z8/cYYvb28vr7W/zovLy+qruHN9mz+28nfb4z8/cb4P/9+5ePjoyZJkiTLo3x+ftYkSZJkeZSvr6+aJEmSLI/y/f1dkyRJkuVRfn5+apIkSbI8yvv7e9V4e3tT9UimN+Z4p3eX8qiHfK8WjXXPKL27lEc95Hu1aKx7RundpTzqId+rRWPdM0rvLuVRD/leLRrrHk5ZWVmpSZIkydJYqX8BJiUk3cGXLyQAAAAASUVORK5CYII=");
      box-shadow: 5px 5px 5px -5px #333;
    }

    .TWbWindowHeader: disabled {
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAWgAAAAdCAYAAACHffYTAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAFJ0lEQVR4Xu2byUp2SQyG6/4XXsvvPM8TIgiCgoKiLty4ceO+mtMQCCF5K2XlNKe7s3iQ804Vvr3l4eGhJkmSJMujnJ2d1SRJkmR5lNXV1arx588fVY9kemOOd3p3KY96yPdq0Vj3jNK7S3nUQ75Xi8a6Z5TeXcqjHvK9WjTWPaP07lIe9ZDv1aKx7uGU9fX1qrG2tqbqv2HasvYi9Na+1ZEa6XPmf4P1Bnmjemvf6kiN9Dnzv8F6g7xRvbVvdaRG+pz532C9Qd6o3tq3OlIjfc78b7DeIA/pZXNzs25sbJhMPqHpXOMgT4P2+K7UiFZPIjMcLT/R8r1Yb7X2kadBe3xXakSrJ5EZjpafaPlerLda+8jToD2+KzWi1ZPIDEfLT7R8L9ZbrX3kadAe35Ua0epJZIaj5SdavhfrrdY+8jRoj+9Of8vW1lbVmExNj4SO0bwRencpj3rI92rRWPeM0rtLedRDvleLxrpnlN5dyqMe8r1aNNY9o/TuUh71kO/VorHu4ZSdnZ2qsb29reqRTG/M8U7vLuVRD/leLRrrnlF6dymPesj3atFY94zSu0t51EO+V4vGumeU3l3Kox7yvVo01j2csre3V3d3d00mn9B0rnGQp0F7fFdqRKsnkRmOlp9o+V6st1r7yNOgPb4rNaLVk8gMR8tPtHwv1lutfeRp0B7flRrR6klkhqPlJ1q+F+ut1j7yNGiP70qNaPUkMsPR8hMt34v1VmsfeRq0x3env+Xg4KBq7O/vq3ok0xtzvNO7S3nUQ75Xi8a6Z5TeXcqjHvK9WjTWPaP07lIe9ZDv1aKx7hmld5fyqId8rxaNdQ+nHB0dVY3Dw0NVj2R6Y453encpj3rI92rRWPeM0rtLedRDvleLxrpnlN5dyqMe8r1aNNY9o/TuUh71kO/VorHu4ZSTk5OqcXx8rOqRTG/M8U7vLuVRD/leLRrrnlF6dymPesj3atFY94zSu0t51EO+V4vGumeU3l3Kox7yvVo01j2cv/+T8PT01IT/V4tH49+aJ3My4wV1end5XushX/M42g7S+LfmyZzMeEGd3l2e13rI1zyOtoM0/q15MiczXlCnd5fntR7yNY+j7SCNf2uezMmMF9Tp3eV5rYd8zeNoO0jj35onczIjKRcXF/X8/Nxk8gmPxr81T+Zkxgvq9O7yvNZDvuZxtB2k8W/NkzmZ8YI6vbs8r/WQr3kcbQdp/FvzZE5mvKBO7y7Paz3kax5H20Ea/9Y8mZMZL6jTu8vzWg/5msfRdpDGvzVP5mRGUq6urqrG5eWlqkcyvTHHO727lEc95Hu1aKx7RundpTzqId+rRWPdM0rvLuVRD/leLRrrnlF6dymPesj3atFY93DK9fV11ZhMTY+EjtC8EXp3KY96yPdq0Vj3jNK7S3nUQ75Xi8a6Z5TeXcqjHvK9WjTWPaP07lIe9ZDv1aKx7uGUm5ubmiRJkiyPcnt7W5MkSZLlUe7u7mqSJEmyPMr9/X1NkiRJlkd5fHysSZIkyfIoz8/PdU6enp5U/Z9kCTf8lvz9xsjfb4z8/cYYvb28vr7W/zovLy+qruHN9mz+28nfb4z8/cb4P/9+5ePjoyZJkiTLo3x+ftYkSZJkeZSvr6+aJEmSLI/y/f1dkyRJkuVRfn5+apIkSbI8yvv7e9V4e3tT9UimN+Z4p3eX8qiHfK8WjXXPKL27lEc95Hu1aKx7RundpTzqId+rRWPdM0rvLuVRD/leLRrrHk5ZWVmpSZIkydJYqX8BJiUk3cGXLyQAAAAASUVORK5CYII=");
    }

    .TWbWindowHeader_focused {
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAARQAAAAcCAYAAACgVECgAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAIp0lEQVR4Xu2Q2a8lVRnF6+9ziMYhGhWn6IMk+qAmChFxgLS2oEIiER/ERCM8YMQXukGUxkjjAMYmmmA0mjjEGcEYXkt2nbN2rVq11tl16t7b8Zp6+GV/3xp2nbO7iw/+td/Y2Ng4Dbq3X7jaF2648GQFGsO+yyRNYU8z8FRXj3GZ1FHNZaAnL4FO686USZrCnmbgqa4e4zKpo5rLQE9eAp3WnSmTNIU9zcBTXT3GZVJHNZeBnrwEOq07UyZpCnuaKXv3yo9e6guv+MjDFWgM+ymjaId7mJ2nunqMy7i9dWJ2+xLQST32U0bRDvcwO0919RiXcXvrxOz2JaCTeuynjKId7mF2nurqMS7j9taJ2e1LQCf12E8ZRTvcw6xe96qbLvfKq29+ZIBnhnOYWYPuTge6DHucPUTK6n3Y+dQMn6wnjWGPs6zDw8wadHc60GXY4+whUlbvw86nZvhkPWkMe5xlHR5m1qC704Euwx5nD5Gyeh92PjXDJ+tJY9jjLOvwMLMG3Z0OdAvda2/5bv+ajz06o+jA+QXntTqnwfX4BsC3+Hussc60/ILzWp3T4Hp8A+Bb/D3WWGdafsF5rc5pcD2+AfAt/h5rrDMtv+C8VqdF9/pPfL8vvO7W71WgMeynjKId7mF2nurqMS7j9taJ2e1LQCf12E8ZRTvcw+w81dVjXMbtrROz25eATuqxnzKKdriH2Xmqq8e4jNtbJ2a3LwGd1GM/ZRTtcA+zet0bP3WlL7zhk49XoDHsp4yiHe5hdp7q6jEu4/bWidntS0An9dhPGUU73MPsPNXVY1zG7a0Ts9uXgE7qsZ8yina4h9l5qqvHuIzbWydmty8BndRjP2UU7XAPs3rdm2//Qf+m256YUXRQ9U9f2UGZ6pFmO9SbkbzQm33jDMG3+Hussc5YX/6P68aOeYdK8kJv9o0zBN/i77HGOmN9+T+uGzvmHSrJC73ZN84QfIu/xxrrjPXl/7hu7Jh3qJDXvfUzT/aFt1z4YQUaw37KKNrhHmbnqa4e4zJub52Y3b4EdFKP/ZRRtMM9zM5TXT3GZdzeOjG7fQnopB77KaNoh3uYnae6eozLuL11Ynb7EtBJPfZTRtEO9zCr191w8am+8LbPXq1AY9hPGUU73MPsPNXVY1zG7a0Ts9uXgE7qsZ8yina4h9l5qqvHuIzbWydmty8BndRjP2UU7XAPs/NUV49xGbe3TsxuXwI6qcd+yija4R5m9bp33vnjvvCOO35UgcawnzKKdriH2Xmqq8e4jNtbJ2a3LwGd1GM/ZRTtcA+z81RXj3EZt7dOzG5fAjqpx37KKNrhHmbnqa4e4zJub52Y3b4EdFKP/ZRRtMM9zOp17/7CT/vCuz7/kwnQJ/7LhQHjK/UudKRXfWRorz711Hf3JI33RMrhzhat3qCbd0jUu9CRXvWRob361FPf3ZM03hMphztbtHqDbt4hUe9CR3rVR4b26lNPfXdP0nhPpBzubNHqDbp5h0S9Cx3pVR+Z/dy99+6f9YX33PXMBOgT/4tP7zC+Uu9CR3rVR4b26lNPfXdP0nhPpBzubNHqDbp5h0S9Cx3pVR8Z2qtPPfXdPUnjPZFyuLNFqzfo5h0S9S50pFd9ZGivPvXUd/ckjfdEyuHOFq3eoJt3SNS70JFe9ZHZz92N91zrb7zn2cr7vvTzl8+pttuvDd7oA/hjDjPyYw/3jdmpz/3yW9I3p7kpSS/svHLvXB+18Zs5k7Txt85z/r/AH3OYkR97uG/MTn3ub++H/NjDfWN26nN/ez/kxx7uG7NT/1rffeArv+wL77/3FxVoDPsukzSFPc3AU109xmVSRzWXgZ68BDqtO1MmaQp7moGnunqMy6SOai4DPXkJdFp3pkzSFPY0A0919RiXSR3VXAZ68hLotO5MmaQp7Gmm7N2H7/tVv5QPffW5gWEWL1HyH6SeI9255nv/y6z5PyW/vd+ONf+n5Lf327Hm/5T8Me/X3fSN3/SFm/dgh8a7o2a+/uuZt5Yl3z0Gvi/NDuenTtHVa91fqJnt/WZe6/5CzWzvN/Na9xdq5pTer7v1gd/1S/n4/b8d2M0+o5T8Ld8ce450J3/v/wH+P9v7HQ//n+39jof/z1m9X3f7t/7Qz/m90aCzN863Peg60JwHDnlMyYHkO/20OOb+Q7+RvXHe3o859BvZG+ft/ZhDv5G9cT6t9+s+950/9Uu5+NAfB4ZZvAQ66DnSna3eeYP/z/Z+x8P/Z3u/4+H/c1bv19196S/9Uu56+M8Dzkugc6iXvFbvvLHm/6BzqJe8Vu+8seb/oHOol7xW77yx5v+gc6jHXnfvY3/vT4MvP/Y3q28sY3u/k7G938k4rffrvvbEP/sdz+9PZdTvu/KPgeQ7DZ1dr+jzvL/zeeqxB1816PwNnh3OY+1QX/V2zv8f15t3dr2iz/P+zu39CujsekWf5/2d2/sV0Nn1ij7P853dA0+90K/iqtEC91/9l9VnHHHnuWd7v5Oxvd/JOKP36x565t99i28/vUM13qEl3Wmqu9whHbR8cExuSfYk97muy0F3muoud0gHLR8ck1uSPcl9ruty0J2mussd0kHLB8fklmRPcp/ruhx0p6le9u7Ss//pLxNlZ6A5j3X0geZcZm1ONczQlVYOvsuwxrCG7KGc81hHH2jOZdbmVMMMXWnl4LsMawxryB7KOY919IHmXGZtTjXM0JVWDr7LsMawhuyhnPNYRx9ozmVcrrvy3Eu94/E9zgPwOdfqnDfW/reSbeXhr/3GeWDtfyvZVh7+2m+cB9b+t5Jt5eGv/Yaju/PRF/vCHY+8UIHGDN7lPSGjTDrSw1wzcuekKx4z5PaoxnvrxOz2JaCTeoO34P8wk470MNeM3DnpiscMuT2q8d46Mbt9Ceik3uAt+D/MpCM9zDUjd0664jFDbo9qvLdOzG5fAjqpN3gL/g8z6UgPc80M+4v9fwEEDHoFLR3K0wAAAABJRU5ErkJggg==");
    }

    .TWbWindowCloseGlyph {
      cursor: default;
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAdCAYAAACwuqxLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAACrklEQVRIS7WUy0orQRCGA3kGH8FXcOFeyN43cOXKN1BwJQgGxQtRBIkQlRjxGjVGRU1MjBoTxfslKBJBQbJxX4evmp7T5nCyyiw+pqfqr/m7q7snICKB+fl5OTw8lOvr66ajHz86OvKNADNnsLq66gsBlpHJZGR9fb1prK2tKYzVIJvNysbGhpJMJj1szI27MZf/5TyDzc1NX/AMtre3m8rg4KCcnp4ag+PjY0mlUsrOzo5sbW1pD5eWlmRxcVFJJBKKfSfHJjJLW2e/ARicnZ0Zg1wuJ7u7ux70Lh6PSywWE47xwsLCL4iRQ4PWrbUMDQ1JsVg0Bvl8Xvb39z0oam9vl2Aw2BA0aN1aSzgcllKpZAxOTk7k4ODAgxa1tLRIf3+/to8JuBAjhwatW2sZHh6WcrlsDAqFgl42e+nS6bS0trbK8vLyPwa0k9jKyopq6L2t42nHIyMjcnl5aQzYbU4SF44nS2xra5NoNKoFxCxoiM3Ozqpmb2/Pq+Npx6Ojo3J1dWUM2G1mZmGJHR0dMjk5qQVuDoiRQ4O2Pg9jY2Nyc3NjDM7Pz3+1gRl2dnbqRjEbNwfEyKFBW5+H8fHxvwYcJ/bBwge6urpkYGBAZ+vmgBg5NGjr8zAxMSF3d3fG4OLiQttk4VT19PRIX1+f3sr6M06MHBq0bq0lEonI/f29MeA4YWKhZb29vdLd3a0fc28oECOHBq1ba5mampKnpydjwG5zpCy8z83NSSgUagia+lrL9PS0vLy8GAM2w+X29laX9/j42BA0aOvrYWZmRl5fX40BwoeHBw+KWd7z83ND0KB1a+23uCfv7+/GACHLaSb8DD8+PoxBpVKRt7c3X1ADBtVq1RfUgMHX15cvqMHn56fUajVfUIPv72/5+fnxBTXgvPpFgF8rf0Z/CMsf9mTVmGAvPj0AAAAASUVORK5CYII=");
    }

    .TWbWindowCloseGlyph_focused {
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAdCAYAAACwuqxLAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAD+klEQVRIS7XV20+TZxwH8CfhD1iy7HJbdrOL7WJ3XixZsmUxCxOnG4ownQcUJjAJUjIjdghSlQJtgbYgxcjR0VChnOnKoYzJaQIrTmASNCOVQhFG8YC7/O75vfJ2D+9eDsnixSfvr8/v+32fNDSBAWDRqTXQmH+F3jG3Bf821PMsWnMTx3P6JMdyfpYo552SO2KPaczDOK7/BV+kt2Hv+VaJcpZcULeP74jYkXrre5ZT50NsXj/2a9uxjwqcPItPmZhRnonkHdM7HuFk/gC+yujYVORFV4jamXguonOWd2sOccYhRF3qwsGsToly3im5I/ZYbr0f8aZhHMruDh0q552SO+STWCOyqsbA8vkFpwvvIOayJySahw9m8q/4Qwsitc04QE8Z/yydZbQiKsuFGF0Pvr7Si5grvEvW3/HpqQLobnr5Bc55JJhHcUTfh8P850VisjsRmV6PiNQqRJytVLVXU40DFxqkLHW/Efpk97eFuFo7DmZwLiDJMoajubdxlP9cyWGdG2+9/yHCwsK29DbPUFbqCn3yWYIVV+33wIyNASSXeBFrHMQJw8BLud147fU3YDKZ4PV6VdGOMpSl7oY+F55khb5uAszUuMgvGA+FSJzBgzffeReDg4OYnJxURTvKxBt7cdI0JBHfsSepGLmOKX5B02Ok2O4hjv+hZYmFt/HeB7vg8XgwMzOjinaUSSrqR3zRiER8R8SZEuQ33AcraF5C2vUpJBb/hgTrmPRMtg5j10e74XK54PP5VNGOMsnWISTxjiiRv2f/2TIUNM2AFbUs41zlNJJtv+NM6V1JaukIPv48Ck6nE4FAQBXtKENZ6op9Epl2A+bWh/yC1r+grXmAtPKpkHM3xrHnUBzsdjuCwaAq2lGGsmJXFv19Ja51zIKZW1eQaX+I89XTL1Xdh7Z6AlGxqaioqMDa2poq2lGGsnI3fR3NR7TVKOv0gVnaVpB9axYX7TMhl2qncSJFB61WC7/fr4p2lKEsdTLtDza841jGjyjv4f9wLO1B6J1zuMwvITrHn8hpmEV6URPCw8O3RBnKyl3RqSw7avoCYMWuIIwt88hr8oUYWh7B0u7nX3EB5Z6AKtpRhrJiV3ZaVwd7/2N+QccTmDsWUdg+v4HFtYAS9wJsXQFVtKOMsif7Tl+P+uFlsJKfnsLWvYTSrkXJNV6W5/9DY2hE8+gKv8D9DOW9K9ySYFkxi59FyvP/5lip+zlqB1ZhH1x5JZit8zka7jxB4+iqxDny71Oet7JdhpV1raHN+wwdd5+GuISnOIsZ5ZmcVeZYWfcLdE28QO8fa5vybDJvh7Lses/fMHauSgzuoEQ575TcEXtMUzSGL1Mcr4gD/wAfheMPGQyFTAAAAABJRU5ErkJggg==");
    }

    .TWbWindowZOrderGlyph {
      cursor: default;
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAdCAYAAAC5UQwxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAADoElEQVRIS72Vx0otQRCGD/gK+gbqA4gKIrhRxL2+gIqCC0FcuFFEDIiioqIiZsxizjnnnDHnBG7cuK/LVzKHmTlz5W7uWXznzHRV119dXd3jOjs7k//N8vKydHV1SUREhLhWVlbEGwwMDEhGRoa4hoeHxYmhoSE3TrZ/GbOTn58vrpGRERkdHfXgt3En29/8wbCVlZWJa2xsTMbHx93w/r9QwYmJCXGCjKh7T0+PdHd3KzzbwQdfYx5Jm+OYx1WwuLhYdnZ2ZGpqygJ70tnZKS0tLdLa2irt7e3S0dHhpq2tTW344GvMm5yctMQxj7sF9/b2ZHp62sLg4KCEhYWJj4/Pr+CDr32+HURVsLS0VPb392Vubs4CZfL19ZXs7Gw9R7T26uqqG96x4YOvMW92dtYSx2BmZuZHkJ/Dw0NZWFiwQAkCAgKkr69PBZeWlnQimRr7Qmn9/f2lv79fy0o3slf2WMBcFSwvL5ejoyMNaIaMQkJCpKmpSebn5zUBGoTm6e3t1USqqqokODhYnxnDhrg9FhDDLXhycuK+EYzykVFUVJRUV1drhgiGh4c77qMZfIw4RixYXFz8EaysrJTT01NZW1tT2B/+cYiNjZWSkhLNkBX7+flJXl6ebG9vy8bGhpBsXFycztna2pLc3Fz1MeIYsYAYKkhZzs/PNYAZsoqPj5eCggJ1ZpWBgYHaIByj9fV1KSwslISEBH1mDBs+9lhGPBWkZAhubm5aIKvU1FTJysrSW4JmYE8bGxu1cRjDhg++iHJWQ0NDPWIBq1XBmpoaubi40DKZoUSZmZmSnJzsvpoiIyO1jKyEd2z44Msq6uvrJTo62iMWkJQK1tbWyuXlpezu7lrgMuCGiYmJcRMUFKTfNPMYPviyioqKCt1TeywgIRWsq6uTm5sbOTg4sMDZpHuNjyhjSUlJ2kRcFMY4PviyyqKiIklMTPSIBdhVsKGhQe7u7uT4+NgCgehegrLHnNX09HTtUhqAFZE1peKdc5aTkyNpaWkesYCmUsHm5mZ5fHzUoL+BOAFTUlK0Yzk2ZhjDho/TfFapguzBy8uLXF1d/cr19bXeJub9cwIfp/msUgW5D9/f37WsTtzf32sF4Onp6a9gN+bc3t5aYgBbo4JOk808Pz8rVOH19dURbPg4zTfgJKjg29ubeANOggp+fn6KN3h4ePgR/Pr6Em9AyVXw+/tbvMHHx8ePoNGBdiiBgZPtX8bsqCA/3qNM/gA10/rDpOO5rwAAAABJRU5ErkJggg==");
    }

    .TWbWindowZOrderGlyph_focused {
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABwAAAAdCAYAAAC5UQwxAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAFgElEQVRIS7XUW1ATZxQH8Mw4vvjijFOetFM701bGmbYz7bTWOjq9UWkRud8vioJ4IQpIoBikAUIJECIoimkLqNxDgECAALkJhGiDLWIFqXihUBxUFK3D87/ft2GXXV3bl/rwm2/Pd86ew7ebRaLSzeJVS9YMIS5Th/UbAyHZpRpErGoA7MqiMYu//3xOLL9c476OVPQgqcyFLcH5kPjKu7DjWCd2ktWXrBSN6T6Lxnz8HD/PxZkkXuJ7rAveKTpE5lngI70AiV+WCXw75d0v4Of4tfx9YY7GXZxvj+oRpbQiItcMSUB2D/5/JgHf9FbEFtgRrbRAEpJrQXCOGXQNUph5euGfZYRfZht2fte6LKOFQ3P+WZ0I/r4PITkWRjC5l2L70N7+mR3YrepHDHmXks/2aKC48CtC86zMUFYgeSw+skZ4S6uwnS+pkkNzO2RNCDpuYu5/vgcVmmtFoLwTcYUDiJIbIfkivhS5tVcRnm8nbJwQRSfWeW7CihUr/tU6z0+YWvf9fO4+YURwdjf2qB2IyjRA8lViGZT1o4gif0EkOXZkgVuUsher17yG1NRU1NbWiqI5WhOd38fcz/Vg0T7kswjL6UWCxkkGtkPy9f5y5NdfQ0zREGIKBzlxKgvWrn8bVVVV0Ol0omhu7fq3EJNnRGiOCeG5vYhQmhFNBnG9ihzMJ5F44jIiZC2QeB8sR0Hj74jTXMIutZMYYsSXXITnex9Bq9XCYDCIojnPdz9EhLwefrJaBGQ0ICS7HVEFVq7X7hInYsgvdD/58JmB3xyqgKp5nBnIl6DpxweffonTp0/DZDIxtm3bJnh/K1euxKpVqwR7b2zcQj4Bm6BXrOoiDpx0ITxND4mP9CyK9ROILxtGfKmLk1jmwNbtQSgtLYXVamV4eHhAo9FgZGSEYbfb0dTUxMU0t3qNB+LVF7k+e4m4YgcOlV9BGB3ol/ITTrTfxIEzIwJJ5ZexPWgP1Go1HA4HY8OGDXA6nRgbG2P09/ejo6ODi2nu9TffQdKpS4JeiWW/4DBZw9PIIw1Mq0SZ8Rak2msCydrf4B+bDJVKBZfLxdi8eTNsNhsmJycZg4ODsFgsXExzG9//GMkVLkGvg+R0KWdHEUb+xUnC0s+hwjSFo1XjjNQlsurriJYqIJVK0dfXx/Dy8mLe5fT0NIMOoKdiY5rbtNULssoRrhddU34cRdrP190DI+U10PbNIKPmJvEHMi64yetu4mixHt7e3hx6QvqIjUYjZ2JiAnNzc4zW1lZ87hMK+flrgl7p1WPIPDeOUDow9ngdqqyzyG64JaBoug1Vyx2Udk7jVPcMTnbcQkBUIhoaGrCwsCCK5nzD90FRNy7olVVzA1nnJxCaSgbuVTSipv8+lPop5DUvU+rvoqD1TxS1z0DdQRhuI3JfOqqrq7G4uCiK5sITZMhvnhT0ym0kByBPLCS1GZJ9eTrUOx6i0DDDUBmmGcx129JKFBumkJD2A+RyOWZnZ0XRHK2htWyvQtqv5S6UZGgIPeEhlR76y/Mo7br3ghOds9z1KdM9KCo6BO9UDK2htYI+xhkU6e8gmA5MVbeh/cpjVJgfEPd5aLxMa3mAavtD8jTm0eR8JIrmaI3Wwu9zH2d67qGk7S6CU8gjrSIFfJW2B4L4fP88agYeoXbgIaNucF6gZmCe1NDaecF9fJXWOfKjm0JQMhnY4HwMUUMieyLqhx6J7vPRk1d0TSOQDmy98hQtw270mvV8/LL9l9VRLcNPyEoML+Dn3mn3CbtHn4HVJYKf/6+6F/f/drv6FOfMM+6B9huLeNVsY89QZ//L/UhL+p5A3bsAdlX3EEsxi9sXyXF53j4bs3vFPY+hMT9FwBEy0P+IDq/c4SYyTEfo8A8Y5fj3pj168AAAAABJRU5ErkJggg==");
    }

    .TWbWindowFullScreenGlyph {
      cursor: default;
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAdCAMAAACZrWzKAAAABGdBTUEAALGPC/xhBQAAAwBQTFRFAgICEBAQGhoaJycnKioqKysrMTExOzs7PDw8R0dHSUlJUVFRUlJSW1tbXFxcZmZmbm5ub29veHh4fHx8gICAhISEh4eHioqKjIyMjY2Njo6OkJCQkZGRkpKSk5OTlJSUlZWVlpaWl5eXmJiYmpqam5ubnJycn5+foKCgoaGhoqKio6OjpKSkpaWlpqamqKioqampq6urrq6ur6+vsLCwsbGxsrKys7OztLS0tbW1tra2t7e3uLi4ubm5urq6u7u7vLy8vb29vr6+v7+/wMDAwcHBwsLCw8PDxMTExcXFxsbGx8fHyMjIycnJysrKy8vLzMzMzc3Nzs7Oz8/P0NDQ0dHR0tLS09PT1NTU1dXV19fX2NjY2dnZ2tra29vb3Nzc3d3d3t7e39/f4ODg4eHh4uLi4+Pj5OTk5eXl5ubm6urq7+/v9PT0AAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAOH3cugAAAAlwSFlzAAAOwgAADsIBFShKgAAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMS4xYyqcSwAAAXdJREFUKFNt0FlXgkAUB/DbXrRHadG+Z4m0GFiu5DjB0ERRZBQVadr6/Z+bEe30wO/h3nv+lzMDwH0kwJHguCWdDjuXZkDTVFXjwtaeIXPCZDg+dGY4ZbTDfUXZ7zhkO4hf5nLpVGIvKSd2ErIsJxOpbDYL8atC4agP/unN5/Mwd10qaV2z5x14tqtYLIJ0c3aW7VbbIaN267oO87cIFXoUhA5EQRBEVFZ6yuUyLNxhXOrfxDjHUkHEaLe/UqnAUtUw9IElw2CxqmYMvDXIToJljxA0tEZI+DQ5XxsyTRNWPMuqjKxYVqkVW8biMCEEVh8oNcbmKUX8QkSJNEophfVH2zbH46yw2DRtKz5u2zZsPDsOnYyxIgoipc5FbNJxHNgKXPd6arr9+zHWp6dc14XtuudVZybYJ7RNzHieB7tN339S+Ft0KL7vg/wZvNbfuLC+1YMgeAE2NBrN9z/NBt/BRyT4igTfkeAnEtRbarWwczUGpAiS9AtDc6IpAX76RwAAAABJRU5ErkJggg==");
     }

    .TWbWindowFullScreenGlyph_focused {
      cursor: default;
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABcAAAAdCAMAAAB2bwf0AAAABGdBTUEAALGPC/xhBQAAAwBQTFRFAgICCw0PFBcaFR4qHSEmJisxLjQ8Nj5GNj5HPUZSSUlJRVBdTVloVWJzXmx9c3NzfHx8QGKNQmSQQ2aRRmeTR2mURmydR22eSWuWS2yXSG2eT3GaUXOcS3WoTHWoTnetTniuT3ivTHq0TXu1Tnu2T3y3UG+qV3agWnujXn6lUHuyUHyzUny2U323VH+0UX64Un+5Z3eJUoC5U4G7VIC6VIG7VIG8VYK8VoO9W4S4WIS+WIW/XIW5Y4CoZoOqa4isYYi7Yom8ZY2/coyvf5SwWojBW4nCXojCXojDXYrEX43GYY3GYI7HZY7AZo/BYo/IZJHKZJHLZZLMaJDDaZHEbJTHaJXOaZbPbJTIb5jLbJjRbZnScJnMcp3Vc57WdJ3Qd6PaeaDSf6XWfabbfqfciYmJi4uLjo6Oj4+PkpKSk5OTlJSUl5eXmZmZm5ubnJycn5+foqKip6enqKioq6urr6+vsrKytLS0uLi4vLy8gKLNgqPPhKbRhabSh6jThKrbhKzfiKrVi6zXjrDZkbPclrbfhKzgiKrliK/ii7DgibDjj7TljLjvjbrwkLXmk7fml7vpl7vqmbrjnb3lmbzpmbzqnb7qnr/rnr/so8HpocHrocHso8LspcPqpMLrpcPspcPtpsTspsXtp8Xup8buq8frqMbsqcbtrcvxrszxsczutdHzvtf1wMDAxcXFycnJzc3NAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAMONKfwAAAAlwSFlzAAAOwgAADsIBFShKgAAAABh0RVh0U29mdHdhcmUAcGFpbnQubmV0IDQuMS4xYyqcSwAAAeZJREFUKFNt0PlTEmEYwPHHs1yvNSuKDhhlsSwkYxpIu5MJdzA1D5QSV8Uru6yF3e3SLlIUQSsgLbWk1q2A/sSedxGnH/jsvN933mfeH3YX3ucGjhvIgTJRT7igtgbVZpFDDSmYGIYxGEwGxJgMDHlwY4AxE2fUqDWbyQhsFovl7Cm9SqfX6XT6+nMIbFVPraePa7RajVaj0R5Ex0xWqxVs+541NRTAfwoampqbobX6uf18Hj3t3zFN512w2+3Quv+F40q+//GuR/lX8UOAPfCSvVb4hOd5mqIomucfFraxLAvth14524oe+Hwijina57tfdNPpdMKtw687uorvCgLO/X5BEO4Vd3d0doLryJvenj1Topi5L4pTe/t6XS64ffStu79kUpIkdS5JkyV33G43eOreDQyWTiSTSZxikxOlgwMeD3AnVrihsvFUKoVzbGq8bIjjOPCe/DAyXD6WTqdpisamx8qHR7xeCFz8PDtTMfp312jFzGwgAMFLX+fmGyt3/gKqbJyfCwZh6fKPhXALeZeslvBCeAmWr/+KfIptyvJPWU7I6FvsYyQSgXgsGo2tbmxuIMz6+pe1eDQeh4RqK7Oh7yRbCVC2FUVdynamJIoCv3ODP7lBaBGFsshhERsCYy5G4z8Z1PV1f1m7ngAAAABJRU5ErkJggg==");
     }

    .TWbWindowMinimizeGlyph {
      cursor: default;
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAdCAYAAACuc5z4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAC90lEQVRIS7WVOUhrQRSGD6lsBMFGBCsLQavU2ouk0lZrS20tLQQhmGA0LqjgLtpoNC5xjVvivivu+1LY2Nj/j//AXG70xveEl+Jj7j3zn3/mzJybyOnpKVKBrK6uIhXI+Pg4nAiFQorTnBNGb5BwOAwyMTGhmHc7yeZ+0svk5CSmpqYSYOwrXzV2nPQyPT0NO1xxdHQUIyMjGB4eVvj8N5hjX0y8Xi+2t7cRiUQUns/Q0BB6e3vR39+PgYEBDA4O6ntPT4+OfDdwnjHmzMzMWKjx7u4u5ubmlLGxMRQUFMDlcv2K/Px8zM7OWojP58Pe3h4WFhYUHkVGRgYaGhqwsrKirbO2tqbitLQ0Hflu4Dy1zJmfn7cQv9+Pg4MDLC0tKSwjOztbb9b0JA1YTXp6uo52Y0ItcxYXFy2ksbERh4eHWF5eVpiYl5enl8aFotGoxnn+mZmZOhot4Ty1zOGzQQKBAI6Pj63d8Tjcbjc6OzsT+pQfQVZWlpZptITmfX19msOjM0hzczNOTk6wvr6usIzCwkIwzhbiZRKa5+TkaJLREpp3dXWhqKgo4XgkGAzi7OwM8Xhc4Q6Ki4uRm5v7DY/Hg83NTUtLaNLW1qY5sVjMQlpaWtR4Y2ND4Y7KysrAOE3Yiuya/f19HdnzRku466amJpSWliYsKK2trTg/P8fW1pZCYUVFBXipNKGZgYswZrSEJuys8vLyhLi0t7fj4uICOzs7CsuorKwEPxzuyMSTQQ21zLHHpaOjA9fX11oq4WrV1dWor6/XozDxZFBDLXPsceGN3t7e4ujoSGHJNTU1qK2t1Q6xt5YT1FDLHONBpLu7Gw8PD3qBhK1XV1eHqqqqhIb/CWqZYzyIsLmfn59xeXmpXF1d6ZdUUlLyK5hjPIjwp+/t7U2P4+7uTnf/+Pj4jWRxwjnmG25ubiB2wdPTk8IKXl5e/glqmWP3IfL6+opUIO/v70gF8vHxgVQgn5+fSAXCG3Xi/v5ecZpzwugN+tf0//HjD2dq1sgXqefYAAAAAElFTkSuQmCC");
     }

    .TWbWindowMinimizeGlyph_focused {
      padding: 0px !important;
      margin: 0px !important;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABYAAAAdCAYAAACuc5z4AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAADzklEQVRIS7XTX0ybZRQG8DdckRjiBRqJCSaGZBeamJgYr5wxGilkf2Ad07kJE2QMspLpqjKl/NlWYmuhpazS1VDYWmmxsBVZgQIdUKAwBBzFrIt2yRaXLhmUoTFeP37nlcrX9kO6RC9+eU8P53nSNIFpXBH8H9gxzTRKNFNxaCe23S6ReM/21Q5i7xeeOLTbSWImlovNrEA1DLH9tUOceBbvYnupVzyzA/VeSJE3jEjupPZSWNFZHw6eHfuHvHEEhaprKDjjxv4zV7dXc2WL8LmwdgBFjaMoEjoIe+f8dbxR1oJG2xIOnfNBrhrC3k97kFfdCdm/UViRu0lWbcUeIXOwbph3EHa4aQJvlrfinP0mDqvHUdTgwdPPvYC0tLTHQhnKUgd1sqPaKbxVYYTaEcQRjR9H1V48kfEkurq6EAwG46Snp8NmsyVRqVQ8837TCO8grPirGeSeuIAmxwqKtdMo1YzhqWee5UXhcDhORkYGnE5nEvoSlCnT+ngHYR/o5yCrMuHLnp9wrGUWx1smkP38Ll4ciUTiZGZmore3N0lHRwfPVOgneQdhZYYbyD/ZLvwb3kIpFQt/3PXiy1heXsbGxkacrKwsuN3uJBaLhWcqDH7eUSp8WXa8bRF7FGbo+m6j3PgDKlun8dIrr8Hv9yMUCsXJzs6Gx+NJYjabeabKOMM7yo0LYFXtN1Hw8TfQ9/+Myq9/hMI0h1dfz0VOTk6SvLw8eL3eJCaTScjIhOwN3kGdrNqyggPKDhiv3YHiYhCnLi5gt0yOtrY2+Hy+lNDt7lw5PrIs8g7ClJ0hvPvZJbQP3cVp6y18Yl3G24XFMBgMmJycTAndUoay1EGdrMb+C47U2mEZvQ+aP7+0gn3vVUKv1yMQCKSEbilDWeogrN55ByX13bBej6DOEUZDdwiHypTQ6XSYn59PCd1ShrLUQZ1M3XcPHzb2wO5/CHXfXahdYZQo6qDVarG0tJQSuqUMZamPMG3/fVScd6F7ehUa96/Q9d/DiRotlEolBgYGUkK3lKEsdVAnax18gJOaPvTOrcHgieDC8AM0mPqRn5//WChDWeqgTmYeW8XpZje+X3wEmi2+VXRNrMExE8V3s+tcz+zWLIVuKWPxPRQ6/sY6hQWxjq9yNF/2r8E+FRV+nnUBvWJbu2+n1jibcH/ZHxWy0c2OKJhz9hGSBCR223AE1iX37MrC7yBXF7ckfo7tttsn7ggbDP6BIQG9iWJ7esUS78Q3sZlN3P4TUsYT3sR5J6xl9Dc0j2yg2btJmGm3k7iMKBebWeEpF/57LvwFjMgRPe7EUckAAAAASUVORK5CYII=");
    }

    .TWbWindowSizerGlyph {
      cursor: default;
      border-right: 1px solid #204D8D;
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAB/UlEQVQ4T52TO4+pURSGT+tnUKqplUyhEQ0JcamIikQxoaCQIBIqidCJSCSDRBQu4864j9vveSfvSr4vZsyZMznFm/XZe+1nXf05Ho/4rXa7HVarFebzuVj+Vu5+BTocDthsNpjNZnh9fUWv1xO7XC7ljj7/BO33e7y9vWE6neLl5QU+nw8ajQZ+vx+dTkfu6fcjiE4sYTweI5/PQ6/XI5fLCVir1aJWq6nl/RWkQJhJJBKByWTC6XTC5XLBdruF0WhEpVL5GaRA2FSW4na7BXI+n8WyX09PTygUCvLNNw+g+0wIyWazAqDe39/Fh4+dTidSqdT3IE6A9U8mE3i9XqTTaTULBUKxtFAohFgshvV6/QhSRsyeuFwuAXyFUOxLPB5HOByWwDxTQbwkhNNhY/n4OwjFzDm9QCAgbeCZCuJBs9mUEdPxvidfxftyuQyPxyNLyTMVtFgs5CKZTMo3RSeK6bNs9oaZE9RoNOBwOB5BdOTGcvUHgwH6/b7Y4XAoZ4pGo5EMo9VqwWazScBPIIqT0Ol0MBgMsFgsEjEYDOL5+RmZTAbFYhHVahXtdluCWK3Wx6mxsVzAbreLer2OUqkkDU0kEohGoxKEe8X9sdvtAjGbzWoSKkiZEv8C97per9/qdruJ5VA+gRTYvZQ9opTtVsQgtPR7AP2/jvgA82AAzPf38YEAAAAASUVORK5CYII=");
    }

    .TWbWindowSizerGlyph_focused {
      background-image: url("data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABIAAAASCAYAAABWzo5XAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAACSklEQVQ4T53S3U9ScRgH8G75O7xLbqwrlmtUJkgSKQRaHBB5U4xpSU3BRYuW2gEh7WVW9LLGVTdttTIuuglSV3lAUTY2LhhsbFywccfdt/McZokQsi6enZ39nvM5v+flmGlpC+2G0f8DzOJ36O9/E570vn/WFjQS+AXDgw0M+75CM/cRg7PvofWuQT8fF84o50hoxP8TzEIcOu8XXHCG0dmtgUgkQtc5Btq5D8I55bWEKEk/H4P29iecHvZCLBYjGAwilUqho6MDatdbPqdW3j8hAeF7QciJXgukUikSiYSApNNpSCQSqKdWW0MHkeOnNDAajXVIJpOBSqXCpfEATP7N5tDBcjq7L4Nl2QYkm83CarVCab7bHKIJ1Br7uSWSy+Xg8XigYGYw6t9ohJjFdQzdieKk3Na0nH2kUCggFApBPjTJQ+v1EC3XFX5PzjC+hsYeRorFIiKRCGQaeyNEmzrgiggjPgoplUqIRqM4rzLAzMbrIcNCDF1n9X/2pBVSLpfBcRx6+nWNkDmwKWxsO0ilUkE+n0dP3wAsbKweGlvmIFU7hY2lZaM9oRHTdKix1BMqh25CSLVaRW+fEralQ1OzLyfgCMYwwa7B7nsHkzuMq9dD0DnuQW12Q6WfRL/WCsUgA7lSA5niImQyGRwrte//lhbiMLaShPPpNqZWd3Dj2Q6mn6fgepHCzfAubr3cxQwfs6/24H69B8+btHA2/ihZD1GMBjmY+bDwqPUhBxt/Swq6Lf2EPnI8TmKCj2tPtoV3ymuA/j+28BtrXYT80IXhrQAAAABJRU5ErkJggg==");
    }

  ';
  Sheet.Append(StyleCode);
end;


end.
