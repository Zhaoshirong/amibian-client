unit desktop.scrollbar;

{$I 'Smart.inc'}

{.$DEFINE DEBUG}
{.$DEFINE NO_CAPTURE}

interface

const
  CNT_SCROLLBAR_SIZE = 20;

uses
  W3C.DOM,
  System.Types,
  System.Types.Graphics,
  System.Colors,
  System.Time,

  //desktop.types,
  desktop.control,

  SmartCL.System,
  SmartCL.Time,
  SmartCL.Touch,
  SmartCL.Components,
  SmartCL.Theme,
  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet;

type

  TWbScrollbarLowerButton  = class(TWbCustomControl)
  public
    function CreationFlags: TW3CreationFlags; override;
  end;

  TWbScrollbarHigherButton = class(TWbCustomControl)
  public
    function CreationFlags: TW3CreationFlags; override;
  end;

  TWbScrollbarTopButton = class(TWbScrollbarLowerButton)
  end;

  TWbScrollbarBottomButton = class(TWbScrollbarHigherButton)
  end;

  TWbScrollbarLeftButton = class(TWbScrollbarLowerButton)
  end;

  TWbScrollbarRightButton = class(TWbScrollbarHigherButton)
  end;

  TWbScrollbarHandle = class(TWbCustomControl)
  public
    function CreationFlags: TW3CreationFlags; override;
  end;

  TWbCustomScrollBar = class(TWbCustomControl)
  private
    FUpBtn:     TWbScrollbarLowerButton;
    FDownBtn:   TWbScrollbarHigherButton;
    FHandle:    TWbScrollbarHandle;
    FTotal:     integer;
    FPageSize:  integer;
    FPosition:  integer;
    FBusy:      boolean;
  protected
    // Shared variables with decendants. No point exposing these
    // as getter/setter properties; thats just bloat
    FDragSize:  integer = 0;
    FDragPos:   integer = 0;
    FEntry:     integer = 0;

    // Scroll-state handling
    function    SetBusy(const NewValue: boolean): boolean;
    function    GetBusy: boolean;

    // Overrides default behavior of enabled/disabled and
    // applies the changes to child elements as well
    procedure   SetEnabled(const EnabledValue: boolean); override;

    // Event handlers for up/down + left/right buttons
    procedure   HandleMinClick(Sender: TObject); virtual;
    procedure   HandleMaxClick(Sender: TObject); virtual;

    // Event handler for mouse-wheel
    procedure   MouseWheelHandler(Sender: TObject; Shift: TShiftState;
                WheelDelta: integer; const MousePos: TPoint;
                var Handled: boolean); virtual;

    // Child elements must override these if they wish to
    // create up/down + left/right child elements of a particular type
    function    CreateLowerButton: TWbScrollbarLowerButton; virtual; abstract;
    function    CreateHigherButton: TWbScrollbarHigherButton; virtual; abstract;

    // Position handling code
    procedure   SetTotal(NewTotal: integer); virtual;
    procedure   SetPageSize(NewPageSize: integer); virtual;
    procedure   SetPosition(NewPosition: integer); virtual;
    procedure   SetPositionNoCalc(NewPosition: integer); virtual;

    // Calculates pixel-size of grab-handle based on pagesize
    function    CalcSizeOfHandle: integer;

    // Converts between offset and
    function    PositionToPixelOffset(const aPosition: integer): integer;
    function    PixelOffsetToPosition(const pxOffset: integer): integer;

    procedure   Recalculate; virtual;
    function    GetArea: integer; virtual; abstract;

    procedure   InitializeObject; override;
    procedure   FinalizeObject; override;
    procedure   ObjectReady; override;
  public
    function    CreationFlags: TW3CreationFlags; override;

    // Move up or down by one page-size
    procedure   PageUp; virtual;
    procedure   PageDown; virtual;

    property    MinButton: TWbScrollbarLowerButton read FUpBtn;
    property    MaxButton: TWbScrollbarHigherButton read FDownBtn;
    property    DragHandle: TWbScrollbarHandle read FHandle;
  published
    property    Total: integer read FTotal write SetTotal;
    property    PageSize: integer read FPageSize write SetPageSize;
    property    Position: integer read FPosition write SetPosition;

    property    OnScrollBegins: TNotifyEvent;
    property    OnPositionChanged: TNotifyEvent;
    property    OnScrollEnds: TNotifyEvent;
  end;

  TWbVerticalScrollbar = class(TWbCustomScrollBar)
  private
    procedure HandleDragBarMouseDown(Sender: TObject; button: TMouseButton;
      shiftState: TShiftState; x, y: integer);
    procedure HandleDragBarMouseUp(Sender: TObject; button: TMouseButton;
      shiftState: TShiftState; x, y: integer);
    procedure HandleDragBarMouseMove(Sender: TObject;
      shiftState: TShiftState; x, y: integer);
  protected
    function    GetArea: integer; override;
    function    CreateLowerButton: TWbScrollbarLowerButton; override;
    function    CreateHigherButton: TWbScrollbarHigherButton; override;

    procedure   InitializeObject; override;
    procedure   Resize; override;
  public
    procedure   StyleAsEnabled;
    procedure   StyleAsDisabled;

  end;

  TWbHorizontalScrollbar = class(TWbCustomScrollBar)
  private
    procedure HandleDragBarMouseDown(Sender: TObject; button: TMouseButton;
      shiftState: TShiftState; x, y: integer);
    procedure HandleDragBarMouseUp(Sender: TObject; button: TMouseButton;
      shiftState: TShiftState; x, y: integer);
    procedure HandleDragBarMouseMove(Sender: TObject;
      shiftState: TShiftState; x, y: integer);
  protected
    function    GetArea: integer; override;
    function    CreateLowerButton: TWbScrollbarLowerButton; override;
    function    CreateHigherButton: TWbScrollbarHigherButton; override;

    procedure   InitializeObject; override;
    procedure   Resize; override;
  public
    procedure   StyleAsEnabled;
    procedure   StyleAsDisabled;
  end;


const
  CNT_BTNUP_STYLED ='data:image/png;base64,'+
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAB8ElEQVQ4T6WS3UtTcRzG/RO6ECovBJGgC0EwUBKtRYPNxYQ86kySaoW7CHFSCVqelcy3ksPQXjRGEvbiwcJm5GoVGCmBwSRwV7soRoMl4UlxKBE87fmB/hwcvKiLD3x5ng/P1Tdnf6ET/4MY8D37kYU6mcK1ie/ofPxNwJvZTsfaFpID3VPLqGqf2cZ2aRrHW3RYPGMC3sx2Onsqu+RA78ufqL76VuDsjMBxOYSyOh/2FR4S8GbGbsvLPXZDDgzMrEC5Pis42ZUR2qdQUFQBTdMEvJmx2/L2Wv1yYPCNgYaeebj8c1DUCI64OqAoCmKxmIC3yDIdHbp5tj45EHi/iqabC2jsnUeD7zWKyx0IBoOIx+MC3szY0aGbf+KWHBieXcM5LYqmvo+ocvvhdruRSCSyYMaODt2Cak0O3Jtbx4XAIs72f0CFzQVd15FKpbJgxo4O3QM1Q3Lg/qc0PEOLqG0JwOv1wjAMU9jRoXuw7o4cePB5A60jS3DUNyMcDiOdTpvCjg7dolOjcuBhdBMd419hsVigqiqSyaQp7OjQLT4dlAOPvvxGd+Y9r2gh2O32XaFDt+TMmByYiP2B/8UKBl8ZuB35hbvvVk1hR4du6flxOdAzvYZyz1McvajD2joJW9tzU9jRoXu4+Ykc+Hec+Au2O3GTZvoFaAAAAABJRU5ErkJggg==';

  CNT_BTNUP_UNSTYLED ='data:image/png;base64,'+
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAABb0lEQVQ4T6WTT64BYRDEaxj/DmMvgoXETVxH7GxYWoiFCwgXmERCLJA4go2wEczLr+mJmWf1XiVluqt7qtv3ETQajVj/gBkMh8N3+kIcx7rf73o8Hpbn83mFYaggCCwHu91OvV5PIQmFw+FgBcDLt9vNCIrFohETRxRF9szZRy5nUyDx8/k0w8FgYCRGy/alDHD3CWy0XC7V6XSMxL6+96UMKH5OX61WKpfLarfbRmK07BaJgYscHmbr9VqtVitpJEajRg/arw18Oo1MbDabpkFiNGq+RcqABGew3W5Vr9dVKBSSRmI0aoDelAEb4LzZbFQqlVSr1ZKXnWjU6PEtEgMSTna/36tarep6vep8PutyuRiJ0ajR8/UWKpWKjsejkQn8Cj+J5nV6/Rbs4nFD7Ha7Go/H6vf7VvwGeuj1Dey/MJvNdDqdTIR+oFn4WcH5fK7pdPoyGI1GWiwW9vLnHWfhX40nQyaTycvgXf8DpB/w1teQBRaXhgAAAABJRU5ErkJggg==';

  CNT_BTNDOWN_STYLED ='data:image/png;base64,'+
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAB9UlEQVQ4T6WST0iTcRzGgx136RBEHgKJoEOUdAiSBhWCtaYHSbAhapjmUjGNNtPlv/5KoaK8mUNc9YrOTORNMDw0Ogw6FC0Pr2QLhbcOm6hvSqOI4HHPD9xP4aVDHj7w5Xk+PKfvrn0H8rATxEDrywRaxuNoHjXQ+PwrvMHPlrCjQzenXpMDHZNLyG3QcObqMByXAzh56bEl7OjQ3e24JQfuTS3DeUND1rla2Gy2f0KH7p7T7XKgc3oVF1pmYLfb4XK5EI1GLWFHh+7enDty4NGMidIHERzJdkJRFOi6bgk7OnQzcu/LgZ43a6jo/oCC6i643W7EYjFL2NGhu//8QznQ93Ydlb2zqOv/BMfZIqiqCsMwtsGMHR26mfldcqA/8hNVio66wBxKvE/g8XgQj8e3wYwdHboHC3rlQOBdEjUD87g2+AWNT+fhLKyApmkwTVPAmxk7OnQPFSpyYOj9L9QPLaAhuIib6iKq2lT4fD4kk0kBb2bs6NA9fHFADjz7+Bte9ZugaeQ72scMFJVfRzgcFvBmxm7TO1o8KAeGZ/+gOZR65RT+sQQ6JhLwByIoq/YLeIss1W16x0qDciCk/0XbxEqa25Or6Jwy0fP6h4A3s63O8XJVDtx9tY4TlSNpsq+MwuEJ4VTNCwFvZlsdkh74f/KwAYB+cA2hUht2AAAAAElFTkSuQmCC';

  CNT_BTNDOWN_UNSTYLED ='data:image/png;base64,'+
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsIAAA7CARUoSoAAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAABdUlEQVQ4T6WTTYrCQBCFKxp/wHN4BkVEd57ElTfwBK7cZqO49hDeQIRsFAXBU7jwN873xmqSkdnMFJRd/d7rV5W0ibrdbmb/CBnMZjPLssxut5tdr1e73+9vuhhxHFu1WrVKpWKHw8Gm06nFEFEU2W63s/P5LAP2pVJJhzyez6eaYNBoNGyz2QiXAWLI9XptSZKI+C1Go5H1+/3QIBjUajVbLBYix+OxOuWDySaTiTSDwSAY6JeR6/W69Xo9GTDNzwCDQ4O2XC4Ll4FPAMmz0e1yuei5SWowODRoPyYA+LoRjZ6mqQ6CkdRgcGgcDwYOcD2dTse2261IxvbHAYND82HgE5CI6MSBx+OhpAaDc13BgA0vhZUO7Xbb9vt9MKAG8+6uDQZM4CBrq9XSmz4ej0pqsLyGDAaA/E1JCMblrk+nk5IaDM51hQnyrtRks9m04XCopHY8ryP0Mc3nc1utVgIIf6msBDfh30I+lsvlt8F7/4cwewFhq7KCU36EywAAAABJRU5ErkJggg==';

  CNT_BTNLEFT_STYLED ='data:image/png;base64,' +
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAACRklEQVQ4T6XS60uTURzA8f6EXgRRL4KMKOgGJdJN57UtLyMdGlZQrkya5P0+nblpZq2HUCyqWYbz8jinzpjDLEoqQgKhYG+SbsvV1NpKGkUE39bzogcxKerFFw7nwAfOOb8lK9ak8D9JQG3f9KIZbD6qu99Q2fGCio7n6Ls90l58oUMGjAMz7C0bnpeq1Imy2EFCfh8xOiuK7Daijl0lNrRWFjlYGlkjA6dvvketH5VKqbpFckUIKRlkT14PcSfaiUwvZ7dyP9HR0SiPmEgscbAspk4Gmob9pNXeJbXmNupKJ4nFdpQnrezSlBIepaagoACXy4XBYEClUpGmd7I8vl4GzCMBMoxjpOpdJBf3sn1fERsiEtBqtYiiSCAQIBgM4vV6JeCgaZSVykYZuHDnEwca7hObZSZsUyQajQaLxYLH48Hn8y0AtE1jrEo6JwMt9+bQmscJ26xAEATcbjeTk5OLAjnCQ1arBRm49OAzutanrAtP+Csgt3mctWnNMnDlUZBCyzOyanvZFnq0P10h/+IE69NbZeDa4y+UtL9E3/maBpsHbZUFRWLmoo9Y2uZmY+ZlGbgx8ZVy6xSVnVMYRC/1fW85O/gOndFKUnr2gm+s6XrFlkMWGbA++Ya+xydVLYZG2DaNyT7DGccsgnOWoiYbGVmF0iCVCUMY7dNsPXxdBnrc3zll/zCvun4/pgE/jUN+zg8HaB75SEsoszNA/aCfiKMdMtAwNMeO412/bWdON1E6kbg8G/GhFLmitPfz7Bfw76XwA/esddtL6QqSAAAAAElFTkSuQmCC';

  CNT_BTNLEFT_UNSTYLED ='data:image/png;base64,'+
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAACAElEQVQ4T6WTW4vpURjG9weTzJ3kSrgyF26UryBESEYkkZJpSjNRDmOcz6ZhbmTIaRyGiEi+xDO9b/HfU3vf7H3xtFbrXc9vPeu/3v8vkUiE/xEDZrPZX/X5+YnhcIher8eiOa09Pz8LgPl8jmw2+0MvLy9IpVKIx+N4fHzEw8MDIpEIotEorxsMBgGwXC5RKBRY+XwemUwGiUQCsVgMT09PsFgs0Gg0kEgksNlsXDMajQJgtVqhXC4z4GKmk00mE1QqFW5vb+Hz+aDT6SCVSpFOp2E2mwXAer1mAMVOJpMcTy6XQ6lUwuPx4O3tDe12G9VqlQG5XA5Wq1UAbDYbBrhcLtzc3EAmk+Hu7g6dTodFZoJUKhUGlEolvsoVsN1u0Wg0IBaLOVqz2WTT+/v7D8AlAY0Oh0MA7HY73kinUzRKQ6rVanh9feUawer1OgNodDqdAmC/36Pb7eL+/p430BXsdjtHpdiUjkCXBK1Wi697BRwOB3x8fKDf72MwGPCHUygUUKvV/C0IUiwW+ZUIQIncbrcAOB6PbCRRp41GI1YgEOAn1Gq1/Ix6vZ4BdJjX6xUAp9PpahqPx5hMJphOp9yyJOpAMlMjUVdSnYBXwPl8ZsPvupipzReLBXfr19cXz+kf8fv9AoA2Udw/KRgMIhQKIRwOs2hOa1S7Av5dInwDieyqriTgiL4AAAAASUVORK5CYII=';

  CNT_BTNRIGHT_STYLED ='data:image/png;base64,'+
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAACTUlEQVQ4T6XSW0jTURzA8cBHX3oIoh6CkKCHyAgKklTMy5pt1gYqreyyLjI152U2bepSp2bZCGVFXsrY1C1d+je2MMuSipBAKdhL0m1ZbVpbSaOI4NvaQ/+kpKiHLxzOgQ+cc36LlsXI+Z8igLHfT3WfD0Ovl3LrEyqsT6nsfRnZ+372u1KKBRGoHZhBUiKwJc9GwsF2Eg91khRep2r7kZQKbC1zIT3qntfi+CoRaLj6lnSdwDppAdHR0cSsjWeDXEuypou0QjtS3SCycjfyY9fJMIxEWpJUIwJN7gBKg4uoqChkMhkWiwWVSkVsnJT4TD2SIzbSS51kVLhQVN1AabzF0hSTCDQPB9lVNxIBJicn8Xg8TE1NYbVa0Wg0xKVmkppjRFZ6GYXhGlm1YyyXNIrAmZsfUDeN/QJ4vV58Ph+CIKDX60lT7EdZ1I6q/g4rtp0Sgdbbc+Sa7y0IBINBQqEQo6Oj7MgpRN08zsoMswicu/uR/JbxvwKy1DryLI9YpWwRgbb7IbRnJ/54hWx1CUXmYYo7HrM60yICFx58oqzTs+Ajbldp0DbYqe/zYuh+ga7rGWt2nheBSxOfqep5HgF+/sY0xT4KTD2cHHyDqf811Y5XVHRPo7dNE7u7QwRsD79Q6/SzR98WGaTNkmw0NVbMrllOCLPUOWcw9vmpdPgx2MMjH2793osiYPd8xTQYoNkVpHX4PS3hTruDNA4FqBsIUHMlwHHnu3ltPGAVgfqhOeJye0nMd5BS2EdyuIQ8R2Rv0+GeBfsB/HtyvgHFz2u31KJnKAAAAABJRU5ErkJggg==';

  CNT_BTNRIGHT_UNSTYLED ='data:image/png;base64,'+
  'iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAACDUlEQVQ4T6WSS0uqcRDGD7j0K7lSaCfUTsWV5i5wKyKhSJiKWZZEIgiiKJSXvGbZBc1NeMFrXlEURfwST8xA79uBczbnLB7+w8w7v7m880smk+F/xIDPz08MBgO0223U63UW2eSj2J+USqVEwHA4RCKRQDgcRjAYRCgUQiQSQTweZ//9/T0ymcxvMpvNImA8HuP29hZHR0eQSqVQKBQwmUyIRqOIxWIcS6fTyOVyyOfzLIvFIgKm0ymSySQkEgnkcjnsdjuUSiUODg5gtVq5k28IJT88POD4+FgEzGYzZLNZBjw+PqJaraJSqcDlckGlUkGr1cLhcODu7o7HIYDNZhMB8/kcxWKRAaVSiZMJ8v7+zrq4uIBer4fBYMDNzQ0DqEsBsFgsuPJ3Bz8BtVqN7ZeXF3i9Xt7T8/MzTk5ORMByuUS5XGYAvZREyW9vb3h6euKKpMvLSxiNRo7RSAJgtVrh9fVV6IASqQqNQ6O53W4cHh5yddrDx8cHnE6nCFiv10wlAG25UChw8unpKTQaDXQ6HQKBAFqtFprNJhqNBkMFwGazYScB9vb2eEH0C9VqNa6vr9HpdFh0nQQhnZ2diYDtdoterwePx8OHtL+/j6urKz5lUr/f53i32xVg5+fnImC32/F9j0YjTCYTvkyy6cR/Qn7K5/OJAPrQ7/dzuzQriWzyUSd/kwD4d8nwBUVApEtAAhmPAAAAAElFTkSuQmCC';


implementation

//###########################################################################
// TWbScrollbarLowerBtn
//###########################################################################

function TWbScrollbarLowerButton.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);   // Dont adjust
  Exclude(result, cfAllowSelection);      // No selection [text, glyphs]
  Exclude(result, cfReportChildAddition); // No add child events
  Exclude(result, cfReportChildRemoval);  // No remove child events
  Exclude(result, cfReportMovement);      // No movement events either
  Include(result, cfReportResize);        // yes resize management
end;

//###########################################################################
// TWbScrollbarHigherBtn
//###########################################################################

function TWbScrollbarHigherButton.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);   // No boxing-model adjustment
  Exclude(result, cfAllowSelection);      // No selection [text, glyphs]
  Exclude(result, cfReportChildAddition); // No add child events
  Exclude(result, cfReportChildRemoval);  // No remove child events
  Exclude(result, cfReportMovement);      // No movement events either
  Include(result, cfReportResize);        // yes resize management
end;

//###########################################################################
// TWbScrollbarHandle
//###########################################################################

function TWbScrollbarHandle.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);   // No boxing-model adjustment
  Exclude(result, cfAllowSelection);      // No selection [text, glyphs]
  Exclude(result, cfReportChildAddition); // No add child events
  Exclude(result, cfReportChildRemoval);  // No remove child events
  Exclude(result, cfReportMovement);      // No movement events either
  Include(result, cfReportResize);        // yes resize management
end;

//###########################################################################
// TWbCustomScrollBar
//###########################################################################

procedure TWbCustomScrollBar.InitializeObject;
begin
  inherited;
  FUpBtn := CreateLowerButton();
  FDownBtn := CreateHigherButton();
  FHandle := TWbScrollBarHandle.Create(Self);
  FHandle.TransparentEvents := True;
end;

procedure TWbCustomScrollBar.FinalizeObject;
begin
  FUpBtn.Free;
  FDownBtn.Free;
  FHandle.Free;
  inherited;
end;

procedure TWbCustomScrollBar.SetEnabled(const EnabledValue: boolean);
begin
  FUpBtn.Enabled := EnabledValue;
  FDownBtn.Enabled := EnabledValue;
  FHandle.Enabled := EnabledValue;
  inherited SetEnabled(EnabledValue);
end;

function TWbCustomScrollBar.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);   // No boxing-model adjustment
  Exclude(result, cfAllowSelection);      // No selection [text, glyphs]
  Exclude(result, cfReportChildAddition); // No add child events
  Exclude(result, cfReportChildRemoval);  // No remove child events
  Exclude(result, cfReportMovement);      // No movement events either
  Include(result, cfReportResize);        // yes resize management
  Exclude(Result, cfKeyCapture);          // no listen to keyboard presses
end;

procedure TWbCustomScrollBar.ObjectReady;
begin
  inherited;
  FUpBtn.OnClick := @HandleMinClick;
  FDownBtn.OnClick := @HandleMaxClick;
  OnMouseWheel := @MouseWheelHandler;
end;

procedure TWbCustomScrollBar.MouseWheelHandler(Sender: TObject;
    Shift: TShiftState; WheelDelta: integer; const MousePos: TPoint;
    var Handled: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (Total > 0) then
    begin
      var e := BrowserAPI.Event;
      if (e) then
      begin
        var LDelta: float := max(-1, min(1, wheeldelta or e.detail) );
        var LNewPos := Round(Position - LDelta * 50);

        case GetBusy() of
        true:
          begin
            SetPosition(LNewPos);
          end;
        false:
          begin
            var LOldBusyState := SetBusy(true);

            if not LOldBusyState then
            begin
              if assigned(OnScrollBegins) then
                OnScrollBegins(self);
            end;

            SetPosition(LNewPos);

            if not LOldBusyState then
            begin
              if assigned(OnScrollEnds) then
                OnScrollEnds(self);
            end;
            SetBusy(LOldBusyState);
          end;
        end;

      end;

      Handled := true;
    end;
  end;
end;

function TWbCustomScrollBar.SetBusy(const NewValue: boolean): boolean;
begin
  result := FBusy;
  FBusy := NewValue;
end;

function TWbCustomScrollBar.GetBusy: boolean;
begin
  result := FBusy;
end;

procedure TWbCustomScrollBar.PageUp;
begin
  var LCache := SetBusy(true);
  if not LCache then
  begin
    if assigned(OnScrollBegins) then
      OnScrollBegins(self);
  end;

  SetPosition(FPosition - FPageSize);

  if not LCache then
  begin
    if assigned(OnScrollEnds) then
      OnScrollEnds(self);
  end;
  SetBusy(LCache);
end;

procedure TWbCustomScrollBar.PageDown;
begin
  var LCache := SetBusy(true);
  if not LCache then
  begin
    if assigned(OnScrollBegins) then
      OnScrollBegins(self);
  end;

  SetPosition(FPosition + FPageSize);

  if not LCache then
  begin
    if assigned(OnScrollEnds) then
      OnScrollEnds(self);
  end;
  SetBusy(LCache);
end;

procedure TWbCustomScrollBar.HandleMinClick(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FTotal > 0 then
      PageUp();
  end;
end;

procedure TWbCustomScrollBar.HandleMaxClick(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    if FTotal > 0 then
      PageDown();
  end;
end;

procedure TWbCustomScrollBar.SetTotal(NewTotal: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    NewTotal := Tinteger.EnsureRange(NewTotal, 0, MAX_INT);
    if NewTotal <> FTotal then
    begin
      FTotal := NewTotal;

      if FPageSize > FTotal then
        FPageSize := FTotal;

      if FPosition > FTotal - FPageSize then
      begin
        if (FTotal - FPageSize) < 1 then
          FPosition := 0
        else
          FPosition := FTotal - FPageSize;
      end;

      If (Handle)
      and (csReady in ComponentState) then
      begin
        Recalculate();
        Invalidate();

        if not (csUpdating in ComponentState) then
        begin
          if assigned(OnPositionChanged) then
            OnPositionChanged(Self);
        end;
      end;
    end;
  end;
end;

procedure TWbCustomScrollBar.SetPageSize(NewPageSize: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    NewPageSize := Tinteger.EnsureRange(NewPageSize, 0, FTotal);
    if NewPageSize <> FPageSize then
    begin
      FPageSize := NewPageSize;

      if FTotal > 0 then
      begin
        Recalculate();
        Invalidate();

        if not (csUpdating in ComponentState) then
        begin
          if assigned(OnPositionChanged) then
            OnPositionChanged(Self);
        end;

      end;
    end;
  end;
end;

procedure TWbCustomScrollBar.SetPosition(NewPosition: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    NewPosition := TInteger.EnsureRange(NewPosition, 0, FTotal - FPageSize );
    if NewPosition <> FPosition then
    begin
      FPosition := NewPosition;

      if FTotal > 0 then
      begin
        Recalculate();
        Invalidate();

        if not (csUpdating in ComponentState) then
        begin
          if assigned(OnPositionChanged) then
            OnPositionChanged(Self);
        end;
      end;
    end;
  end;
end;

procedure TWbCustomScrollBar.SetPositionNoCalc(NewPosition: integer);
begin
  NewPosition := TInteger.EnsureRange(NewPosition, 0, FTotal - FPageSize);
  if NewPosition <> FPosition then
  begin

    FPosition := NewPosition;

    //if not (csUpdating in ComponentState) then
    //begin
      if assigned(OnPositionChanged) then
        OnPositionChanged(Self);
    //end;
  end;
end;

function TWbCustomScrollBar.CalcSizeOfHandle: integer;
begin
  var LRatio := float( GetArea() / FTotal );
  result := round( LRatio * FPageSize);
end;

function TWbCustomScrollBar.PositionToPixelOffset(const aPosition: integer): integer;
begin
  result := round( aPosition * GetArea() / FTotal);
end;

function TWbCustomScrollBar.PixelOffsetToPosition(const pxOffset: integer): integer;
begin
  result := round( pxOffset * FTotal / GetArea() );
end;

procedure TWbCustomScrollbar.Recalculate;
begin
  FDragSize := CalcSizeOfHandle();
  FDragPos := PositionToPixelOffset(Position);
end;

//###########################################################################
// TWbHorizontalScrollbar
//###########################################################################

procedure TWbHorizontalScrollbar.InitializeObject;
begin
  inherited;
  SimulateMouseEvents := true;

  MinButton.Background.FromURL(CNT_BTNLEFT_STYLED);
  MaxButton.Background.FromURL(CNT_BTNRIGHT_STYLED);

  (* hook into our own events *)
  OnMouseDown := HandleDragBarMouseDown;
  OnMouseMove := HandleDragBarMouseMove;
  OnMouseUp := HandleDragBarMouseUp;

  self.Total := 200;
  self.PageSize := 90;
  self.Position := 0;
end;

procedure TWbHorizontalScrollbar.StyleAsEnabled;
begin
  StyleClass := 'TWbHorizontalScrollbar';
  DragHandle.StyleClass := 'TWbScrollbarHandle';
  MinButton.Background.FromURL(CNT_BTNLEFT_STYLED);
  MaxButton.Background.FromURL(CNT_BTNRIGHT_STYLED);
end;

procedure TWbHorizontalScrollbar.StyleAsDisabled;
begin
  StyleClass := 'TWbHorizontalScrollbar_unfocused';
  DragHandle.StyleClass := 'TWbScrollbarHandle_unfocused';
  MinButton.Background.FromURL(CNT_BTNLEFT_UNSTYLED);
  MaxButton.Background.FromURL(CNT_BTNRIGHT_UNSTYLED);
end;

function TWbHorizontalScrollbar.CreateLowerButton: TWbScrollbarLowerButton;
begin
  result := TWbScrollbarLeftButton.Create(self);
end;

function TWbHorizontalScrollbar.CreateHigherButton: TWbScrollbarHigherButton;
begin
  result := TWbScrollbarRightButton.Create(self);
end;

function TWbHorizontalScrollbar.GetArea: integer;
begin
  Result := (MaxButton.Left - 1) - (MinButton.BoundsRect.Right + 1);
end;

procedure TWbHorizontalScrollbar.HandleDragBarMouseDown(Sender: TObject;
  Button: TMouseButton; shiftState: TShiftState; x, y: integer);
begin
  if button=mbLeft then
  begin
    if DragHandle.BoundsRect.ContainsPos(x, y) then
    begin
      {$IFNDEF NO_CAPTURE}
      SetCapture;
      {$ENDIF}
      FEntry := x - DragHandle.Left;

      SetBusy(True);
      if assigned(OnScrollBegins) then
        OnScrollBegins(self);
    end else
    begin
      if  (x < DragHandle.Left)
      and not MinButton.BoundsRect.ContainsPos(x, y) then
      begin
        PageUp();
        exit;
      end;

      if (x>DragHandle.BoundsRect.Right)
      and not MaxButton.BoundsRect.ContainsPos(x, y) then
        PageDown();
    end;
  end;
end;

procedure TWbHorizontalScrollbar.HandleDragBarMouseMove(Sender: TObject;
  shiftState: TShiftState; x, y: integer);
begin
  if GetBusy() then
  begin
    (* take offset on draghandle into account *)
    var dx := x - FEntry;

    (* position draghandle *)
    DragHandle.Left := Tinteger.EnsureRange(dx,
      MinButton.BoundsRect.Right,
      MaxButton.Left - FDragSize);

    (* Update position based on draghandle *)
    var mNewPos := PixelOffsetToPosition(DragHandle.Left - (MinButton.BoundsRect.Right + 1));
    SetPositionNoCalc(mNewPos);
  end;
end;

procedure TWbHorizontalScrollbar.HandleDragBarMouseUp(Sender: TObject;
  button: TMouseButton; shiftState: TShiftState; x, y: integer);
begin
  if GetBusy() then
  begin
    SetBusy(False);
    {$IFNDEF NO_CAPTURE}
    ReleaseCapture;
    {$ENDIF}
    if assigned(OnScrollEnds) then
    OnScrollEnds(self);
  end;
end;

procedure TWbHorizontalScrollbar.Resize;
begin
  inherited;

  var ch:=ClientHeight;
  MinButton.SetBounds(0, 0, ch, ch);
  MaxButton.SetBounds( (ClientWidth-ch), 0, ch, ch);

  Recalculate;
  DragHandle.Visible := if (total < 1) or (FDragpos < 0) then false else true;

  if DragHandle.visible then
  begin
    var dx := ch +  FDragPos;
    DragHandle.SetBounds(dx, 1, FDragSize, ch-3);
  end;
end;

//###########################################################################
// TWbVerticalScrollbar
//###########################################################################

procedure TWbVerticalScrollbar.InitializeObject;
begin
  inherited;
  SimulateMouseEvents := True;

  MinButton.Background.FromURL(CNT_BTNUP_STYLED);
  MaxButton.Background.FromURL(CNT_BTNDOWN_STYLED);

  (* hook into our own events *)
  OnMouseDown := HandleDragBarMouseDown;
  OnMouseMove := HandleDragBarMouseMove;
  OnMouseUp := HandleDragBarMouseUp;

  self.Total := 200;
  self.PageSize := 90;
  self.Position := 0;
end;

procedure TWbVerticalScrollbar.StyleAsEnabled;
begin
  StyleClass := 'TWbVerticalScrollbar';
  DragHandle.StyleClass := 'TWbScrollbarHandleV';
  MinButton.Background.FromURL(CNT_BTNUP_STYLED);
  MaxButton.Background.FromURL(CNT_BTNDOWN_STYLED);
end;

procedure TWbVerticalScrollbar.StyleAsDisabled;
begin
  StyleClass := 'TWbVerticalScrollbar_unfocused';
  DragHandle.StyleClass := 'TWbScrollbarHandleV_unfocused';
  MinButton.Background.FromURL(CNT_BTNUP_UnSTYLED);
  MaxButton.Background.FromURL(CNT_BTNDOWN_UnSTYLED);
end;

function TWbVerticalScrollbar.CreateLowerButton: TWbScrollbarLowerButton;
begin
  result := TWbScrollbarTopButton.Create(self);
end;

function TWbVerticalScrollbar.CreateHigherButton: TWbScrollbarHigherButton;
begin
  result := TWbScrollbarBottomButton.Create(self);
end;

procedure TWbVerticalScrollbar.HandleDragBarMouseDown(Sender: TObject;
  Button: TMouseButton; shiftState: TShiftState; x, y: integer);
begin
  {$IFDEF DEBUG}
  writeln('Mouse-Down: '+IntToStr(x)+' '+IntToStr(y));
  {$ENDIF}
  if button = mbLeft then
  begin
    if DragHandle.BoundsRect.ContainsPos(x, y) then
    begin
      SetBusy(true);
      DragHandle.SetFocus();

      {$IFNDEF NO_CAPTURE}
      SetCapture();
      {$ENDIF}
      FEntry := y - DragHandle.Top;


      //BrowserAPI.Event.stopImmediatePropagation();

      if assigned(OnScrollBegins) then
        OnScrollBegins(self);
    end else
    begin
      if  (y < DragHandle.Top)
      and not MinButton.BoundsRect.ContainsPos(x, y) then
      begin
        case GetBusy() of
        true:
          begin
            SetPosition(Position-PageSize);
          end;
        false:
          begin
            var LCache := SetBusy(true);
            if not LCache then
            begin
              if assigned(OnScrollBegins) then
                OnScrollBegins(self);
            end;

            SetPosition(Position-PageSize);

            if not LCache then
            begin
              if assigned(OnScrollEnds) then
                OnScrollEnds(self);
            end;
            SetBusy(LCache);
          end;
        end;
      end else

      if (y>DragHandle.BoundsRect.Bottom)
      and not MaxButton.BoundsRect.ContainsPos(x, y) then
      begin
        case GetBusy() of
        true:
          begin
            SetPosition(Position+PageSize);
          end;
        false:
          begin
            var LCache := SetBusy(true);
            if not LCache then
            begin
              if assigned(OnScrollBegins) then
                OnScrollBegins(self);
            end;

            SetPosition(Position+PageSize);

            if not LCache then
            begin
              if assigned(OnScrollEnds) then
                OnScrollEnds(self);
            end;
            SetBusy(LCache);
          end;
        end;
      end;
    end;
  end;
end;

procedure TWbVerticalScrollbar.HandleDragBarMouseMove(Sender: TObject;
  shiftState: TShiftState; x, y: integer);
begin
  {$IFDEF DEBUG}
  writeln("DragBarMouseMove fired");
  {$ENDIF}

  if GetBusy() then
  begin
    {$IFDEF DEBUG}
    writeln("DragBarMouseMove Busy flag is kosher");
    {$ENDIF}

    (* take offset on draghandle into account *)
    var dy := y - FEntry;

    (* position draghandle *)
    DragHandle.Top := TInteger.EnsureRange(dy,
      MinButton.BoundsRect.Bottom,
      MaxButton.Top - FDragSize);

    (* Update position based on draghandle *)
    var LNewPos := PixelOffsetToPosition(DragHandle.Top - MinButton.BoundsRect.Bottom);
    SetPositionNoCalc(LNewPos);
  end;
end;

procedure TWbVerticalScrollbar.HandleDragBarMouseUp(Sender: TObject;
  button: TMouseButton;
  shiftState: TShiftState; x, y: integer);
begin
  {$IFDEF DEBUG}
  writeln('Mouse UP: '+IntToStr(x)+' '+IntToStr(y));
  {$ENDIF}

  SetBusy(False);
  {$IFNDEF NO_CAPTURE}
  ReleaseCapture();
  {$ENDIF}
  if assigned(OnScrollEnds) then
  OnScrollEnds(self);


  if GetBusy() then
  begin
  end;
end;

procedure TWbVerticalScrollbar.Resize;
begin
  inherited;
  var cw := ClientWidth;
  MinButton.SetBounds(0, 0, cw, cw);
  MaxButton.SetBounds(0,(ClientHeight-cw), cw, cw);
  Recalculate();

  //DragHandle.visible := (Total > 0) or (FDragpos > 0);
  //if DragHandle.Visible then
  DragHandle.SetBounds(1, cw + FDragPos, cw-3, FDragSize);
end;

function TWbVerticalScrollbar.GetArea: integer;
begin
  Result := MaxButton.Top  - MinButton.BoundsRect.Bottom;
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var StyleCode := #"
    .TWbVerticalScrollbar {
      padding: 0px;
      overflow: hidden;
      background-repeat: repeat-y;
      -webkit-touch-callout: none;
      background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAYCAYAAADzoH0MAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAAWUlEQVQ4T2NQ8O39L6ns+5/fouK/qF3df2mXlv+K3t3/1QMn/teNmPHfJG7ef6vUpf8dslf/dyva9N+nYsf/oLp9YD2T9v//P2rAqAGjBowaMGrAMDLg/38AjrwynnV8JvgAAAAASUVORK5CYII=);
    }

    .TWbVerticalScrollbar_unfocused {
      padding: 0;
      overflow: hidden;
      -webkit-touch-callout: none;
      background-repeat: repeat-y;
      background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAVCAYAAABPPm7SAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAAUUlEQVQ4T+3MoRHAIAxA0Q7DBigUKjE4FA6FQ0Vl2czye9e7bpEB3ntUlVIKtVZaa/TeERHGGMw5WWux9+acw70XM8PdPxMRZJBBBhlk8AfBC+sTo+W3YGHSAAAAAElFTkSuQmCC);
    }


    .TWbHorizontalScrollbar {
      padding: 0;
      overflow: hidden;
      cursor: default;
      background-repeat: repeat-x;
      background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABgAAAAQCAYAAAAMJL+VAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAAAZdEVYdFNvZnR3YXJlAHBhaW50Lm5ldCA0LjAuMTM0A1t6AAAAd0lEQVQ4T7XUIRKCUABF0bcDk8Gx4BhMNgNDYAyMBX7QZKKRbKSfSDbHoMlEI9FMJFnadRUvnC0cJeGJk1bbgJMWacRJy7zDSevijpM25QMn7c4vnLS/fnDSoe5xUtYMOOl4G3HSqf3ipCpOOOnS/XDyZ/eewQf+3e0yns2b5KsAAAAASUVORK5CYII=);
    }

    .TWbHorizontalScrollbar_unfocused {
      padding: 0;
      overflow: hidden;
      cursor: default;
      background-repeat: repeat-x;
      background-image: url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABoAAAAQCAIAAACHs/j/AAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAABUSURBVDhPzdShEQBBCANA+LmyKAdDA7SAQ9EvZ9LBR9yqKIYg0O4WHjUzRAZ1d0QGjQhEhu9QkcdpZmJRhrfLsm9XVejN8HjZmcGiDOwXsLuI/4lciIEVyftpF84AAAAASUVORK5CYII=);
    }


  .TWbScrollbarHandleV {
    overflow: hidden;
    cursor: default;

    border-style: solid;
    border-top: 1px solid #7FACE9;
    border-left: 1px solid #7FACE9;
    border-right: 1px solid #3966A6;
    border-bottom: 1px solid #3966A6;
    outline: 1px solid #000000;

    background: #4976b6; /* Old browsers */
    background: -moz-linear-gradient(left,  #4976b6 0%, #80aded 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left,  #4976b6 0%,#80aded 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right,  #4976b6 0%,#80aded 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4976b6', endColorstr='#80aded',GradientType=1 ); /* IE6-9 */
  }

  .TWbScrollbarHandleV:active {
    cursor: default;
    border-style: solid;
    border-top: 1px solid #FFFFFF;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #D2D2D2;
    border-bottom: 1px solid #D2D2D2;
    outline: 1px solid #000000;

    background: #4976b6; /* Old browsers */
    background: -moz-linear-gradient(left,  #c8c8c8 0%, #f5f5f5 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left,  #c8c8c8 0%,#f5f5f5 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right,  #c8c8c8 0%,#f5f5f5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c8c8c8', endColorstr='#f5f5f5',GradientType=1 ); /* IE6-9 */
  }

  .TWbScrollbarHandleV:disabled {
    cursor: wait;
    opacity: 0.5;
    pointer-events: none;

    border-style: solid;
    border-top: 1px solid #FFFFFF;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #D2D2D2;
    border-bottom: 1px solid #D2D2D2;
    outline: 1px solid #000000;

    background: #b4b4b4; /* Old browsers */
    background: -moz-linear-gradient(left,  #b4b4b4 0%, #dddddd 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left,  #b4b4b4 0%,#dddddd 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right,  #b4b4b4 0%,#dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4b4b4', endColorstr='#dddddd',GradientType=1 ); /* IE6-9 */
  }

  .TWbScrollbarHandleV_unfocused {
    cursor: wait;
    opacity: 0.5;
    pointer-events: none;

    border-style: solid;
    border-top: 1px solid #FFFFFF;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #D2D2D2;
    border-bottom: 1px solid #D2D2D2;
    outline: 1px solid #000000;

    background: #b4b4b4; /* Old browsers */
    background: -moz-linear-gradient(left,  #b4b4b4 0%, #dddddd 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left,  #b4b4b4 0%,#dddddd 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right,  #b4b4b4 0%,#dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4b4b4', endColorstr='#dddddd',GradientType=1 ); /* IE6-9 */
  }

  .TWbScrollbarHandle {
    overflow: hidden;
    cursor: default;
    border-style: solid;
    border-top: 1px solid #7FACE9;
    border-left: 1px solid #7FACE9;
    border-right: 1px solid #3966A6;
    border-bottom: 1px solid #3966A6;
    outline: 1px solid #000000;

    background: #4976b6; /* Old browsers */
    background: -moz-linear-gradient(top, #4976b6 0%, #80aded 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, #4976b6 0%,#80aded 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, #4976b6 0%,#80aded 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#4976b6', endColorstr='#80aded',GradientType=0 ); /* IE6-9 */
  }

  .TWbScrollbarHandle:active {
    cursor: default;
    border-style: solid;
    border-top: 1px solid #FFFFFF;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #D2D2D2;
    border-bottom: 1px solid #D2D2D2;
    outline: 1px solid #000000;

    background: #c8c8c8; /* Old browsers */
    background: -moz-linear-gradient(top, #c8c8c8 0%, #f5f5f5 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(top, #c8c8c8 0%,#f5f5f5 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to bottom, #c8c8c8 0%,#f5f5f5 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#c8c8c8', endColorstr='#f5f5f5',GradientType=0 ); /* IE6-9 */
  }

  .TWbScrollbarHandle_unfocused {
    cursor: wait;
    opacity: 0.5;
    pointer-events: none;

    border-style: solid;
    border-top: 1px solid #FFFFFF;
    border-left: 1px solid #FFFFFF;
    border-right: 1px solid #D2D2D2;
    border-bottom: 1px solid #D2D2D2;
    outline: 1px solid #000000;

    background: #b4b4b4; /* Old browsers */
    background: -moz-linear-gradient(left,  #b4b4b4 0%, #dddddd 100%); /* FF3.6-15 */
    background: -webkit-linear-gradient(left,  #b4b4b4 0%,#dddddd 100%); /* Chrome10-25,Safari5.1-6 */
    background: linear-gradient(to right,  #b4b4b4 0%,#dddddd 100%); /* W3C, IE10+, FF16+, Chrome26+, Opera12+, Safari7+ */
    filter: progid:DXImageTransform.Microsoft.gradient( startColorstr='#b4b4b4', endColorstr='#dddddd',GradientType=1 ); /* IE6-9 */
  }";
  Sheet.Append(StyleCode);
end;

end.
