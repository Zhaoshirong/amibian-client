unit desktop.window.studio;

interface

uses
  System.Types,
  System.Types.Graphics,
  System.Time,
  System.Colors,
  System.Types.Convert,
  System.Lists,

  desktop.types,
  desktop.window,
  desktop.toolbar,
  desktop.panel,
  desktop.button,

  desktop.window.ace,

  AceEdit.Mode.Pascal,
  AceEdit.Theme.Dreamweaver,
  AceEdit.Theme.Monokai,
  AceEdit.Theme.Twilight,

  SmartCL.AceEditor,
  SmartCL.System,
  SmartCl.Events,
  SmartCL.Theme,
  SmartCL.Css.StyleSheet,
  SmartCL.ControlWrapper,
  SmartCL.Fonts,
  SmartCL.Borders,
  SmartCL.Layout,
  SmartCL.Components,
  SmartCL.Controls.Label,
  SmartCL.Controls.Panel,
  SmartCL.Controls.Toolbar,
  SmartCL.Controls.Scrollbar;

type

  TWbProjectPanel = class(TW3Panel)
  private
    FToolbar: TW3Toolbar;
    FTitle:   TW3Label;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure StyleTagObject; override;
    procedure Resize; override;
  end;

  TWbStudioWindow = class(TWbWindow)
  private
    FProjectPanel:  TWbProjectPanel;

    FPanel:       TWbPanel;
    FFooter:      TWbAceFooter;
    FToolBar:     TWbToolbar;
    FBtnNew:      TWbToolbarButton;
    FBtnOpen:     TWbToolbarButton;
    FBtnSave:     TWbToolbarButton;
    FBtnCut:      TWbToolbarButton;
    FBtnCopy:     TWbToolbarButton;
    FBtnPaste:    TWbToolbarButton;
    FIgnorePos:   boolean;
    FEditor:      TW3AceEditor;

    procedure KillScrollbars;

    function  GetPageSize: TPoint;
    function  GetDocSize: TPoint;

    procedure UpdateScrollbars;
    procedure UpdatePosInfo;

    procedure HandleAceSessionChange(Sender: TObject; const Session: JAceSession);
    procedure HandleAceScrollLeftChange(Sender: TObject; const Session: JAceSession; Value: integer);
    procedure HandleAceScrollTopChange(Sender: TObject; const Session: JAceSession; Value: integer);

    procedure HandleScrollbegins(Sender: TObject);
    procedure HandleScrollEnds(Sender: TObject);
    procedure HandleScrollChanged(Sender: TObject);

  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  Editor: TW3AceEditor read FEditor;
  end;

implementation

uses SmartCL.Fonts.Detector;

//#############################################################################
// TWbProjectPanel
//#############################################################################

procedure TWbProjectPanel.InitializeObject;
begin
  inherited;
  FToolbar := TW3Toolbar.Create(self);
  FToolbar.SetSize(100, 36);

  FTitle := TW3Label.Create(self);
  FTitle.Caption := 'Project Manager';
  FTitle.SetSize(100, 24);
  FTitle.Border.Bottom.Style :=  besGroove;
  FTitle.Border.Bottom.Width := 2;
end;

procedure TWbProjectPanel.FinalizeObject;
begin
  FTitle.free;
  FToolbar.free;
  inherited;
end;

procedure TWbProjectPanel.StyleTagObject;
begin
  inherited;
  //ThemeBackground := bsContainerBackground;
end;

procedure TWbProjectPanel.ObjectReady;
begin
  inherited;
  TW3Dispatch.WaitFor([FToolbar, FTitle], procedure ()
  begin
    TW3Dispatch.Execute(Resize, 50);
  end);
end;

procedure TWbProjectPanel.Resize;
begin
  inherited;
  if (csReady in ComponentState) then
  begin
    if TW3Dispatch.AssignedAndReady([FTitle, FToolbar]) then
    begin
      var LBounds := ClientRect();

      FTitle.SetBounds(LBounds.left, LBounds.top, LBounds.width, FTitle.Height);
      LBounds.top += FTitle.height;

      LBounds := Border.AdjustClientRect(LBounds);
      FToolbar.SetBounds(LBounds.left, LBounds.top, LBounds.width, FToolbar.Height);
      LBounds.top += FToolbar.height + 2;

    end;
  end;
end;




//#############################################################################
// TWbStudioWindow
//#############################################################################

procedure TWbStudioWindow.InitializeObject;
begin
  inherited;
  Header.Title.Caption := 'Cloud builder';

  SetOptions([woSizeable, woHScroll, woVScroll]);
  MinimumWidth := 220; //550;
  MinimumHeight := 220;

  FProjectPanel := TWbProjectPanel.Create(content);

  FEditor := TW3AceEditor.Create(Content);
  FPanel := TWbPanel.Create(Content);
  FPanel.Border.Size:=0;
  FToolbar := TWbToolbar.Create(FPanel);
  FFooter := TWbAceFooter.Create(Content);

  SetSize(420, 360);
end;

procedure TWbStudioWindow.FinalizeObject;
begin
  FProjectPanel.free;
  FEditor.free;
  FToolbar.free;
  FFooter.free;
  FPanel.free;
  inherited;
end;

procedure TWbStudioWindow.ObjectReady;
begin
  inherited;

  TW3Dispatch.WaitFor([FToolbar],
  procedure()
  begin
    FToolbar.ButtonWidth := 80;
    FToolbar.ButtonHeight := 28;

    TW3Dispatch.WaitFor([FBtnNew, FBtnOpen, FBtnSave, FBtnCut, FBtnCopy, FBtnPaste],
    procedure ()
    begin
      FBtnNew := FToolbar.Add();
      FBtnNew.Layout := glGlyphLeft;
      FBtnNew.Glyph.LoadFromURL('res/new_file.png');
      FBtnNew.Caption := "New";

      FBtnOpen := FToolbar.Add();
      FBtnOpen.Layout := glGlyphLeft;
      FBtnOpen.Glyph.LoadFromURL('res/open_file.png');
      FBtnOpen.Caption := "Open";

      FBtnSave := FToolbar.Add();
      FBtnSave.Layout := glGlyphLeft;
      FBtnSave.Glyph.LoadFromURL('res/save_file.png');
      FBtnSave.Caption := "Save";

      FBtnCut := FToolbar.Add();
      FBtnCut.Layout := glGlyphLeft;
      FBtnCut.Glyph.LoadFromURL('res/cut.png');
      FBtnCut.Caption := "Cut";

      FBtnCopy := FToolbar.Add();
      FBtnCopy.Layout := glGlyphLeft;
      FBtnCopy.Glyph.LoadFromURL('res/copy.png');
      FBtnCopy.Caption := "Copy";

      FBtnPaste := FToolbar.Add();
      FBtnPaste.Layout := glGlyphLeft;
      FBtnPaste.Glyph.LoadFromURL('res/paste.png');
      FBtnPaste.Caption := "Paste";

      FToolbar.Invalidate();

      TW3Dispatch.WaitFor([FPanel, FFooter, FEditor], procedure ()
      begin
        // Make sure these are always visible, because we are going
        // to hide them
        var opts := TVariant.CreateObject;
        opts["hScrollBarAlwaysVisible"] := false;
        opts["vScrollBarAlwaysVisible"] := false;
        FEditor.SetOptions(Opts);

        FEditor.SetFont('Courier New', 12);

        FEditor.selection.on("changeCursor", procedure ()
        begin
          UpdatePosInfo();
          UpdateScrollbars();
        end);

        FEditor.OnSessionChanged := @HandleAceSessionChange;
        FEditor.OnScrollLeftChanged := @HandleAceScrollLeftChange;
        FEditor.OnScrollTopChanged := @HandleAceScrollTopChange;

        KillScrollbars();

        FEditor.EditorMode := TW3AceModePascal.Create(FEditor);
        FEditor.Theme := TW3AceThemeTwilight.Create(FEditor);

        writeln("Setting up scrollhandlers");
        VerticalScroll.OnScrollBegins:= @HandleScrollbegins;
        VerticalScroll.OnScrollEnds:= @HandleScrollEnds;
        VerticalScroll.OnChanged := @HandleScrollChanged;

        TW3Dispatch.Execute( procedure ()
        begin
          FEditor.width := 0;
          Resize();
        end, 200);

      end);
    end);

  end);

  FPanel.OnResize := procedure (Sender:TObject)
  begin
    FToolbar.SetBounds(FPanel.ClientRect);
  end;

end;

procedure TWbStudioWindow.HandleScrollBegins(Sender: TObject);
begin
  FIgnorePos := true;
end;

procedure TWbStudioWindow.HandleScrollChanged(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
    FEditor.Renderer.scrollToRow(VerticalScroll.Position);
end;

procedure TWbStudioWindow.HandleScrollEnds(Sender: TObject);
begin
  FIgnorePos := false;
end;

procedure TWbStudioWindow.HandleAceScrollTopChange(Sender: TObject; const Session: JAceSession; Value: integer);
begin
  if not (csDestroying in ComponentState) then
  begin
    if not FIgnorePos then
      UpdateScrollbars();
  end;
end;

procedure TWbStudioWindow.HandleAceScrollLeftChange(Sender: TObject; const Session: JAceSession; Value: integer);
begin
  if not (csDestroying in ComponentState) then
    UpdateScrollbars;
end;

procedure TWbStudioWindow.HandleAceSessionChange(Sender: TObject; const Session: JAceSession);
begin
  if not (csDestroying in ComponentState) then
  begin
    UpdatePosInfo();
    UpdateScrollbars();
  end;
end;

function TWbStudioWindow.GetPageSize: TPoint;
var
  LItems: array of THandle;
begin
  var LRoot := Handle;
  asm
    @LItems = (@LRoot).getElementsByClassName('ace_scroller');
  end;
  if LItems.length > 0 then
  begin
    var LTextLayer := LItems[0];
    var LWrapper := TW3HtmlElement.Create(LTextLayer);
    try
      result.x := LWrapper.clientWidth;
      result.y := LWrapper.clientHeight;

      var wd := FEditor.Renderer.layerConfig.width;
      var hd := FEditor.Renderer.layerConfig.height;

      result.y := hd div FEditor.LineHeight;
      result.x := hd div FEditor.Renderer.layerConfig.characterWidth;

      if (FEditor.LineHeight * result.y) < hd then
        inc(result.y);
    finally
      LWrapper.free;
    end;
  end;

end;

function TWbStudioWindow.GetDocSize: TPoint;
begin
  // Get pixel-width of char [mono-font]
  var LCharWidth := FEditor.Renderer.layerConfig.characterWidth;

  // Get edit-region pixel width
  var wd := FEditor.Renderer.layerConfig.width;

  // divide to get number of chars per row
  result.x := wd div LCharWidth;
  if (result.x * LCharWidth) < FEditor.Renderer.layerConfig.width then
    inc(result.x);

  // get number of lines in document
  result.y := FEditor.Session.getDocument().getAllLines().length;
end;

procedure TWbStudioWindow.UpdateScrollbars;
begin
  if not (csDestroying in ComponentState) then
  begin
    if not (csDestroying in FEditor.ComponentState) then
    begin
      var LDocSize  := GetDocSize();
      var LPageSize := GetPageSize();
      var LPosition := FEditor.GetCursorPosition();

      VerticalScroll.Total := LDocSize.y;
      VerticalScroll.PageSize := LPageSize.y;

      if not FIgnorePos then
        VerticalScroll.Position := FEditor.Renderer.layerConfig.firstRowScreen;

      HorizontalScroll.Total := LDocSize.x;
      HorizontalScroll.PageSize := LPageSize.x;

      if not FIgnorePos then
        HorizontalScroll.Position := LPosition.column mod LPageSize.x;
    end;
  end;
end;

procedure TWbStudioWindow.UpdatePosInfo;
begin
  if not (csDestroying in ComponentState) then
  begin
    if (csReady in FEditor.ComponentState) then
    begin
      var LInfo := FEditor.GetCursorPosition();
      var dx := LInfo.column;
      var dy := LInfo.row;
      FFooter.Column.Caption := 'Col: ' + dx.ToString();
      FFooter.Row.Caption := 'Row: ' + dy.ToString();
      FFooter.FileName.Caption := 'noname.txt';
    end;
  end;
end;

procedure TWbStudioWindow.KillScrollbars;
var
  LItems: array of THandle;
begin
  if not (csDestroying in ComponentState) then
  begin
    var LTemp := Handle;

    if FEditor <> nil then
    begin
      asm
        @LItems = (@LTemp).getElementsByClassName('ace_scrollbar');
      end;
      for var LRef in LItems do
      begin
        LRef.style['overflow-x']:='hidden';
        LRef.style['overflow-y']:='hidden';
      end;
    end;
    if not (csDestroying in ComponentState) then
      TW3Dispatch.Execute(KillScrollbars, 1000);
  end;
end;

procedure TWbStudioWindow.Resize;
begin
  inherited;
  if (csReady in ComponentState) then
  begin
    if TW3Dispatch.AssignedAndReady([FProjectPanel, FPanel, FFooter, FEditor]) then
    begin
      var LBounds := Content.ClientRect();

      FProjectPanel.SetBounds(LBounds.left, LBounds.top, 300, LBounds.Height);
      inc(LBounds.left, 300);


      FPanel.SetBounds(LBounds.left, LBounds.top, LBounds.width, 44);

      FFooter.SetBounds(LBounds.left, LBounds.Bottom-22, LBounds.Width, 22);

      FEditor.SetBounds(LBounds.left, LBounds.top + 44, LBounds.width, LBounds.height - (44 + 22) );
    end;

    UpdateScrollbars();
  end;
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := #'

    .TWbStudioWindow {
      padding: 0px !important;
      margin: 0px !important;
      overflow-x: hidden;
      overflow-y: hidden;
    }

    .TWbProjectPanel {
      border-radius: 0px;
      padding: 0px !important;
      margin: 0px !important;
      overflow-x: hidden;
      overflow-y: hidden;
    }

  ';
  StyleCode := StrReplace(StyleCode,"§",prefix);
  Sheet.Append(StyleCode);
end;

end.
