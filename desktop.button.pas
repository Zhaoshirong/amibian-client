unit desktop.button;

interface

{$I 'Smart.inc'}

uses
  System.Types,
  System.Colors,
  System.Types.Graphics,
  System.Types.Convert,
  System.Time,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Device.Storage,

  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,
  SmartCL.Theme,

  SmartCL.Device.Storage,
  SmartCL.Application,
  SmartCL.Components,
  SmartCL.System,
  SmartCL.Graphics,
  SmartCL.FileUtils,
  SmartCL.Forms,
  SmartCL.Fonts,
  SmartCL.Controls.Button;

type

  TWbButton = class(TW3Button)
  protected
    procedure StyleTagObject; override;
  end;

implementation

procedure TWbButton.StyleTagObject;
begin
  inherited;
  ThemeReset();
  Font.Size := 16;
  Font.Color := clBlack;
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := StrReplace(
  #"

  .TWbButton {
    cursor: default;
    border-top: 1px solid #E0E0E0;
    border-left: 1px solid #E0E0E0;
    border-bottom: 1px solid #979797;
    border-right: 1px solid #979797;

    font-family: Ubuntu, tahoma, verdana;

    outline: 1px solid #42415A;
    outline-offset: 0px;

    background-image: -ms-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -moz-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -o-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #B0B0B0), color-stop(100, #D7D7D7));
    background-image: -webkit-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
    background-image: linear-gradient(to bottom, #B0B0B0 0%, #D7D7D7 100%);

    font-size: normal;
    text-align: center;
    font-weight: normal;
    §text-size-adjust: auto;
  }

  .TWbButton:active {
    border-top: 1px solid #979797;
    border-left: 1px solid #979797;
    border-bottom: 1px solid #E0E0E0;
    border-right: 1px solid #E0E0E0;

    background-image: -ms-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -moz-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -o-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: -webkit-gradient(linear, left bottom, right top, color-stop(0, #B0B0B0), color-stop(100, #D7D7D7));
    background-image: -webkit-linear-gradient(bottom left, #B0B0B0 0%, #D7D7D7 100%);
    background-image: linear-gradient(to top right, #B0B0B0 0%, #D7D7D7 100%);
  }

  .TWbButton:focus {
    text-decoration: underline;
   }
  ","§",prefix);
  Sheet.Append(StyleCode);
end;


end.
