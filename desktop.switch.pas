unit desktop.switch;

interface

{$I 'Smart.inc'}

uses
  W3C.DOM,
  System.Types,
  System.Time,
  System.Colors,
  System.Events,

  SmartCL.Css.Classes,
  SmartCL.Css.StyleSheet,
  SmartCL.Theme,
  SmartCL.System,
  SmartCL.Touch,
  SmartCL.Effects,
  SmartCL.borders,
  SmartCL.Components,
  SmartCL.Controls.Label;

type

  EW3ToggleSwitch = EW3Exception;

  (* Class for switch leaver *)
  TWBToggleKnob = class(TW3CustomControl)
  protected
    procedure HandleKeyPressed(Sender: TObject; const Character: char); virtual;
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
  public
    function CreationFlags: TW3CreationFlags; override;
  end;

  (* We want text to be non-selectable, so we isolate that
     in a base-class and inherit from that *)
  TWBToggleLabel = class(TW3Label)
  protected
    procedure StyleTagObject; override;
  public
    function CreationFlags: TW3CreationFlags; override;
  end;

  (* The on label, inherits from our non-text-selectable parent *)
  TWBToggleOnLabel = class(TWBToggleLabel)
  end;

  (* The off label, inherits from our non-text-selectable parent *)
  TWBToggleOffLabel = class(TWBToggleLabel)
  protected
    procedure StyleTagObject; override;
  end;

  (* And the actual switch control *)
  TWBToggleSwitch = class(TW3CustomControl)
  private
    FChecked: boolean;
    FKnob:    TWBToggleKnob;
    FOnText:  TWBToggleOnLabel;
    FOffText: TWBToggleOffLabel;
    procedure HandleTouch(Sender: TObject; Info: TW3TouchData);
    procedure HandleMouseClick(Sender: TObject);
  protected
    procedure SetChecked(const CheckValue: boolean); virtual;
    procedure SetEnabled(const EnabledValue: boolean); override;

    procedure RegisterChild(const Child: TW3TagContainer); override;

    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady;override;

    procedure StyleTagObject; override;

    procedure Resize; override;
  public
    property  TextOn: TWBToggleOnLabel read FOnText;
    property  TextOff: TWBToggleOffLabel read FOffText;
    procedure Toggle;

    function CreationFlags: TW3CreationFlags; override;
  published
    property Checked: boolean read FChecked write SetChecked;
  end;

implementation

uses
  System.Consts, SmartCL.Consts;

//###########################################################################
// TW3ToggleBaseLabel
//###########################################################################

procedure TWBToggleKnob.InitializeObject;
begin
  inherited;
  OnKeyPress := @HandleKeyPressed;
end;

procedure TWBToggleKnob.FinalizeObject;
begin
  OnKeyPress := nil;
  inherited;
end;

function TWBToggleKnob.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfSupportAdjustment);   // Dont adjust
  Exclude(result, cfAllowSelection);      // No selection [text, glyphs]
  Exclude(result, cfReportChildAddition); // No add child events
  Exclude(result, cfReportChildRemoval);  // No remove child events
  Exclude(result, cfReportMovement);      // No movement events either
  Exclude(result, cfReportResize);        // No we dont want resize management
  Include(Result, cfKeyCapture);          // Yes listen to keyboard presses
end;

procedure TWBToggleKnob.HandleKeyPressed(Sender: TObject; const Character: char);
begin
  if Character in [#13, #32] then
    TWBToggleSwitch(Parent).Toggle();
end;
//###########################################################################
// TWBToggleLabel
//###########################################################################

procedure TWBToggleOffLabel.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;

//###########################################################################
// TWBToggleLabel
//###########################################################################

function TWBToggleLabel.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(result, cfAllowSelection);      // No selection [text, glyphs]
  Exclude(result, cfReportChildAddition); // No add child events
  Exclude(result, cfReportChildRemoval);  // No remove child events
  Exclude(result, cfReportMovement);      // No movement events either
  Exclude(Result, cfKeyCapture);          // No listening to keyboard events
  Exclude(result, cfReportResize);        // Yes we want resize management
end;

procedure TWbToggleLabel.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;

//###########################################################################
// TWBToggleSwitch
//###########################################################################

procedure TWbToggleSwitch.InitializeObject;
begin
  inherited;
  (* Create the "ON" label *)
  FOnText := TWBToggleOnLabel.Create(Self);
  FOnText.Caption := TBoolean.OnOff(true);
  FOnText.VAlign := tvCenter;
  FOnText.AlignText := taCenter;

  (* Create the "OFF" label *)
  FOffText := TWBToggleOffLabel.Create(Self);
  FOffText.Caption := TBoolean.OnOff(false);
  FOffText.VAlign := tvCenter;
  FOffText.AlignText := taCenter;

  (* Create our leaver knob *)
  FKnob := TWBToggleKnob.Create(Self);
  FKnob.OnClick := HandleMouseClick;
  FKnob.OnTouchEnd := HandleTouch;

  self.OnClick := HandleMouseClick;

  SetSize(100, 32);
end;

function TWbToggleSwitch.CreationFlags: TW3CreationFlags;
begin
  result := inherited CreationFlags();
  Exclude(Result, cfSupportAdjustment);
  Exclude(Result, cfAllowSelection);
  Exclude(Result, cfKeyCapture);
end;

procedure TWbToggleSwitch.FinalizeObject;
begin
  FKnob.Free;
  FOffText.Free;
  FOnText.Free;
  inherited;
end;

procedure TWbToggleSwitch.ObjectReady;
begin
  inherited;

  var LBounds := ClientRect;
  FKnob.MoveTo(LBounds.left, LBounds.top);

  if FChecked then
  begin
    var LChecked := FChecked;
    FChecked := false;
    SetChecked(LChecked);
  end;
end;

procedure TWbToggleSwitch.StyleTagObject;
begin
  inherited;
  ThemeReset();
end;

procedure TWbToggleSwitch.RegisterChild(const Child: TW3TagContainer);
begin
  if Child  <> nil then
  begin
    if (Child is TWBToggleKnob)
    or (child is TWbToggleLabel) then
    inherited RegisterChild(Child) else
    raise EW3ToggleSwitch.CreateFmt(CNT_ERR_CONTROL_Augmentation, [classname]);
  end else
  inherited RegisterChild(Child);
end;

procedure TWbToggleSwitch.SetEnabled(const EnabledValue: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    inherited SetEnabled(EnabledValue);
    case EnabledValue of
    true:
      begin
        FKnob.opacity := 255;
        FKnob.alphablend := false;
      end;
    false:
      begin
        FKnob.opacity := 128;
        FKnob.alphablend := true;
      end;
    end;
  end;
end;

procedure TWbToggleSwitch.SetChecked(const CheckValue: boolean);
begin
  if not (csDestroying in ComponentState) then
  begin
    if (csReady in ComponentState) then
    begin
      if (CheckValue <> FChecked) then
      Toggle;
    end else
    FChecked := CheckValue;
  end;
end;

procedure TWbToggleSwitch.HandleTouch(Sender: TObject; Info: TW3TouchData);
begin
  if (Info.Touches[0].Target = FKnob.Handle) then
  Toggle;
end;

procedure TWbToggleSwitch.HandleMouseClick(Sender: TObject);
begin
  if not (csDestroying in ComponentState) then
  begin
    TW3ControlTracker.SetFocusedControl( TW3CustomControl(Sender) );
    if Enabled then
      Toggle();
  end;
end;

procedure TWbToggleSwitch.Toggle;
begin
  if not (csDestroying in ComponentState) then
  begin
    if (csReady in ComponentState) then
    begin
      if enabled then
      begin
        var LBounds := ClientRect;
        var dx := LBounds.left;
        var dy := LBounds.Top;

        if not FChecked then
          dx := LBounds.Right - (FKnob.Width);

        FKnob.fxMoveTo(dx, dy, 0.12, procedure ()
        begin
          FChecked := not FChecked;
          if Assigned(OnChanged) then
            OnChanged(Self);
        end);

      end;
    end else
    FChecked := not FChecked;
  end;
end;


procedure TWbToggleSwitch.Resize;
begin
  inherited;

  var LBounds := ClientRect;

  if not LBounds.empty then
  begin
    var Half := LBounds.width div 2;

    FOnText.SetBounds(
      LBounds.left,
      LBounds.top,
      Half,
      LBounds.height
      );

    var dx := LBounds.left;
    inc(dx, half);

    LBounds.left += Half;

    FOffText.SetBounds(LBounds);

    FKnob.top := LBounds.Top;
    FKnob.height := LBounds.height;
  end;
end;

initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var Prefix := BrowserAPI.PrefixDef('');
  var StyleCode := #"

.TWBToggleSwitch {
  margin: 0px;
  padding: 0px;
  background-color: #FFFFFF;
	border: 1px solid #AAA;
	border-radius:6px;
	color:#FFF;
	overflow: hidden;
	box-shadow:0 1px 0 #FFF;
 }

.TWBToggleKnob {
  width: 56% !important;
  border: 1px solid #333333;

  cursor: default;
  margin: 1px;
  padding: 0px;
  border-radius: 6px;

  border-top: 1px solid #E0E0E0;
  border-left: 1px solid #E0E0E0;
  border-bottom: 1px solid #979797;
  border-right: 1px solid #979797;

  background-image: -ms-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
  background-image: -moz-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
  background-image: -o-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
  background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #B0B0B0), color-stop(100, #D7D7D7));
  background-image: -webkit-linear-gradient(top, #B0B0B0 0%, #D7D7D7 100%);
  background-image: linear-gradient(to bottom, #B0B0B0 0%, #D7D7D7 100%);

  -webkit-box-shadow: 0px 0px 0px 1px rgba(0,0,0,0.75);
     -moz-box-shadow: 0px 0px 0px 1px rgba(0,0,0,0.75);
          box-shadow: 0px 0px 0px 1px rgba(0,0,0,0.75);
}

.TWBToggleOnLabel {
  cursor: default;
  margin: 0px;
  padding: 0px;
  border-radius: 6px;
  text-transform: capitalize;
	text-shadow: 1px 1px #093B5C;
  color: #FFFFFF;

  background: #618ECE;
  background-image: -webkit-linear-gradient(top, #2B5897 0%, #5B88C7 100%);
  background-image:    -moz-linear-gradient(top, #2B5897 0%, #5B88C7 100%);
  background-image:     -ms-linear-gradient(top, #2B5897 0%, #5B88C7 100%);
  background-image:      -o-linear-gradient(top, #2B5897 0%, #5B88C7 100%);
  background-image:        -webkit-gradient(linear, left top, left bottom, color-stop(0, #2B5897), color-stop(100, #5B88C7));
  background-image:         linear-gradient(to bottom, #2B5897 0%, #5B88C7 100%);

  -webkit-box-shadow: inset 0 0 10px #000000;
     -moz-box-shadow: inset 0 0 10px #000000;
       -o-box-shadow: inset 0 0 10px #000000;
          box-shadow: inset 0 0 10px #000000;
}

.TWBToggleOffLabel {
  cursor: default;
  margin: 0px;
  padding: 0px;
  border-radius: 6px;
  color: #FFFFFF;
  text-transform: capitalize;
  text-shadow: 1px 1px #093B5C;

	box-shadow:0 4px 5px -2px rgba(0,0,0,0.3) inset;

  background-image: -ms-linear-gradient(top, #000000 0%, #293D69 100%);
  background-image: -moz-linear-gradient(top, #000000 0%, #293D69 100%);
  background-image: -o-linear-gradient(top, #000000 0%, #293D69 100%);
  background-image: -webkit-gradient(linear, left top, left bottom, color-stop(0, #000000), color-stop(100, #293D69));
  background-image: -webkit-linear-gradient(top, #000000 0%, #293D69 100%);
  background-image: linear-gradient(to bottom, #000000 0%, #293D69 100%);
}
  ";
  Sheet.Append(StyleCode);
end;


end.


end.