unit desktop.control.toolbarpanel;

interface

uses
  W3C.DOM,
  System.Types,
  System.Types.Convert,
  System.Types.Graphics,
  System.Colors,
  System.Time,
  System.Widget,

  desktop.types,
  desktop.panel,
  desktop.edit,
  desktop.button,
  desktop.toolbar,

  desktop.control.pathpanel,

  SmartCL.Time,
  SmartCL.System,
  SmartCL.Layout,
  SmartCL.Controls.Elements,
  SmartCL.Graphics,
  SmartCL.Components,
  SmartCL.Effects,
  SmartCL.Fonts,
  SmartCL.Borders,
  SmartCL.CSS.Classes,
  SmartCL.CSS.StyleSheet;

type

  TWbToolbarPanel = class(TWbPanel)
  private
    FToolbar: TWbToolbar;
    FPath:    TWbPathPanel;
    FLayout:  TLayout;
  protected
    procedure InitializeObject; override;
    procedure FinalizeObject; override;
    procedure ObjectReady; override;
    procedure Resize; override;
  public
    property  PathPanel: TWbPathPanel read FPath;
    property  Toolbar: TWbToolbar read FToolbar;
  end;


implementation


//#############################################################################
// TWbToolbarPanel
//#############################################################################

procedure TWbToolbarPanel.InitializeObject;
begin
  inherited;
  FToolbar := TWbToolbar.Create(self);
  FToolbar.width := 100;
  FToolbar.Height := 32;

  FPath := TWbPathPanel.Create(self);
  FPath.Width := 100;
  FPath.height := 38;

  FLayout := nil;

  SetSize(100, 76);
end;

procedure TWbToolbarPanel.FinalizeObject;
begin
  FLayout.free;
  FToolbar.free;
  FPath.free;
  inherited;
end;

procedure TWbToolbarPanel.ObjectReady;
begin
  inherited;

  TW3Dispatch.WaitFor([FToolbar, FPath], procedure ()
  begin
    FLayout := Layout.Client(
      [
        Layout.top(Layout.Height(40), FToolbar),
        layout.top(Layout.Height(36), FPath)
      ]);

    Resize();
    TW3Dispatch.Execute(Invalidate, 100);
  end);
end;

procedure TWbToolbarPanel.Resize;
begin
  inherited;
  if (csReady in ComponentState) then
  begin
    if FLayout <> nil then
      FLayout.Resize(self);
  end;
end;


initialization
begin
  var Sheet := TW3StyleSheet.GlobalStyleSheet;
  var StyleCode := #"
    .TWbToolbarPanel {
      padding: 0px !important;
      margin: 0px !important;
    }
  ";
  Sheet.Append(StyleCode);
end;

end.
