unit desktop.rexx;

interface

uses 
  System.Types,
  System.Types.Convert,
  System.JSON,
  System.Streams,
  System.Reader,
  System.Writer,
  System.Text.Parser;

type

  TRexxToken = (
    rcNoop    = 0,
    rcSay     = 1,
    rcAddress = 2,
    rcArg     = 3,
    rcCall    = 4,
    rcReturn  = 5,
    rcShell   = 6,
    rcSignal  = 7
  );

  TRexxSymbol = class(TParserModelObject)
  public
  end;

  TRexxParameter = class(TRexxSymbol)
  public
    property  Value: variant;
  end;

  TRexxCodeBlock = class(TRexxSymbol)
  public
    property SubItems: array of TRexxSymbol;
  end;

  TRexxInstruction = class(TRexxCodeBlock)
  public
    property  Command: TRexxToken;
    property  Parameters: array of TRexxParameter;
  end;

  TRexxModelProgram = class(TRexxCodeBlock)
  end;

  TRexxContext = class(TParserContext)
  end;

  TRexxParser = class(TCustomParser)
  private
    function  ParseSay(const Context: TRexxContext): boolean;
    function  ParseAddress(const Context: TRexxContext): boolean;
    function  ParseArg(const Context: TRexxContext): boolean;
    function  ParseCall(const Context: TRexxContext): boolean;
    function  ParseReturn(const Context: TRexxContext): boolean;
    function  ParseShell(const Context: TRexxContext): boolean;
    function  ParseSignal(const Context: TRexxContext): boolean;
  public
    function  Parse: boolean; override;
  end;

implementation

//#############################################################################
// TRexxParser
//#############################################################################

function TRexxParser.Parse: boolean;
begin
  result := false;

  var LBuffer := Context.Buffer;
  if not LBuffer.EOF then
  begin
    repeat
      LBuffer.ConsumeJunk();
      if LBuffer.EOF() then
        break;

      var LTemp := '';
      var LBookmark := LBuffer.Bookmark();

      if LBuffer.ReadWord(LTemp) then
      begin
        LBuffer.Restore(LBookmark);
        case LTemp.trim().ToLower() of
        'say':
          begin
          end;
        end;

      end else
        break;
    until LBuffer.EOF;
  end;
end;

function TRexxParser.ParseSay(const Context: TRexxContext): boolean;
begin
  result := false;
end;

function TRexxParser.ParseAddress(const Context: TRexxContext): boolean;
begin
  result := false;
end;

function TRexxParser.ParseArg(const Context: TRexxContext): boolean;
begin
  result := false;
end;

function TRexxParser.ParseCall(const Context: TRexxContext): boolean;
begin
  result := false;
end;

function TRexxParser.ParseReturn(const Context: TRexxContext): boolean;
begin
  result := false;
end;

function TRexxParser.ParseShell(const Context: TRexxContext): boolean;
begin
  result := false;
end;

function TRexxParser.ParseSignal(const Context: TRexxContext): boolean;
begin
  result := false;
end;

end.
